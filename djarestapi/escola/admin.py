from django.contrib import admin

from escola.models import Aluno, Curso

class Alunos(admin.ModelAdmin):
    list_display = ('id', 'nome','rg','cpf','data')
    list_display_links = ('id','nome')
    search_fields = ('nome',)
    list_per_page = 20

admin.site.register(Aluno,Alunos)

class Cursos(admin.ModelAdmin):
    list_display = ('id','cod', 'desc','nivel')
    list_display_links = ('id','cod','desc')
    search_fields = ('desc',)
    list_per_page = 20

admin.site.register(Curso,Cursos)
