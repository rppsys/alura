# Projeto Alura

## Esse app roda dentro de uma vEnv chamada vML

## Piplist

pip install selenium
pip install jupyter ipython django-extensions
pip install numpy
pip install pandas
pip install bokeh
pip install sklearn
pip install nltk

pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0 

pip3 install pycairo
pip3 install PyGObject

pip3 install pyperclip
pip install matplotlib

## Drivers

https://sites.google.com/chromium.org/driver/getting-started?authuser=0

https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/

https://github.com/mozilla/geckodriver/releases

https://webkit.org/blog/6900/webdriver-support-in-safari-10/

## Driver Firefox

https://github.com/mozilla/geckodriver

https://www.selenium.dev/selenium/docs/api/py/

https://www.selenium.dev/pt-br/documentation/webdriver/elements/finders/

https://www.selenium.dev/pt-br/documentation/webdriver/elements/locators/

https://w3c.github.io/webdriver/#element-click

# Importação do WebKit2

import gi
gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2

# Comandos

python -m ipykernel install --user --name=vALURA

