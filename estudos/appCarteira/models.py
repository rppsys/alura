from django.db import models
from django.utils import timezone

class Exchange(models.Model):
    def __str__(self):
        return self.strNome
    strNome = models.CharField('Nome', max_length=30)
    strSigla = models.CharField('Sigla', max_length=10)

class Mercado(models.Model):
    def __str__(self):
        return self.strPar + ' (' + self.fkExchange.strSigla + ')'
    fkExchange = models.ForeignKey(Exchange, on_delete=models.PROTECT, verbose_name='Exchange', related_name='Mercado_fkExchange')
    strPar = models.CharField('Par', max_length=10)

class Ativo(models.Model):
    def __str__(self):
        return self.strSigla
    strSigla = models.CharField('Sigla', max_length=10)
    strNome = models.CharField('Nome', max_length=30)

class Transacao(models.Model):
    def __str__(self):
        return '{} {} para {} {}'.format(self.satAtivoDebito/100000000,self.fkAtivoDebito.strSigla,self.satAtivoCredito/100000000,self.fkAtivoCredito.strSigla)

    class Meta:
        ordering = ('dthrData',)
        verbose_name = 'transação'
        verbose_name_plural = 'transações'

    # Momento
    dthrData = models.DateTimeField('Data', default=timezone.now)
    dthrDataAtualizada = models.DateTimeField('Data Atualizada', default=timezone.now)

    # Mercado
    fkMercado = models.ForeignKey(Mercado, on_delete=models.PROTECT, verbose_name='Mercado', related_name='Transacao_fkMercado')

    # Par de Débito (De)
    fkAtivoDebito = models.ForeignKey(Ativo, on_delete=models.PROTECT, verbose_name='Ativo Debito (De)', related_name='Transacao_fkAtivoDebito')
    satAtivoDebito = models.PositiveBigIntegerField('Valor Débito [sat]',default=0)

    # Par de Crédito (Para)
    fkAtivoCredito = models.ForeignKey(Ativo, on_delete=models.PROTECT, verbose_name='Ativo Crédito (Para)', related_name='Transacao_fkAtivoCredito')
    satAtivoCredito = models.PositiveBigIntegerField('Valor Crédito [sat]',default=0)

    # Cotações
    satUADtoAC = models.PositiveBigIntegerField('1 AD = X ACs.: X = ? sat',default=0) # 1 BRL = X BTCs em sat?
    satUACtoAD = models.PositiveBigIntegerField('1 AC = X ADs.: X = ? sat',default=0) # 1 BTC = X BRLs em sat?        

    # Par Taxa
    fkAtivoTaxa = models.ForeignKey(Ativo, on_delete=models.PROTECT, verbose_name='Ativo Taxa', related_name='Transacao_fkAtivoTaxa',blank = True, null = True)
    satAtivoTaxa = models.PositiveBigIntegerField('Valor Taxa [sat]',default=0)

