from datetime import timezone, datetime, timedelta
from django.contrib import admin
from .models import Exchange, Mercado, Ativo, Transacao

admin.site.register(Exchange)
admin.site.register(Mercado)
admin.site.register(Ativo)

class TransacaoAdmin(admin.ModelAdmin):
    list_display = ('get_data','fkMercado','get_transacao','get_UADtoAC','get_UACtoAD')
    list_display_links = ('get_transacao',)
    list_per_page = 20

    def get_data(self,obj):
        return obj.dthrData.astimezone().strftime('%d/%m/%Y\n %H:%M:%S')
    get_data.short_description = 'Data'
    get_data.admin_order_field = 'dthrData'

    def get_transacao(self,obj):
        return '{:.8f} {} para {:.8f} {}'.format(obj.satAtivoDebito/100000000,obj.fkAtivoDebito.strSigla,obj.satAtivoCredito/100000000,obj.fkAtivoCredito.strSigla)
    get_transacao.short_description = 'Transação'

    def get_UADtoAC(self,obj):
        return '1 {} = {:.8f} {}'.format(obj.fkAtivoDebito.strSigla,obj.satUADtoAC/100000000,obj.fkAtivoCredito.strSigla)
    get_UADtoAC.short_description = 'Cotação Debitado para Creditado'


    def get_UACtoAD(self,obj):
        return '1 {} = {:.8f} {}'.format(obj.fkAtivoCredito.strSigla,obj.satUACtoAD/100000000,obj.fkAtivoDebito.strSigla)
    get_UACtoAD.short_description = 'Cotação Creditado para Debitado'

admin.site.register(Transacao,TransacaoAdmin)

