# coding=utf-8
# https://lazka.github.io/pgi-docs/Gtk-3.0/classes.html
# https://lazka.github.io/pgi-docs/WebKit2-4.0/classes/WebView.html

import os
from os import listdir
from os.path import isfile, join
import sys

import django
import gi
import datetime
import time
import pytz
import os.path
import argparse
import re
import math
import pandas as pd
import configparser
import json

import matplotlib.pyplot as plt
import numpy as np

# Google (Ainda não usei para nada)
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# Plotagem
from math import pi
from bokeh.plotting import figure, show, output_file, output_notebook, save
from bokeh.models import Toggle, BoxAnnotation, CustomJS, PrintfTickFormatter, ColumnDataSource, Range1d, LabelSet, Label, DatetimeTickFormatter, HoverTool
from bokeh.layouts import column, row
from bokeh.models.annotations import Span

from bokeh import events
from bokeh.models import Button, Div, TextInput, Slider


# Bibliotecas Django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'estudos.settings')
django.setup()
from django.utils import timezone
from django.utils.timezone import make_aware
from appCarteira.models import Exchange, Mercado, Ativo, Transacao

# Bibliotecas Gtk
gi.require_version('Gtk','3.0')
from gi.repository import Gtk,Gdk,Pango,GLib, GObject

# Minhas Bibliotecas Externas
import apiBINANCE as apiBNCE

print("")

clNoColor='\033[0m'
clBlack='\033[0;30m'
clRed='\033[0;31m'
clGreen='\033[0;32m'
clOrange='\033[0;33m'
clBlue='\033[0;34m'
clPurple='\033[0;35m'
clCyan='\033[0;36m'
clLight_Gray='\033[0;37m'
clDark_Gray='\033[1;30m'
clLight_Red='\033[1;31m'
clLight_Green='\033[1;32m'
clYellow='\033[1;33m'
clLight_Blue='\033[1;34m'
clLight_Purple='\033[1;35m'
clLight_Cyan='\033[1;36m'
clWhite='\033[1;37m'

dictColors = {
'clBlack':clBlack,
'clRed':clRed,
'clGreen':clGreen,
'clOrange':clOrange,
'clBlue':clBlue,
'clPurple':clPurple,
'clCyan':clCyan,
'clLightGray':clLight_Gray,
'clDarkGray':clDark_Gray,
'clDarkRed':clLight_Red,
'clLightGreen':clLight_Green,
'clYellow':clYellow,
'clLightBlue':clLight_Blue,
'clMediumPurple':clLight_Purple,
'clDarkCyan':clLight_Cyan,
'clWhite':clWhite
}

listPrint = []
lpF = '%Y-%m-%d %H:%M:%S %f'
listColor = ['blue','red','cyan']


def retPrintStr():
    return '\r\n'.join([s for s in listPrint])

def printClear():
    listPrint.clear()

def printCor(clColor,arg):
    print(clColor + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clColor + arg + clNoColor

def printL(arg):
    print(clOrange + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clOrange + arg + clNoColor

def printR(arg):
    print(clRed + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clRed + arg + clNoColor

def printA(arg):
    print(clLight_Blue + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clLight_Blue + arg + clNoColor

def printC(arg):
    print(clLight_Cyan + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clLight_Cyan + arg + clNoColor

def printY(arg):
    print(clYellow + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clYellow + arg + clNoColor

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# Meus Imports
import utils as u

strSep = os.path.sep
strPathDir = os.getcwd()

# Carrega INI
strINIFullFilename = strPathDir  + strSep +  "est.ini"
cfgINI = u.iniciarIni(strINIFullFilename)

# Carrega demais caminhos
strGladeFullFilename = strPathDir  + strSep +  "est.glade"
u.iniRead(cfgINI, 'PATH', 'strGladeFullFilename',strGladeFullFilename)

strTokenFullFilename = strPathDir + strSep +  "token.json"
u.iniRead(cfgINI, 'PATH', 'strTokenFullFilename',strTokenFullFilename)

strCredentialsFullFilename = strPathDir + strSep +  "credentials.json"
u.iniRead(cfgINI, 'PATH', 'strCredentialsFullFilename',strCredentialsFullFilename)

strAutokeyFullFilename = strPathDir + strSep +  "autokeydata.ini"
u.iniRead(cfgINI, 'PATH', 'strAutokeyFullFilename',strAutokeyFullFilename)

strImageFilename = strPathDir + strSep +  "imgPrincipal.png"
u.iniRead(cfgINI, 'PATH', 'strImageFilename',strImageFilename)

strDictCotacoesJsonFilename = strPathDir + strSep +  "dictCotacoes.json"
u.iniRead(cfgINI, 'PATH', 'strDictCotacoesJsonFilename',strDictCotacoesJsonFilename)

strListBRLJsonFilename = strPathDir + strSep +  "listBRL.json"
u.iniRead(cfgINI, 'PATH', 'strListBRLJsonFilename',strListBRLJsonFilename)

strDFAtualJsonFilename = strPathDir + strSep +  "dfAtual.json"
u.iniRead(cfgINI, 'PATH', 'strDFAtualJsonFilename',strDFAtualJsonFilename)

strHTMLFilename = "/home/ronie/prog/alura/estudos/data/BTCBRL.html"
u.iniRead(cfgINI, 'PATH', 'strHTMLFilename',strHTMLFilename)


cfgAuto = configparser.ConfigParser()
# Não vou ler pois sempre vou criar um novo somente para leitura sobrescrevendo o anterior
# cfgAuto.read(strAutokeyFullFilename)
cfgAuto['data'] = {}
cfgAuto['data']['hoje'] = datetime.datetime.today().strftime("%Y%m%d%H%M%S")
with open(strAutokeyFullFilename, 'w') as configfile:
    cfgAuto.write(configfile)

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--developer", required=False,help="Executar como desenvolvedor")
ap.add_argument("-x", "--xxxx", required=False,help="Xxxx")
ap.add_argument("-r", "--run", required=False,help="Run: Onde você vai executar")
args =  vars(ap.parse_args())

if args['developer'] != None:
    print("")
    printR('##### Modo Desenvolvedor #####')
    booDev = True
else:
    print("")
    printY('##### Modo Usuário #####')
    booDev = False

if args['xxxx'] != None:
    booHide = True
else:
    booHide = False

if args['run'] == 'linux':
    printC('>> Alterou execução para Linux')
    u.iniWrite(cfgINI,'geral','run','linux')
elif args['run'] == 'rasp':
    printC('>> Alterou execução para RaspberryPi')
    u.iniWrite(cfgINI,'geral','run','rasp')

# Diz onde estou executando (Linux = True, RaspberrPi = False)
booLinux = u.iniRead(cfgINI,'geral','run','linux') == 'linux'

if booLinux:
    printL('>> Linux')
    gi.require_version('WebKit2', '4.0')
    from gi.repository import WebKit2
else:
    printL('>> RaspberryPi ')


class DialogTransacaoBinance(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Transação Binance")
        self.show_all()

class Manipulador:

    # -----------------------------------------------------
                    # Eventos do Glade
    # -----------------------------------------------------
    def __init__(self):
        self.iniciarGeral()
        # self.iniciarClock()
        # self.iniciarWeb()

    def on_main_window_destroy(self, Window):
        self.sair()

    def sair(self):
        print('Saindo...')
        Gtk.main_quit()

    def iniciarClock(self):
        self.onClockTimer()
        self.startClockTimer()

    def iniciarGeral(self):
        # -----------------------------------
        # Inicializações de Variáveis Globais
        # -----------------------------------
        self.listSTP = ['spPageImagem','spPageGrafico','spPageTransacoes']
        self.activeSTP = 2
        self.listStackRightLabels = {}

        # Parâmetros de Plotagem
        self.dfAtual = None
        self.plot_width = 1500
        self.plot_height = 600
        self.hover_indice_anterior = -1
        self.hover_indice_atual = -1

        self.listStackRightLabels = {
        'Índice':None,
        'Data': None,
        'Valor Abriu':None,
        'Valor Fechou':None,
        'Maior':None,
        'Menor':None,
        'Volume':None,
        'Data Abriu':None,
        'Data Fechou':None,
        }

        # Inicializar o Webview
        if booLinux:
            self.Webview = WebKit2.WebView()
            self.webkitDict = {}
            swPage2 = Builder.get_object("swPage2")
            swPage2.add(self.Webview)
            # self.exemploPlotaImagem()

        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageTransacoes"))

        if booDev:
            self.atualizarDictCotacoes()
        else:
            self.carregarDictCotacoes()

        # Inicia Dialogo
        self.dictComboAtivos = {}
        self.dlgTransacaoBinance_init()

        # Inicializa Tabelas
        self.iniciarTbTransacao()
        self.atualizarTbTransacao()

        self.iniciarTbCarteira()
        self.atualizarTbCarteira()

        self.atualizarTbTransacoesEstimativas()
        self.atualizarTbCarteiraEstimativas()

    # -----------------------------------------------------
                    # dlgTransacaoBinance
    # -----------------------------------------------------
    def dlgMensagem(self, strTextoPrincipal, strTextoSecundario, iconName):
        dlgMensagem: Gtk.MessageDialog = Builder.get_object("dlgMensagem")
        dlgMensagem.props.text = strTextoPrincipal
        dlgMensagem.props.secondary_text = strTextoSecundario
        dlgMensagem.props.icon_name = iconName
        dlgMensagem.show_all()
        dlgMensagem.run()
        dlgMensagem.hide()

    def dlgTransacaoBinance_init(self):
        if len(self.dictComboAtivos) == 0:
            self.dictComboAtivos['dlgTB_comboDe'] = Gtk.ListStore(int, str)
            self.dictComboAtivos['dlgTB_comboPara'] = Gtk.ListStore(int, str)
            self.dictComboAtivos['dlgTB_comboTaxa'] = Gtk.ListStore(int, str)

            qryAtivos = Ativo.objects.all()
            for objAtivo in qryAtivos:
                for d in self.dictComboAtivos:
                    self.dictComboAtivos[d].append([objAtivo.id,'{} ({})'.format(objAtivo.strNome,objAtivo.strSigla)])

            for d in self.dictComboAtivos:
                comboAux = Builder.get_object(d)
                comboAux.set_model(self.dictComboAtivos[d])
                renderer_text = Gtk.CellRendererText()
                comboAux.pack_start(renderer_text, True)
                comboAux.add_attribute(renderer_text, "text", 1)
                comboAux.set_active(0)

    def dlgTransacaoBinance_show(self):
        # https://python-gtk-3-tutorial.readthedocs.io/pt_BR/latest/combobox.html
        dlgTB: Gtk.Dialog = Builder.get_object("dlgTransacaoBinance")

        # Calendário
        dthrHoje = datetime.datetime.now()
        cdCalendar: Gtk.Calendar = Builder.get_object("dlgTB_Calendar")
        cdCalendar.select_month(int(dthrHoje.strftime("%-m"))-1, int(dthrHoje.strftime("%Y")))
        cdCalendar.select_day(int(dthrHoje.strftime("%-d")))

        dlgTB_editDataHora = Builder.get_object("dlgTB_editDataHora")
        dlgTB_editDataHora.set_text(dthrHoje.strftime("%Y-%m-%d %H:%M:%S"))

        # ------------------
        # Inicializações
        # ------------------

        listDlgTB_Campos = ['De','Para','Taxa']

        # Edits
        listDlgTB_Edits = ['dlgTB_edit','dlgTB_editCot']
        for strEdit in listDlgTB_Edits:
            for strCampo in listDlgTB_Campos:
                strNome = '{}{}'.format(strEdit,strCampo)
                dlgTB_editAux = Builder.get_object(strNome)
                dlgTB_editAux.set_text('0,00')

        # Labels
        listDlgTB_Labels = ['dlgTB_labelCotHoje','dlgTB_labelPrevHoje','dlgTB_labelCot']
        for strLabel in listDlgTB_Labels:
            for strCampo in listDlgTB_Campos:
                strNome = '{}{}'.format(strLabel,strCampo)
                dlgTB_labelAux = Builder.get_object(strNome)
                dlgTB_labelAux.set_label('')

        # ------------------
        # Exibe
        # ------------------

        dlgTB.show_all()
        resposta = dlgTB.run()
        dlgTB.hide()

    def on_dlgTB_btnTrocar_clicked(self,button):
        dlgTB_comboDe = Builder.get_object("dlgTB_comboDe")
        indice_active_de = dlgTB_comboDe.get_active()

        dlgTB_comboPara = Builder.get_object("dlgTB_comboPara")
        indice_active_para = dlgTB_comboPara.get_active()

        dlgTB_comboDe.set_active(indice_active_para)
        dlgTB_comboPara.set_active(indice_active_de)

        self.calculaPrevias()
        self.calculaPreviaHoje('De')
        self.calculaPreviaHoje('Para')
        self.calculaPreviaHoje('Taxa')

        self.atualizaDlgTBLabelCot('De')
        self.atualizaDlgTBLabelCot('Para')
        self.atualizaDlgTBLabelCot('Taxa')

    def on_dlgTB_btnAtualizar_clicked(self,button):
        # Teria que pedir para atualizar as cotações antes - Depois eu faço
        self.calculaPreviaHoje('De')
        self.calculaPreviaHoje('Para')
        self.calculaPreviaHoje('Taxa')

        self.atualizaDlgTBLabelCot('De')
        self.atualizaDlgTBLabelCot('Para')
        self.atualizaDlgTBLabelCot('Taxa')

    def on_dlgTB_btnSalvar_clicked(self,button):
        # Debitado
        dlgTB_comboDe = Builder.get_object("dlgTB_comboDe")
        dlgTB_editDe = Builder.get_object("dlgTB_editDe")
        numDe = self.getComboID(dlgTB_comboDe)
        strDe = dlgTB_editDe.get_text()
        valDe = self.getFloatFromStr(strDe)

        # Creditado
        dlgTB_comboPara = Builder.get_object("dlgTB_comboPara")
        dlgTB_editPara = Builder.get_object("dlgTB_editPara")
        numPara = self.getComboID(dlgTB_comboPara)
        strPara = dlgTB_editPara.get_text()
        valPara = self.getFloatFromStr(strPara)

        # valUACtoAD
        dlgTB_editCotDe = Builder.get_object("dlgTB_editCotDe")
        strValUACtoAD = dlgTB_editCotDe.get_text()
        valUACtoAD = self.getFloatFromStr(strValUACtoAD)

        # valUADtoAC
        dlgTB_editCotPara = Builder.get_object("dlgTB_editCotPara")
        strValUADtoAC = dlgTB_editCotPara.get_text()
        valUADtoAC = self.getFloatFromStr(strValUADtoAC)

        # Taxa
        dlgTB_comboTaxa = Builder.get_object("dlgTB_comboTaxa")
        dlgTB_editTaxa = Builder.get_object("dlgTB_editTaxa")
        numTaxa = self.getComboID(dlgTB_comboTaxa)
        strTaxa = dlgTB_editTaxa.get_text()
        valTaxa = self.getFloatFromStr(strTaxa)

        # Datas
        dlgTB_editDataHora = Builder.get_object("dlgTB_editDataHora")
        strDataHora = dlgTB_editDataHora.get_text()
        dthrDataHora = datetime.datetime.strptime(strDataHora, "%Y-%m-%d %H:%M:%S")

        # Preciso descobrir o Mercado e Calcular as Cotacoes
        objM = self.getMercado(numDe,numPara)
        if objM != None:
            objTransacao = Transacao(
                dthrData = dthrDataHora.astimezone(),
                dthrDataAtualizada =  dthrDataHora.astimezone(),
                fkMercado_id = objM.id,
                fkAtivoDebito_id = numDe,
                satAtivoDebito = round(valDe*100000000,0),
                fkAtivoCredito_id = numPara,
                satAtivoCredito = round(valPara*100000000,0),
                satUADtoAC = round(valUADtoAC*100000000,0),
                satUACtoAD = round(valUACtoAD*100000000,0),
                fkAtivoTaxa_id = numTaxa,
                satAtivoTaxa = round(valTaxa*100000000,0)
            )
            objTransacao.save()
            print('Adicionou "{}"'.format(str(objTransacao)))
            self.atualizarTbTransacao()
            self.atualizarTbTransacoesEstimativas()
            self.atualizarTbCarteira()
            self.atualizarTbCarteiraEstimativas()
        else:
            print('Esse mercado não existe!')

    def on_dlgTB_Calendar_day_selected(self,calendar):
        y,m,d = calendar.get_date()
        m = m + 1 # O mes sempre vem com offset em 0
        dtData = datetime.date(y,m,d)
        dthrHoje = datetime.datetime.now()
        dlgTB_editDataHora = Builder.get_object("dlgTB_editDataHora")
        dlgTB_editDataHora.set_text(dtData.strftime("%Y-%m-%d") + ' ' + dthrHoje.strftime("%H:%M:%S"))

    def getMercado(self,numDe,numPara):
        objAtivoDe = Ativo.objects.get(pk=numDe)
        objAtivoPara = Ativo.objects.get(pk=numPara)

        strMercado = objAtivoDe.strSigla + objAtivoPara.strSigla
        qryMercado = Mercado.objects.filter(strPar=strMercado)
        if len(qryMercado) == 0:
            strMercado = objAtivoPara.strSigla + objAtivoDe.strSigla
            qryMercado = Mercado.objects.filter(strPar=strMercado)
            if len(qryMercado) == 0:
                return None
            else:
                return qryMercado[0]
        else:
            return qryMercado[0]

    def getFloatFromStr(self,strStr):
        strAux = ''
        floatRet = 0

        if strStr != '':
            np = strStr.find('.')
            bp = np > 0
            nv = strStr.find(',')
            bv = nv > 0

            if bp and bv:
                # print('p e v')
                if nv > np: #Ex.: 150.000,2456#Ex.: 0.001234
                    strAux = strStr.replace('.','').replace(',','.')
                    try:
                        floatRet = float(strAux)
                    except ValueError:
                        floatRet = 0
                        print('{} not float 1'.format(strAux))
                else: #Ex.: 150,000.2456
                    strAux = strStr.replace(',','')
                    try:
                        floatRet = float(strAux)
                    except ValueError:
                        floatRet = 0
                        print('{} not float 2'.format(strAux))
            elif bp and not bv: #Ex.: 0.001234
                # print('p e !v')
                try:
                    floatRet = float(strStr)
                except ValueError:
                    floatRet = 0
                    print('{} not float 3'.format(strStr))
            elif not bp and bv: #Ex.: 0,001234
                # print('!p e v')
                strAux = strStr.replace(',','.')
                try:
                    floatRet = float(strAux)
                except ValueError:
                    floatRet = 0
                    print('{} not float 4'.format(strAux))
            elif not bp and not bv:
                # print('!p e !v')
                try:
                    floatRet = float(strStr)
                except ValueError:
                    floatRet = 0
                    print('{} not float 5'.format(strStr))
            return floatRet
        else:
            return 0

    # Deixei de exemplo
    def getComboVals(self,combo):
        tree_iter = combo.get_active_iter()
        numID, strNome = -1, ''
        if tree_iter is not None:
            model = combo.get_model()
            numID, strNome = model[tree_iter][:2]
        return numID, strNome

    def getComboID(self,combo):
        tree_iter = combo.get_active_iter()
        numID = -1
        if tree_iter is not None:
            model = combo.get_model()
            numID = model[tree_iter][0]
        return numID

    def calculaPrevias(self):
        dlgTB_editDe = Builder.get_object("dlgTB_editDe")
        strDe = dlgTB_editDe.get_text()
        valDe = self.getFloatFromStr(strDe)

        dlgTB_editPara = Builder.get_object("dlgTB_editPara")
        strPara = dlgTB_editPara.get_text()
        valPara = self.getFloatFromStr(strPara)

        if strDe != '':
            if strPara != '':
                if valDe != 0:
                    if valPara != 0:
                        # Pega Ativos
                        numAtivoDe = self.getComboID(Builder.get_object("dlgTB_comboDe"))
                        objAtivoDe = Ativo.objects.get(pk=numAtivoDe)
                        numAtivoPara = self.getComboID(Builder.get_object("dlgTB_comboPara"))
                        objAtivoPara = Ativo.objects.get(pk=numAtivoPara)
                        # Calcula
                        valUACtoAD = round(valDe/valPara,10) # Esse vai para dlgTB_editCotDe
                        valUADtoAC = round(1/valUACtoAD,10) # Esse vai para dlgTB_editCotPara

                        # Mostra
                        dlgTB_editCotDe = Builder.get_object("dlgTB_editCotDe")
                        dlgTB_editCotDe.set_text('{:>10.10f}'.format(valUACtoAD).replace('.','-').replace(',','.').replace('-',','))
                        dlgTB_editCotPara = Builder.get_object("dlgTB_editCotPara")
                        dlgTB_editCotPara.set_text('{:>10.10f}'.format(valUADtoAC).replace('.','-').replace(',','.').replace('-',','))

    def atualizaDlgTBLabelCot(self,strCampo):
        strNome = 'dlgTB_editCot{}'.format(strCampo)
        dlgTB_editCotCampo = Builder.get_object(strNome)
        strText = dlgTB_editCotCampo.get_text()
        valCampo = self.getFloatFromStr(strText)

        if strText != '':
            if valCampo != 0:
                # Pega Ativos
                numAtivoDe = self.getComboID(Builder.get_object("dlgTB_comboDe"))
                objAtivoDe = Ativo.objects.get(pk=numAtivoDe)
                numAtivoPara = self.getComboID(Builder.get_object("dlgTB_comboPara"))
                objAtivoPara = Ativo.objects.get(pk=numAtivoPara)
                # Atualiza Label
                strNome = 'dlgTB_labelCot{}'.format(strCampo)
                dlgTB_labelCotCampo = Builder.get_object(strNome)

                if strCampo == 'De':
                    strSet = '1 {} = {:>10.10f} {}'.format(objAtivoPara.strSigla,valCampo,objAtivoDe.strSigla).replace('.','-').replace(',','.').replace('-',',')
                else:
                    strSet = '1 {} = {:>10.10f} {}'.format(objAtivoDe.strSigla,valCampo,objAtivoPara.strSigla).replace('.','-').replace(',','.').replace('-',',')

                dlgTB_labelCotCampo.set_markup('<span foreground="cyan">{}</span>'.format(strSet))

    def calculaPreviaHoje(self,strCampo):
        comboAtivo = Builder.get_object('dlgTB_combo{}'.format(strCampo))
        numAtivo = self.getComboID(comboAtivo)
        objAtivo = Ativo.objects.get(pk=numAtivo)

        editAtivo = Builder.get_object('dlgTB_edit{}'.format(strCampo))
        strAtivo = editAtivo.get_text()
        valAtivo = self.getFloatFromStr(strAtivo)

        if strAtivo != '':
            if valAtivo != 0:
                booRet, valCotacao = self.getCotacao(objAtivo.strSigla)
                if booRet:
                    strAux = 'dlgTB_labelCotHoje{}'.format(strCampo)
                    labelCotHoje = Builder.get_object(strAux)
                    labelPrevHoje = Builder.get_object('dlgTB_labelPrevHoje{}'.format(strCampo))

                    # Campo Cotação
                    strCotHoje = '1 {} = R$ {:>10.10f}'.format(objAtivo.strSigla,valCotacao).replace('.','*').replace(',','.').replace('*',',')
                    labelCotHoje.set_markup('<span foreground="yellow">{}</span>'.format(strCotHoje))

                    # Campo Valor Estimado
                    valTotal = valAtivo
                    valEstimado = valTotal*valCotacao
                    strValEstimado = 'R$ {:>10.10f}'.format(valEstimado).replace('.','*').replace(',','.').replace('*',',')
                    labelPrevHoje.set_markup('<span foreground="yellow">{}</span>'.format(strValEstimado))

    def on_dlgTB_editDe_changed(self,entry):
        # Edit De e Para altera as duas Edit Cotações e elas alteram seus labels
        # E só isso
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.calculaPrevias()
        self.calculaPreviaHoje('De')
        return True

    def on_dlgTB_editPara_changed(self,entry):
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.calculaPrevias()
        self.calculaPreviaHoje('Para')
        return True

    def on_dlgTB_editTaxa_changed(self,entry):
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.calculaPreviaHoje('Taxa')
        return True

    def on_dlgTB_editCotDe_changed(self,entry):
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.atualizaDlgTBLabelCot('De')
        return True

    def on_dlgTB_editCotPara_changed(self,entry):
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.atualizaDlgTBLabelCot('Para')
        return True

    def on_dlgTB_editCotTaxa_changed(self,entry):
        value = entry.get_text()
        value = ''.join([c for c in value if (c.isdigit() or c == '.' or c ==',')])
        entry.set_text(value)
        self.atualizaDlgTBLabelCot('Taxa')
        return True



    # -----------------------------------------------------
                    # Menu Principal
    # -----------------------------------------------------

    def on_miOpcoesTBAdd_activate(self,button):
        self.dlgTransacaoBinance_show()


    def on_miExibirTudo_activate(self,button):
        Builder.get_object("stackLeft").show()
        Builder.get_object("stackRight").show()
        Builder.get_object("stackBar").show()

    def on_miEsconderTudo_activate(self,button):
        Builder.get_object("stackLeft").hide()
        Builder.get_object("stackRight").hide()
        Builder.get_object("stackBar").hide()

    def on_miPrincipalImagem_activate(self,button):
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_UP)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageImagem"))

    def on_miPrincipalGrafico_activate(self,button):
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageGrafico"))

    def on_miPrincipalTransacoes_activate(self,button):
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageTransacoes"))


    def on_tvTreeTransacao_row_activated(self,tree_view, path, column):
        if booLinux:
            tvTree = tree_view
            lsList = tvTree.props.model
            treeiter = lsList.get_iter(path)
            numID = lsList.get_value(treeiter, 0)
            objT = Transacao.objects.get(pk=numID)

            # Vai plotar o gráfico
            strOutputFilename = 'data/{}.html'.format(objT.fkMercado.strPar)

            # Só vou carregar da internet se for modo Desenvolvimento
            if booDev:
                self.dfAtual = self.plotBNCEMarketHTML(strMarket=objT.fkMercado.strPar,strTime='1d',numData=5000,htmlFilename=strOutputFilename)
                self.dfAtual.to_json(strDFAtualJsonFilename)
            else:
                self.dfAtual = pd.read_json(strDFAtualJsonFilename)

            # self.Webview.load_uri("https://www.google.com/") # Funciona!!
            # webview.load_html(strHTMLFilename,None)
            strHTML = '<p> Nada </p>'
            with open(strOutputFilename, 'r') as fileHTML:
                strHTML = fileHTML.read()
            self.Webview.load_html(strHTML,None)
            self.Webview.connect("script_dialog", self.on_webview_script_dialog_from_hover)

            Builder.get_object("lbGraficoNome").set_label(objT.fkMercado.strPar)

            # E depois mostrar
            Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)

            if booHide:
                Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageImagem"))
            else:
                Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageGrafico"))

            # Vou carregar na barra os labels por código
            if self.listStackRightLabels['Índice'] == None:
                srGraficoStatus = Builder.get_object("srGraficoStatus")
                for v in self.listStackRightLabels:
                    lbLabel = Gtk.Label(label=v)
                    lbLabel.set_xalign(0.5)
                    lbLabel.set_yalign(0.5)
                    lbLabel.set_justify(Gtk.Justification.CENTER)

                    self.listStackRightLabels[v] = lbLabel
                    srGraficoStatus.pack_start(lbLabel, True, True, 0)
                srGraficoStatus.show_all()

            # Inicia a Tabela de Status
            self.iniciarTbTabela(self.dfAtual)
            self.atualizarTbTabela(self.dfAtual)

            # Mostra a barra direita
            Builder.get_object("stackRight").show()
            Builder.get_object("stackRight").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
            Builder.get_object("stackRight").set_visible_child(Builder.get_object("srGraficoStatus"))

            # Mostra a barra de status com a tabela
            Builder.get_object("stackBar").show()
            Builder.get_object("stackBar").set_transition_type(Gtk.StackTransitionType.SLIDE_UP)
            Builder.get_object("stackBar").set_visible_child(Builder.get_object("pageTabela"))
        else:
            self.dlgMensagem('Informação','Gráfico não disponível no RaspBerryPi','dialog-emblem-default')

    def on_tvTreeTransacao_cursor_changed(self,tree_view):
        tvTree = tree_view
        path, focus_column = tvTree.get_cursor()
        lsList = tvTree.props.model

        if path != None:
            treeiter = lsList.get_iter(path)
            numID = lsList.get_value(treeiter, 0)
            objT = Transacao.objects.get(pk=numID)

            # Seta comboDe
            comboDe = Builder.get_object('dlgTB_comboDe')
            lsListDe = comboDe.props.model
            indice = 0
            for item in lsListDe:
                if objT.fkAtivoDebito.id == item[0]:
                    comboDe.set_active(indice)
                    break
                else:
                    indice = indice + 1

            # Seta comboPara
            comboPara = Builder.get_object('dlgTB_comboPara')
            lsListPara = comboPara.props.model
            indice = 0
            for item in lsListPara:
                if objT.fkAtivoCredito.id == item[0]:
                    comboPara.set_active(indice)
                    break
                else:
                    indice = indice + 1

            # Seta comboTaxa
            comboTaxa = Builder.get_object('dlgTB_comboTaxa')
            lsListTaxa = comboTaxa.props.model
            if objT.fkAtivoTaxa != None:
                indice = 0
                for item in lsListTaxa:
                    if objT.fkAtivoTaxa.id == item[0]:
                        comboTaxa.set_active(indice)
                        break
                    else:
                        indice = indice + 1
            else:
                comboTaxa.set_active(5) # É o BNB

    def on_webview_script_dialog_from_hover(self,web_view, dialog):
        strMsg = dialog.get_message()
        json_acceptable_string = strMsg.replace("'", "\"")
        d = json.loads(json_acceptable_string)
        # print(d['indice'])
        indice = int(d['indice'])
        self.hover_indice_atual = indice

        # Aqui posso fazer contas para apresentar
        # Mas antes quero captar o evento de click

        tvTree = Builder.get_object('tvTreeTabela')
        lsList = tvTree.props.model

        numCor = len(self.tbTabela_dictCols.keys())
        lsList[indice][numCor] = 'red'

        tvTree.set_cursor(indice,None,False)

        if len(self.listStackRightLabels) != 0:
            for v in self.listStackRightLabels:
                lbLabel = self.listStackRightLabels[v]
                if math.isnan(self.dfAtual.iloc[indice]['open_inc']):
                    strCor = 'red'
                else:
                    strCor = 'green'
                if v == 'Índice':
                    lbLabel.set_markup('<span foreground="cyan"><b>Índice</b></span>\n<span foreground="cyan">{}</span>'.format(indice))
                elif v == 'Data':
                    dthrAux = self.dfAtual.iloc[indice]['date']
                    strAux = dthrAux.strftime('%A\n%d/%m/%Y\n%H:%M:%S')
                    lbLabel.set_markup('<span foreground="cyan"><b>Data</b></span>\n<span foreground="cyan">{}</span>'.format(strAux))
                elif v == 'Valor Abriu':
                    lbLabel.set_markup('<span foreground="yellow"><b>Valor Abriu</b></span>\n<span foreground="yellow">R$ {: >10,.8g}</span>'.format(self.dfAtual.iloc[indice]['open']).replace('.','*').replace(',','.').replace('*',','))
                elif v == 'Valor Fechou':
                    lbLabel.set_markup('<span foreground="{}"><b>Valor Fechou</b></span>\n<span foreground="{}">R$ {: >10,.8g}</span>'.format(strCor,strCor,self.dfAtual.iloc[indice]['close']).replace('.','*').replace(',','.').replace('*',','))
                elif v == 'Maior':
                    lbLabel.set_markup('<span foreground="yellow"><b>Maior</b></span>\n<span foreground="yellow">R$ {: >10,.8g}</span>'.format(self.dfAtual.iloc[indice]['high']).replace('.','*').replace(',','.').replace('*',','))
                elif v == 'Menor':
                    lbLabel.set_markup('<span foreground="yellow"><b>Menor</b></span>\n<span foreground="yellow">R$ {: >10,.8g}</span>'.format(self.dfAtual.iloc[indice]['low']).replace('.','*').replace(',','.').replace('*',','))
                elif v == 'Volume':
                    lbLabel.set_markup('<span foreground="yellow"><b>Volume</b></span>\n<span foreground="yellow">{: >10,.8g}</span>'.format(self.dfAtual.iloc[indice]['volume']).replace('.','*').replace(',','.').replace('*',','))
                elif v == 'Data Abriu':
                    #dthrAux = self.dfAtual.iloc[indice]['Open time']
                    #strAux = dthrAux.strftime('%d/%m/%Y\n%H:%M:%S')
                    #lbLabel.set_markup('<span foreground="yellow"><b>Data Abriu</b></span>\n<span foreground="yellow">{}</span>'.format(strAux))
                    pass
                elif v == 'Data Fechou':
                    #dthrAux = self.dfAtual.iloc[indice]['Close time']
                    #strAux = dthrAux.strftime('%d/%m/%Y\n%H:%M:%S')
                    #lbLabel.set_markup('<span foreground="yellow"><b>Data Fechou</b></span>\n<span foreground="yellow">{}</span>'.format(strAux))
                    pass

        # Atualiza Indicador
        if self.hover_indice_anterior != self.hover_indice_atual:
            if self.hover_indice_anterior != -1:
                lsList[self.hover_indice_anterior][numCor] = 'yellow'
            self.hover_indice_anterior = self.hover_indice_atual

        return True



        #Se a linha %K (Azul) cruzar para acima da %D (Vermelho) temos, em geral, a configuração de um call de compra (preço sobe).

        #Por outro lado, se a %K cruzar para baixo da %D temos um sinal de venda. (preço cai)




    # -----------------------------------------------------
                    # Inicializações
    # -----------------------------------------------------
    def iniciarWeb(self):
        # Vai plotar o gráfico
        strM = 'BTCBRL'
        strOutputFilename = 'data/{}.html'.format(strM)

        self.plotBNCEMarketHTML(strMarket=strM,strTime='1d',numData=5000,htmlFilename=strOutputFilename)

        strHTML = '<p> Nada </p>'
        with open(strOutputFilename, 'r') as fileHTML:
            strHTML = fileHTML.read()
        self.Webview.load_html(strHTML,None)
        self.Webview.connect("script_dialog", self.on_webview_script_dialog_from_hover)

        # E depois mostrar
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageGrafico"))

    def iniciarTbTransacao(self):
        # Gera Tabela Principal

        # Cria dictCols
        self.TbTransacao_dictCols = {
        'ID':'N',
        'Data':'T',
        'Mercado':'T',
        'satDebitado':'T',
        'Debitado':'T',
        'satCreditado':'T',
        'Creditado':'T',
        'Cotação':'T',
        'Cotação R$':'T',
        'Valor Estimado':'T',
        'Ganho':'T',
        }
        dictCols = self.TbTransacao_dictCols

        # Cria dCI (dictColsIndex)
        listDictCols = list(self.TbTransacao_dictCols.keys())
        self.TbTransacao_dictColsIndex = {}
        for v in self.TbTransacao_dictCols:
            self.TbTransacao_dictColsIndex[v] = listDictCols.index(v)
        dCI = self.TbTransacao_dictColsIndex


        # Atacha
        tvTree = Builder.get_object('tvTreeTransacao')
        tvTree.props.activate_on_single_click = False

        listAux = []
        colIndex = 0
        for colTitulo in dictCols:
            if dictCols[colTitulo] == 'T':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'N':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'B':
                renderer = Gtk.CellRendererToggle()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, active = colIndex)
            else:
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            column.props.alignment = 0.5
            column.props.max_width = 400
            column.props.min_width = 120

            if (dictCols[colTitulo] == 'T') or (dictCols[colTitulo] == 'N'):
                column.add_attribute(renderer, "foreground", len(dictCols.keys()))
                dCI['corFonte'] = len(dictCols.keys())
                # column.add_attribute(renderer, "background", len(dictCols.keys())+1)
                # dCI['corFundo'] = len(dictCols.keys()) + 1

            tvTree.append_column(column)
            listAux.append(str)
            colIndex += 1

        # Esconde coluna ID
        tvTree.get_column(dCI['ID']).set_visible(False)
        tvTree.get_column(dCI['satDebitado']).set_visible(False)
        tvTree.get_column(dCI['satCreditado']).set_visible(False)

        # Gera Lista
        # Depois preciso descobrir como gerar os argumentos abaixo a partir do numero de colIndex ou de listAux
        lsList = Gtk.ListStore(*[str]*(len(dictCols.keys())+1))
        tvTree.props.model = lsList

    def atualizarTbTransacao(self):
        self.qryTransacao_Todos = Transacao.objects.all().order_by('dthrData')
        Builder.get_object("lbStatusTransacao").set_label(str(len(self.qryTransacao_Todos)) + ' registros')

        # Transações
        tvTreeTransacao = Builder.get_object("tvTreeTransacao")
        lsList = tvTreeTransacao.props.model
        lsList.clear()

        # https://docs.python.org/3/library/string.html#format-string-syntax
        for objT in self.qryTransacao_Todos:
            lsList.append([
                          str(objT.id), # ID
                          objT.dthrData.astimezone().strftime('%d/%m/%Y\n %H:%M:%S'), # Data
                          str(objT.fkMercado), # Mercado
                          str(objT.satAtivoDebito), # satDebitado
                          '{: >10,.8g} {}'.format(objT.satAtivoDebito/100000000,objT.fkAtivoDebito.strSigla).replace('.','-').replace(',','.').replace('-',','), # Debitado
                          str(objT.satAtivoCredito), #satCreditado
                          '{: >10,.8g} {}'.format(objT.satAtivoCredito/100000000,objT.fkAtivoCredito.strSigla).replace('.','-').replace(',','.').replace('-',','), # Creditado
                          '1 {} = {: >10,.8g} {}'.format(objT.fkAtivoCredito.strSigla,objT.satUACtoAD/100000000,objT.fkAtivoDebito.strSigla).replace('.','-').replace(',','.').replace('-',','), # Cotação
                          '---', # Cotação R$
                          '---', # Valor Estimado R$
                          '---', # Ganho
                          'cyan']) # Cor
        if len(self.qryTransacao_Todos) > 0:
            tvTreeTransacao.set_cursor(0,None,False)
            self.on_tvTreeTransacao_cursor_changed(tvTreeTransacao)

    def iniciarTbCarteira(self):
        # Gera Tabela Principal

        # Cria dictCols
        self.tbCarteira_dictCols = {
        'Ativo_ID':'N',
        'Ativo':'T',
        'Total':'T',
        'Sigla':'T',
        'Cotação R$':'T',
        'Valor Estimado':'T',
        }
        dictCols = self.tbCarteira_dictCols

        # Cria dCI (dictColsIndex)
        listDictCols = list(self.tbCarteira_dictCols.keys())
        self.tbCarteira_dictColsIndex = {}
        for v in self.tbCarteira_dictCols:
            self.tbCarteira_dictColsIndex[v] = listDictCols.index(v)
        dCI = self.tbCarteira_dictColsIndex

        # Atacha
        tvTree = Builder.get_object('tvTreeCarteira')
        tvTree.props.activate_on_single_click = False

        listAux = []
        colIndex = 0
        for colTitulo in dictCols:
            if dictCols[colTitulo] == 'T':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'N':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'B':
                renderer = Gtk.CellRendererToggle()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, active = colIndex)
            else:
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            column.props.alignment = 0.5
            column.props.max_width = 400
            column.props.min_width = 120

            if (dictCols[colTitulo] == 'T') or (dictCols[colTitulo] == 'N'):
                column.add_attribute(renderer, "foreground", len(dictCols.keys()))
                dCI['corFonte'] = len(dictCols.keys())
                # column.add_attribute(renderer, "background", len(dictCols.keys())+1)
                # dCI['corFundo'] = len(dictCols.keys()) + 1


            tvTree.append_column(column)
            listAux.append(str)
            colIndex += 1

        # Esconde coluna ID
        tvTree.get_column(0).set_visible(False)

        # Gera Lista
        # Depois preciso descobrir como gerar os argumentos abaixo a partir do numero de colIndex ou de listAux
        lsList = Gtk.ListStore(*[str]*(len(dictCols.keys())+1)) # 1 str adicional para a cor # https://stackoverflow.com/questions/6819271/change-gtkliststore-model-dynamically
        tvTree.props.model = lsList

    def atualizarTbCarteira(self):
        self.qryCarteira_Todos = Transacao.objects.all().order_by('dthrData')

        # Carteira
        tvTreeCarteira = Builder.get_object("tvTreeCarteira")
        lsList = tvTreeCarteira.props.model
        lsList.clear()

        # Aqui eu gero a estrutura de dados que vai conter a moeda e suas quantidades
        dictAtivo = {}
        for objT in self.qryCarteira_Todos:
            # Débito
            if objT.fkAtivoDebito.id in dictAtivo:
                dictAtivo[objT.fkAtivoDebito.id] -= objT.satAtivoDebito
            else:
                dictAtivo[objT.fkAtivoDebito.id] = (-1) * objT.satAtivoDebito

            # Crédito
            if objT.fkAtivoCredito.id in dictAtivo:
                dictAtivo[objT.fkAtivoCredito.id] += objT.satAtivoCredito
            else:
                dictAtivo[objT.fkAtivoCredito.id] = objT.satAtivoCredito
        Builder.get_object("lbStatusCarteira").set_label(str(len(dictAtivo)) + ' ativos')

        # Popula Tabela
        for d in dictAtivo:
            objAtivo = Ativo.objects.get(pk=d)

            # Cor
            if dictAtivo[d] < 0 :
                strClBackgroundColor = 'red'
            else:
                strClBackgroundColor = 'cyan'

            # Insere
            lsList.append([
                str(objAtivo.id), # Ativo_ID
                objAtivo.strNome, # Ativo
                '{: >10,.8g}'.format(dictAtivo[d]/100000000).replace('.','*').replace(',','.').replace('*',','), # Total
                objAtivo.strSigla, # Sigla
                '---', # Cotação R$
                '---', # Valor Estimado
                strClBackgroundColor]) # Cor

    def atualizarDictCotacoes(self):
        printL('>> Atualizando dicionários...')
        printL('')
        self.df_BRL = apiBNCE.listMarketThatContainsStr('BRL')
        self.listBRL = list(self.df_BRL)
        with open(strListBRLJsonFilename, "w") as outfile:
            json.dump(self.listBRL, outfile)

        self.dictCotacoes = {}
        qryAtivos = Ativo.objects.all()
        for objAtivo in qryAtivos:
            if objAtivo.strSigla != 'BRL':
                strMarket = '{}BRL'.format(objAtivo.strSigla)
                if strMarket in self.listBRL:
                    # print('Mercado {} existe!'.format(strMarket))
                    df,lst = apiBNCE.getKlines(strMarket,'1m',1)
                    # Conversões
                    df['open'] = pd.to_numeric(df['open'])
                    df['high'] = pd.to_numeric(df['high'])
                    df['low'] = pd.to_numeric(df['low'])
                    df['close'] = pd.to_numeric(df['close'])
                    df['volume'] = pd.to_numeric(df['volume'])
                    df['Quote asset volume'] = pd.to_numeric(df['Quote asset volume'])
                    df['Taker buy base asset volume'] = pd.to_numeric(df['Taker buy base asset volume'])
                    df['Taker buy quote asset volume'] = pd.to_numeric(df['Taker buy quote asset volume'])
                    df['Ignore'] = pd.to_numeric(df['Ignore'])
                    # Converte colunas datetime para tipo numérico
                    df['Open time'] = pd.to_datetime(df['Open time'], unit='ms')
                    df['Close time'] = pd.to_datetime(df['Close time'], unit='ms')
                    df['date'] = df['Close time']

                    # Cotacao
                    valCotacao = df.iloc[0]['close']
                    self.dictCotacoes[strMarket] = valCotacao
                else:
                    pass
                    # print('Mercado {} não existe!'.format(strMarket))
                    # Preciso escrever uma forma de converter indiretamente depois (talvez passando por BTC)
            else:
                # print('Real para Real')
                strMarket = 'BRLBRL'
                valCotacao = 1
                self.dictCotacoes[strMarket] = valCotacao
        # Salva como arquivo JSON para acessar depois Offline
        with open(strDictCotacoesJsonFilename, "w") as outfile:
            json.dump(self.dictCotacoes, outfile)

    def carregarDictCotacoes(self):
        printL('>> Carregando dicionários...')
        printL('')
        # listBRL
        self.listBRL = []
        with open(strListBRLJsonFilename) as json_file:
            self.listBRL = json.load(json_file)
        # print(self.listBRL)

        # dictCotacoes
        self.dictCotacoes = {}
        with open(strDictCotacoesJsonFilename) as json_file:
            self.dictCotacoes = json.load(json_file)
        # print(self.dictCotacoes)

    def getCotacao(self,strAtivoSigla):
        if strAtivoSigla != 'BRL':
            strMarket = '{}BRL'.format(strAtivoSigla)
            if strMarket in self.listBRL:
                valCotacao = self.dictCotacoes[strMarket]
                return True, valCotacao
            else:
                return False, 0
                # Preciso escrever uma forma de converter indiretamente depois (talvez passando por BTC)
        else:
            return True, 1

    def atualizarTbCarteiraEstimativas(self):
        # Pega Dicionário de Colunas
        dictCols = self.tbCarteira_dictCols
        dCI = self.tbCarteira_dictColsIndex

        tvTreeCarteira = Builder.get_object("tvTreeCarteira")
        lsList = tvTreeCarteira.props.model

        valEstimadoTotal = 0
        for row in lsList:
            objAtivo = Ativo.objects.get(pk=row[dCI['Ativo_ID']])

            # Campo Sigla
            row[dCI['Sigla']] = objAtivo.strSigla

            booRet, valCotacao = self.getCotacao(objAtivo.strSigla)

            if booRet:
                # Campo Cotação
                row[dCI['Cotação R$']] = '1 {} = {: >10,.8g} BRL'.format(objAtivo.strSigla,valCotacao).replace('.','*').replace(',','.').replace('*',',')

                # Campo Valor Estimado
                valTotal = float(str(row[dCI['Total']]).replace('.','').replace(',','.'))
                valEstimado = valTotal*valCotacao
                row[dCI['Valor Estimado']] = 'R$ {: >10,.8g}'.format(valEstimado).replace('.','*').replace(',','.').replace('*',',')
                valEstimadoTotal += valEstimado
            else:
                # Campo Cotação
                row[dCI['Cotação R$']] = '---'

                # Campo Valor Estimado
                row[dCI['Valor Estimado']] = '---'
        Builder.get_object("lbStatusCarteira").set_label(str(len(lsList)) + ' ativos R$ {: >10,.8g}'.format(valEstimadoTotal).replace('.','*').replace(',','.').replace('*',','))

    def atualizarTbTransacoesEstimativas(self):
        # Pega Dicionário de Colunas
        dictCols = self.TbTransacao_dictCols
        dCI = self.TbTransacao_dictColsIndex

        tvTreeTransacao = Builder.get_object('tvTreeTransacao')
        lsList = tvTreeTransacao.props.model


        valGanhoTotal = 0
        for row in lsList:
            objTransacao = Transacao.objects.get(pk=row[dCI['ID']])
            objAtivo = objTransacao.fkAtivoCredito

            booRet, valCotacao = self.getCotacao(objAtivo.strSigla)
            if booRet:
                # Campo Cotação
                row[dCI['Cotação R$']] = '1 {} = {: >10,.8g} BRL'.format(objAtivo.strSigla,valCotacao).replace('.','*').replace(',','.').replace('*',',')

                # Campo Valor Estimado
                valCreditado = float(str(row[dCI['satCreditado']]).replace('.','').replace(',','.'))/100000000
                valEstimado = valCreditado*valCotacao
                row[dCI['Valor Estimado']] = 'R$ {: >10,.8g}'.format(valEstimado).replace('.','*').replace(',','.').replace('*',',')

                # Campo Ganho
                valDebitado = float(str(row[dCI['satDebitado']]).replace('.','').replace(',','.'))/100000000
                valGanho = valEstimado - valDebitado
                row[dCI['Ganho']] = 'R$ {: >10,.8g}'.format(valGanho).replace('.','*').replace(',','.').replace('*',',')
                valGanhoTotal += valGanho

                # Cor
                if valGanho < 0:
                    row[dCI['corFonte']] = 'red'
            else:
                # Campo Cotação
                row[dCI['Cotação R$']] = '---'

                # Campo Valor Estimado
                row[dCI['Valor Estimado']] = '---'

        Builder.get_object("lbStatusTransacao").set_label(str(len(lsList)) + ' transações R$ {: >10,.8g}'.format(valGanhoTotal).replace('.','*').replace(',','.').replace('*',','))

    def iniciarTbTabela(self,df):
        # Gera Tabela Para DF

        # Cria dictCols
        listAuxDfCols = df.columns.values.tolist()
        self.tbTabela_dictCols = {}
        self.tbTabela_dictCols['Indice'] = 'N'
        for col in listAuxDfCols:
            self.tbTabela_dictCols[col.replace('_','-')] = 'T'

        dictCols = self.tbTabela_dictCols

        # Cria dCI (dictColsIndex)
        listDictCols = list(self.tbTabela_dictCols.keys())
        self.tbTabela_dictColsIndex = {}
        for v in self.tbTabela_dictCols:
            self.tbTabela_dictColsIndex[v] = listDictCols.index(v)
        dCI = self.tbTabela_dictColsIndex


        # Atacha
        tvTree = Builder.get_object('tvTreeTabela')
        tvTree.props.activate_on_single_click = False

        listAux = []
        colIndex = 0
        for colTitulo in dictCols:
            if dictCols[colTitulo] == 'T':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn('{}\n{}'.format(str(colIndex),colTitulo), renderer, text = colIndex)
            elif dictCols[colTitulo] == 'N':
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn('{}\n{}'.format(str(colIndex),colTitulo), renderer, text = colIndex)
            elif dictCols[colTitulo] == 'B':
                renderer = Gtk.CellRendererToggle()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn('{}\n{}'.format(str(colIndex),colTitulo), renderer, active = colIndex)
            else:
                renderer = Gtk.CellRendererText()
                renderer.set_alignment(0.5,0.5)
                column = Gtk.TreeViewColumn('{}\n{}'.format(str(colIndex),colTitulo), renderer, text = colIndex)
            column.props.alignment = 0.5
            column.props.max_width = 400
            column.props.min_width = 50

            if (dictCols[colTitulo] == 'T') or (dictCols[colTitulo] == 'N'):
                column.add_attribute(renderer, "foreground", len(dictCols.keys()))
                dCI['corFonte'] = len(dictCols.keys())
                # column.add_attribute(renderer, "background", len(dictCols.keys())+1)
                # dCI['corFundo'] = len(dictCols.keys()) + 1

            tvTree.append_column(column)
            listAux.append(str)
            colIndex += 1

        listToHide = [2,3,4,6,8,9,10,11,12,14,15,16,17,18,19,20,26,27,28,29,30,31]
        #for col in listToHide:
        #    tvTree.get_column(col).set_visible(False)

        # Gera Lista
        # Depois preciso descobrir como gerar os argumentos abaixo a partir do numero de colIndex ou de listAux
        lsList = Gtk.ListStore(*[str]*(len(dictCols.keys())+1))
        tvTree.props.model = lsList

    def atualizarTbTabela(self,df):
        # Tabela
        tvTreeTabela = Builder.get_object("tvTreeTabela")
        lsList = tvTreeTabela.props.model
        lsList.clear()

        listAuxDfCols = df.columns.values.tolist()
        for i in range(0,len(df)):
            listRet = []
            listRet.append(str(i))
            for col in listAuxDfCols:
                strRet = ''
                strCor = 'yellow'
                strTipo = str(type(df.iloc[i][col]))
                if strTipo == "<class 'pandas._libs.tslibs.timestamps.Timestamp'>":
                    dthrAux = df.iloc[i][col]
                    strRet = dthrAux.strftime('%A\n%d/%m/%Y\n%H:%M:%S')
                    strCor = 'orange'
                elif strTipo == "<class 'numpy.float64'>":
                    strRet = '{: >10,.8g}'.format(df.iloc[i][col]).replace('.','*').replace(',','.').replace('*',',')
                    strCor = 'yellow'
                elif strTipo == "<class 'numpy.int64'>":
                    strRet = '{: >10,.8g}'.format(df.iloc[i][col]).replace('.','*').replace(',','.').replace('*',',')
                    strCor = 'cyan'
                elif strTipo == "<class 'pandas._libs.tslibs.nattype.NaTType'>":
                    strRet = 'NaN'
                    strCor = 'red'
                listRet.append(strRet)
            listRet.append('yellow')
            lsList.append(listRet)

    # -----------------------------------------------------
                    # Plotagem de Gráficos
    # -----------------------------------------------------

    def plotBNCEMarketHTML(self,strMarket='ETHBTC',strTime='1d',numData=500,htmlFilename=''):
        df,lst = apiBNCE.getKlines(strMarket,strTime,numData)
        # Convertes colunas numéricas para tipo numérico
        df['open'] = pd.to_numeric(df['open'])
        df['high'] = pd.to_numeric(df['high'])
        df['low'] = pd.to_numeric(df['low'])
        df['close'] = pd.to_numeric(df['close'])
        df['volume'] = pd.to_numeric(df['volume'])
        df['Quote asset volume'] = pd.to_numeric(df['Quote asset volume'])
        df['Taker buy base asset volume'] = pd.to_numeric(df['Taker buy base asset volume'])
        df['Taker buy quote asset volume'] = pd.to_numeric(df['Taker buy quote asset volume'])
        df['Ignore'] = pd.to_numeric(df['Ignore'])
        # Converte colunas datetime para tipo numérico
        df['Open time'] = pd.to_datetime(df['Open time'], unit='ms')
        df['Close time'] = pd.to_datetime(df['Close time'], unit='ms')
        df['date'] = df['Close time']

        # Processamento
        #http://www.learndatasci.com/python-finance-part-3-moving-average-trading-strategy/
        df['SMA21'] = df.close.rolling(window=21).mean()
        df['SMA50'] = df.close.rolling(window=50).mean()
        df['SMA200'] = df.close.rolling(window=200).mean()
        df['EMA8'] = df.close.ewm(span=8,adjust=False).mean()
        df['EMA35'] = df.close.ewm(span=35,adjust=False).mean()

        srLen = int(len(df.close)/10)
        df['SUPORT'] = df.high.rolling(window=srLen).min()
        df['RESIST'] = df.low.rolling(window=srLen).max()

        # Gráficos Estocásticos
        #https://www.investopedia.com/terms/s/stochasticoscillator.asp
        sto_K_param_P = 14
        df['sto_LPclose'] = df.close.rolling(window = sto_K_param_P).min()
        df['sto_LP'] = df.low.rolling(window = sto_K_param_P).min()
        df['sto_HP'] = df.high.rolling(window = sto_K_param_P).max()

        df['sto_K'] = 100*((df['close'] - df['sto_LPclose'])/(df['sto_HP'] - df['sto_LP']))
        df['sto_D'] = df.sto_K.rolling(window=3).mean()

        # Gráficos de Candlestick
        inc = df.close > df.open
        dec = df.open > df.close
        df['date_inc'] = df.date[inc]
        df['date_dec'] = df.date[dec]
        df['open_inc'] = df.open[inc]
        df['open_dec'] = df.open[dec]
        df['close_inc'] = df.close[inc]
        df['close_dec'] = df.close[dec]


        # #######################
        # Detecção de Pontos de Compra e Venda Usando os Indicadores Estocásticos
        # #######################

        self.plot_StoCompraVendaGlyph_MaxSize = 30

        # ----------------
        # Pontos de Venda
        # ----------------
        listAux = []
        for i in range(0,len(df)-1):
            ret = 0
            if df.iloc[i]['sto_K'] > df.iloc[i]['sto_D']:
                if df.iloc[i+1]['sto_K'] < df.iloc[i+1]['sto_D']:
                    ret = 1
            listAux.append(ret)
        listAux.append(0)

        df['sto_venda'] = 0
        df['sto_venda'] = listAux
        df['sto_venda_val'] = df['sto_venda']*df['close']

        sto_venda_x = []
        sto_venda_y = []
        sto_venda_size = []
        for i in range(0,len(df)):
            if df.iloc[i]['sto_venda_val'] > 0:
                sto_venda_x.append(df.iloc[i]['date'])
                sto_venda_y.append(df.iloc[i]['sto_venda_val'])
                auxK = df.iloc[i]['sto_K']
                if auxK >= 51:
                    auxY = 2*auxK - 100
                    auxY = math.ceil((auxY*self.plot_StoCompraVendaGlyph_MaxSize)/100)
                    auxY = auxY + 20
                    sto_venda_size.append(auxY)
                elif auxK <= 49:
                    auxY = 100 - 2*auxK
                    auxY = math.ceil((auxY*self.plot_StoCompraVendaGlyph_MaxSize)/100)
                    auxY = auxY + 20
                    sto_venda_size.append(auxY)
                else:
                    sto_venda_size.append(20)

        # ----------------
        # Pontos de Compra
        # ----------------

        listAux = []
        for i in range(0,len(df)-1):
            ret = 0
            if df.iloc[i]['sto_K'] < df.iloc[i]['sto_D']:
                if df.iloc[i+1]['sto_K'] > df.iloc[i+1]['sto_D']:
                    ret = 1
            listAux.append(ret)
        listAux.append(0)

        df['sto_compra'] = 0
        df['sto_compra'] = listAux
        df['sto_compra_val'] = df['sto_compra']*df['close']

        sto_compra_x = []
        sto_compra_y = []
        sto_compra_size = []
        for i in range(0,len(df)):
            if df.iloc[i]['sto_compra_val'] > 0:
                sto_compra_x.append(df.iloc[i]['date'])
                sto_compra_y.append(df.iloc[i]['sto_compra_val'])
                auxK = df.iloc[i]['sto_K']
                if auxK >= 51:
                    auxY = 2*auxK - 100
                    auxY = math.ceil((auxY*self.plot_StoCompraVendaGlyph_MaxSize)/100)
                    auxY = auxY + 20
                    sto_compra_size.append(auxY)
                elif auxK <= 49:
                    auxY = 100 - 2*auxK
                    auxY = math.ceil((auxY*self.plot_StoCompraVendaGlyph_MaxSize)/100)
                    auxY = auxY + 20
                    sto_compra_size.append(auxY)
                else:
                    sto_compra_size.append(20)
                # print('{} = {}'.format(i,sto_compra_size[len(sto_compra_size)-1]))

        # -------------
        # Parâmetro W
        # -------------

        if strTime == '1m':
            w = 1*60*1000
        elif strTime =='3m':
            w = 3*60*1000
        elif strTime =='5m':
            w = 5*60*1000
        elif strTime =='15m':
            w = 15*60*1000
        elif strTime =='30m':
            w = 30*60*1000
        elif strTime =='1h':
            w = 1*60*60*1000
        elif strTime =='2h':
            w = 2*60*60*1000
        elif strTime =='4h':
            w = 4*60*60*1000
        elif strTime =='6h':
            w = 6*60*60*1000
        elif strTime =='8h':
            w = 8*60*60*1000
        elif strTime =='12h':
            w = 12*60*60*1000
        elif strTime =='1d':
            w = 1*24*60*60*1000
        elif strTime =='3d':
            w = 3*24*60*60*1000
        elif strTime =='1w':
            w = 7*24*60*60*1000
        elif strTime =='1M':
            w = 30*24*60*60*1000

        # Plotagem
        source = ColumnDataSource(df)

        TOOLS = "pan,wheel_zoom,box_zoom,reset,save,crosshair"
        p = figure(x_axis_type="datetime", tools=TOOLS, plot_width=self.plot_width, plot_height=self.plot_height, active_scroll = "wheel_zoom")

        p.xaxis.major_label_orientation = pi/4
        p.grid.grid_line_alpha=0.3


        ###########################
        # Gráfico de Candlestick
        ###########################

        booCandlestick = True
        if booCandlestick:
            plot_candleHL = p.segment(x0='date', y0='high', x1='date', y1 = 'low', color="red",legend_label='HL',source=source)
            plot_candleGreen = p.vbar(x='date_inc', width=w, top='open_inc', bottom='close_inc', fill_color="#31BE3A", line_color="black",legend_label='G',source=source)
            plot_candleRed = p.vbar(x='date_dec', width=w, top='open_dec', bottom='close_dec', fill_color="#F2583E", line_color="black",legend_label='R',source=source)

        ###########################
        # Gráficos Lineares
        ###########################

        booClosingPrice = True
        if booClosingPrice:
            plot_closingPrice = p.line(x = 'date',y='close',line_color="black",line_width=2,line_alpha=0.8,line_dash='solid',legend_label="Close Price",muted_color='blue', muted_alpha=0.2, source=source)

        booSuportResist = True
        if booSuportResist:
            plot_SUPORT = p.line(x = 'date',y='SUPORT',line_color="blue",line_width=2,line_alpha=0.8,line_dash='solid',legend_label="SUPORT",muted_color='blue', muted_alpha=0.2, source=source)
            plot_RESIST = p.line(x = 'date',y='RESIST',line_color="red",line_width=2,line_alpha=0.8,line_dash='solid',legend_label="RESIST",muted_color='blue', muted_alpha=0.2, source=source)


        ###########################
        # Médias Móveis
        ###########################

        booMediasMoveis = True
        if booMediasMoveis:
            plot_SMA21 = p.line(x = 'date',y = 'SMA21',line_color="gray",line_width=2,line_alpha=0.8,line_dash='dashed',legend_label="SMA 21",muted_color='gray', muted_alpha=0.1, source=source)
            plot_SMA50 = p.line(x = 'date',y = 'SMA50',line_color="blue",line_width=2,line_alpha=0.8,line_dash='dashed',legend_label="SMA 50",muted_color='gray', muted_alpha=0.1, source=source)
            plot_SMA200 = p.line(x = 'date',y = 'SMA200',line_color="red",line_width=2,line_alpha=0.8,line_dash='dashed',legend_label="SMA 200",muted_color='gray', muted_alpha=0.1, source=source)
            plot_EMA8 = p.line(x = 'date',y = 'EMA8',line_color="orange",line_width=2,line_alpha=0.8,line_dash='dashed',legend_label="EMA 8",muted_color='gray', muted_alpha=0.1, source=source)
            plot_EMA35 = p.line(x = 'date',y = 'EMA35',line_color="purple",line_width=2,line_alpha=0.8,line_dash='dashed',legend_label="EMA 35",muted_color='gray', muted_alpha=0.1, source=source)


        ###########################
        # Posições
        ###########################

        # https://docs.bokeh.org/en/latest/docs/user_guide/styling.html#userguide-styling-line-properties

        qryT = Transacao.objects.filter(fkMercado__strPar=strMarket)
        i = 0
        x0 = df.iloc[0]['date']
        x0 = x0.tz_localize('utc')
        for objT in qryT:
            xx = objT.dthrDataAtualizada.astimezone()
            yy = objT.satUACtoAD/100000000

            # Posição Comprada
            posPrecoSpan = Span(location=yy, dimension='width', line_color=listColor[i % len(listColor)], line_width=2, line_dash='dashed')
            p.add_layout(posPrecoSpan)

            # Só marca se estiver dentro da menor data
            if xx > x0:
                posPrecoLabel = Label(x=xx, y=yy, y_offset=15, x_offset=15, text=str(yy), text_baseline="middle")
                p.add_layout(posPrecoLabel)
                posPrecoCirc = p.circle(x=xx,y=yy, size=30, line_color=listColor[(i + 1) % len(listColor)], fill_color=listColor[i % len(listColor)], fill_alpha=0.5)
            i = i + 1

        ###########################
        # Atributos Gerais de Todos os Gráficos
        ###########################

        p.title.text = strMarket + '-' + strTime + '-' + str(numData)
        if htmlFilename == '':
            htmlFilename = p.title.text + '.html'
        else:
            if htmlFilename.find('.html') < 0:
                htmlFilename = htmlFilename + '.html'

        # Eixo X - Datas
        p.xaxis.axis_label = 'Tempo'
        p.xgrid.grid_line_alpha = 0.6
        p.xgrid.grid_line_dash = [6, 4]

        #listAux = ["%d%b/%y-%Hh%Mm"]

        #p.xaxis.formatter=DatetimeTickFormatter(
        #        hours=listAux,
        #        days=listAux,
        #        months=listAux,
        #        years=listAux,
        #    )

        # Eixo Y - Valores
        p.yaxis.axis_label = 'Preço'
        p.yaxis.minor_tick_in = -3
        p.yaxis.minor_tick_out = 8
        p.yaxis[0].formatter = PrintfTickFormatter(format="%8.8f")

        p.ygrid.band_fill_color="olive"
        p.ygrid.band_fill_alpha = 0.1
        p.ygrid.grid_line_alpha = 0.6
        p.ygrid.grid_line_dash = [6, 4]

        # Legendas
        p.legend.location = "top_right"
        p.legend.click_policy="hide"

        ##############################################
        # JavaScript
        ##############################################

        booJava = False

        def display_event(div, attributes=[], style = 'float:left;clear:left;font_size=13px'):
            "Build a suitable CustomJS to display the current event in the div model."
            return CustomJS(args=dict(div=div), code="""
                const attrs = %s;
                const args = [];
                const args2 = [];
                for (let i = 0; i<attrs.length; i++) {
                    args.push(attrs[i] + '=' + Number(cb_obj[attrs[i]]).toFixed(2));
                    args2.push("'" + attrs[i] + "'" + ':' + Number(cb_obj[attrs[i]]).toFixed(2));
                }
                const line = "<span style=%r><b>" + cb_obj.event_name + "</b>(" + args.join(", ") + ")</span>\\n";
                const line2 = "{'event':'" + cb_obj.event_name + "'," + args2.join(",") + "}\\n";
                window.alert(line2);
            """ % (attributes, style))

        if booJava:
            div = Div(width=400, height=p.height, height_policy="fixed")
            button = Button(label="Button", button_type="success")
            layout = column(button, row(p, div))

            ## Events with no attributes
            button.js_on_event(events.ButtonClick, display_event(div)) # Button click
            p.js_on_event(events.LODStart, display_event(div))         # Start of LOD display
            p.js_on_event(events.LODEnd, display_event(div))           # End of LOD display

            ## Events with attributes
            point_attributes = ['x', 'y', 'sx', 'sy']                  # Point events
            wheel_attributes = point_attributes + ['delta']            # Mouse wheel event
            pan_attributes = point_attributes + ['delta_x', 'delta_y'] # Pan event
            pinch_attributes = point_attributes + ['scale']            # Pinch event

            point_events = [
                events.Tap, events.DoubleTap, events.Press, events.PressUp,
                events.MouseMove, events.MouseEnter, events.MouseLeave,
                events.PanStart, events.PanEnd, events.PinchStart, events.PinchEnd,
            ]

            for event in point_events:
                p.js_on_event(event, display_event(div, attributes=point_attributes))

            p.js_on_event(events.MouseWheel, display_event(div, attributes=wheel_attributes))
            p.js_on_event(events.Pan,        display_event(div, attributes=pan_attributes))
            p.js_on_event(events.Pinch,      display_event(div, attributes=pinch_attributes))


        # --------------------------------
                    # Hover
        # --------------------------------

        # https://docs.bokeh.org/en/latest/docs/reference/colors.html
        # https://docs.bokeh.org/en/2.4.1/docs/reference/models/glyphs/scatter.html

        circulo_source = ColumnDataSource({'cx': [], 'cy': []})
        circulo_plot = p.circle_x(x='cx',y='cy', size=50, line_color="black", fill_color="red", fill_alpha=0.5, source=circulo_source)

        hover_code = """
        const indices = cb_data.index.line_indices;
        for (let i = 0; i < indices.length; i++) {
            const start = indices[i];
            window.alert("{'indice':" + start + "}");
            circulo_source.data['cx'] = [source.data['date'][start]];
            circulo_source.data['cy'] = [source.data['close'][start]];
            circulo_source.change.emit();
        }

        """

        # window.alert("Print '" + cb_obj.tooltips + "'");

        hover_callback = CustomJS(args={'source': source,'circulo_source':circulo_source}, code=hover_code)

        #hover = HoverTool(
        #    tooltips=[
        #        ("Indice", "$index"),
        #        ("Valor", "@close{0.000}"),
        #        ("Data", "@date"),
        #        ("Volume", "@volume"),
        #        ("Open time","@{Open time}")
        #    ],
        #    formatters={
        #        'Valor' : 'printf',
        #        'Data'  : 'datetime',
        #        'Volume': 'printf',
        #        'Open time': 'datetime',
        #    },
        #    mode='mouse',
        #    callback=hover_callback
        #)

        hover = HoverTool(
            tooltips=None,
            formatters={
                'Valor' : 'printf',
                'Data'  : 'datetime',
                'Volume': 'printf',
                'Open time': 'datetime',
            },
            mode='mouse',
            callback=hover_callback
        )


        p.add_tools(hover)


        ##############################################
        # Indicadores de Compra e Venda Estocásticos
        ##############################################

        sto_venda_source = ColumnDataSource({'cx': sto_venda_x, 'cy': sto_venda_y, 'cs': sto_venda_size})
        sto_venda_plot = p.inverted_triangle(x='cx',y='cy', size='cs', line_color="teal", fill_color="gold", fill_alpha=0.5,legend_label='STO Venda', source=sto_venda_source)

        sto_compra_source = ColumnDataSource({'cx': sto_compra_x, 'cy': sto_compra_y, 'cs': sto_compra_size})
        sto_compra_plot = p.triangle(x='cx',y='cy', size='cs', line_color="teal", fill_color="royalblue", fill_alpha=0.5,legend_label='STO Compra', source=sto_compra_source)

        ##############################################
        # Figura para Gráficos Estocásticos
        ##############################################

        # https://www.bussoladoinvestidor.com.br/oscilador-estocastico/

        booEstocasticos = True
        if booEstocasticos:
            q = figure(x_axis_type="datetime", tools=TOOLS, plot_width=self.plot_width,x_range=p.x_range, plot_height= int(0.4 * p.plot_height))
            q.xaxis.major_label_orientation = pi/4
            q.grid.grid_line_alpha=0.3
            q.add_tools(hover)

            plot_stoK = q.line(x = 'date',y = 'sto_K',line_color="blue",line_width=2,line_alpha=0.8,line_dash='solid',legend_label="STO K",muted_color='gray', muted_alpha=0.1, source=source)
            plot_stoD = q.line(x = 'date',y = 'sto_D',line_color="red",line_width=2,line_alpha=0.8,line_dash='solid',legend_label="STO D",muted_color='gray', muted_alpha=0.1, source=source)

            # Caixas delimitando faixas de oversold e overbought
            low_box = BoxAnnotation(top=20, fill_alpha=0.1, fill_color='red')
            mid_box = BoxAnnotation(bottom=20, top=80, fill_alpha=0.1, fill_color='green')
            high_box = BoxAnnotation(bottom=80, fill_alpha=0.1, fill_color='red')

            q.add_layout(low_box)
            q.add_layout(mid_box)
            q.add_layout(high_box)

            # Eixo X - Datas
            q.xaxis.axis_label = 'Tempo'
            q.xgrid.grid_line_alpha = 0.6
            q.xgrid.grid_line_dash = [6, 4]

            listAux = ["%d%b/%y-%Hh%Mm"]

            q.xaxis.formatter=DatetimeTickFormatter(
                    hours=listAux,
                    days=listAux,
                    months=listAux,
                    years=listAux,
                )

            # Eixo Y - Valores
            q.yaxis.axis_label = 'Porcentagem'
            q.yaxis.minor_tick_in = -3
            q.yaxis.minor_tick_out = 8
            q.yaxis[0].formatter = PrintfTickFormatter(format="%8.8f")

            q.ygrid.band_fill_color="olive"
            q.ygrid.band_fill_alpha = 0.1
            q.ygrid.grid_line_alpha = 0.6
            q.ygrid.grid_line_dash = [6, 4]

            # Legendas
            q.legend.location = "top_right"
            q.legend.click_policy="hide"

        # https://docs.bokeh.org/en/2.4.1/docs/first_steps/first_steps_7.html
        output_file(htmlFilename, title=p.title.text)
        if booEstocasticos:
            save(column(p,q))
        else:
            save(p)
        return df

    # https://lazka.github.io/pgi-docs/WebKit2-4.0/classes/WebView.html#WebKit2.WebView.signals.script_dialog
    # https://lazka.github.io/pgi-docs/WebKit2-4.0/classes/ScriptDialog.html#WebKit2.ScriptDialog
    # https://docs.bokeh.org/en/latest/docs/user_guide/interaction/callbacks.html
    # Não estou usando esse que faz par com o javascript geral
    def on_webview_script_dialog(self,web_view, dialog):
        strMsg = dialog.get_message()
        # print(strMsg)
        json_acceptable_string = strMsg.replace("'", "\"")
        d = json.loads(json_acceptable_string)
        print(d['event'])
        return True

    # -----------------------------------------------------
                    # Eventos Auxiliares
    # -----------------------------------------------------



    # -----------------------------------------------------
                    # Exemplos e Testes
    # -----------------------------------------------------

    def on_btnOk_clicked(self,a):
        print('Foi')
        Builder.get_object("stackRight").show()

    def exemploPlotaImagem(self):
        plt.style.use('_mpl-gallery')
        # make data
        x = np.linspace(0, 10, 100)
        y = 4 + 2 * np.sin(2 * x)
        # plot
        fig, ax = plt.subplots()

        ax.plot(x, y, linewidth=2.0)

        ax.set(xlim=(0, 8), xticks=np.arange(1, 8),
               ylim=(0, 8), yticks=np.arange(1, 8))

        plt.savefig(strImageFilename)
        plt.close()
        Builder.get_object("imgPrincipal").set_from_file(strImageFilename)

    def exemploAbreHTML1(self):
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageGrafico"))

        # self.Webview.load_uri("https://docs.bokeh.org/en/latest/docs/user_guide/interaction/widgets.html")
        # https://webkitgtk.org/reference/webkit2gtk/stable/WebKitWebPage.html#WebKitWebPage-console-message-sent


        def display_event(div, attributes=[], style = 'float:left;clear:left;font_size=13px'):
            "Build a suitable CustomJS to display the current event in the div model."
            return CustomJS(args=dict(div=div), code="""
                const attrs = %s;
                const args = [];
                for (let i = 0; i<attrs.length; i++) {
                    args.push(attrs[i] + '=' + Number(cb_obj[attrs[i]]).toFixed(2));
                }
                const line = "<span style=%r><b>" + cb_obj.event_name + "</b>(" + args.join(", ") + ")</span>\\n";
                const text = div.text.concat(line);
                const lines = text.split("\\n")
                if (lines.length > 35)
                    lines.shift();
                div.text = lines.join("\\n");
                window.alert(div.text);
            """ % (attributes, style))

        x = np.random.random(size=4000) * 100
        y = np.random.random(size=4000) * 100
        radii = np.random.random(size=4000) * 1.5
        colors = ["#%02x%02x%02x" % (int(r), int(g), 150) for r, g in zip(50+2*x, 30+2*y)]

        p = figure(tools="pan,wheel_zoom,zoom_in,zoom_out,reset")
        p.scatter(x, y, radius=np.random.random(size=4000) * 1.5,
                  fill_color=colors, fill_alpha=0.6, line_color=None)

        div = Div(width=400, height=p.height, height_policy="fixed")
        button = Button(label="Button", button_type="success")
        layout = column(button, row(p, div))

        ## Events with no attributes
        button.js_on_event(events.ButtonClick, display_event(div)) # Button click
        p.js_on_event(events.LODStart, display_event(div))         # Start of LOD display
        p.js_on_event(events.LODEnd, display_event(div))           # End of LOD display

        ## Events with attributes
        point_attributes = ['x', 'y', 'sx', 'sy']                  # Point events
        wheel_attributes = point_attributes + ['delta']            # Mouse wheel event
        pan_attributes = point_attributes + ['delta_x', 'delta_y'] # Pan event
        pinch_attributes = point_attributes + ['scale']            # Pinch event

        point_events = [
            events.Tap, events.DoubleTap, events.Press, events.PressUp,
            events.MouseMove, events.MouseEnter, events.MouseLeave,
            events.PanStart, events.PanEnd, events.PinchStart, events.PinchEnd,
        ]

        for event in point_events:
            p.js_on_event(event, display_event(div, attributes=point_attributes))

        p.js_on_event(events.MouseWheel, display_event(div, attributes=wheel_attributes))
        p.js_on_event(events.Pan,        display_event(div, attributes=pan_attributes))
        p.js_on_event(events.Pinch,      display_event(div, attributes=pinch_attributes))


        strOutputFilename = strPathDir + strSep +  "layout.html"

        output_file(strOutputFilename, title="JS Events Example")
        save(layout)


        self.Webview.connect("script_dialog", self.on_webview_script_dialog_from_hover)


        with open(strOutputFilename, 'r') as fileHTML:
            strHTML = fileHTML.read()
        self.Webview.load_html(strHTML,None)


    def exemploAbreHTML(self):
        Builder.get_object("stackPrincipal").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
        Builder.get_object("stackPrincipal").set_visible_child(Builder.get_object("spPageGrafico"))

        # define some points and a little graph between them
        x = [2, 3, 5, 6, 8, 7]
        y = [6, 4, 3, 8, 7, 5]
        links = {
            0: [1, 2],
            1: [0, 3, 4],
            2: [0, 5],
            3: [1, 4],
            4: [1, 3],
            5: [2, 3, 4]
        }

        p = figure(width=400, height=400, tools="", toolbar_location=None, title='Hover over points')

        source = ColumnDataSource({'x0': [], 'y0': [], 'x1': [], 'y1': []})
        sr = p.segment(x0='x0', y0='y0', x1='x1', y1='y1', color='olive', alpha=0.6, line_width=3, source=source, )
        cr = p.circle(x, y, color='olive', size=30, alpha=0.4, hover_color='olive', hover_alpha=1.0)

        # add a hover tool that sets the link data for a hovered circle
        code = """
        const links = %s
        const data = {'x0': [], 'y0': [], 'x1': [], 'y1': []}
        const indices = cb_data.index.indices
        window.alert(indices);
        for (let i = 0; i < indices.length; i++) {
            const start = indices[i]
            for (let j = 0; j < links[start].length; j++) {
                const end = links[start][j]
                data['x0'].push(circle.data.x[start])
                data['y0'].push(circle.data.y[start])
                data['x1'].push(circle.data.x[end])
                data['y1'].push(circle.data.y[end])
            }
        }
        segment.data = data
        """ % links

        callback = CustomJS(args={'circle': cr.data_source, 'segment': sr.data_source}, code=code)
        p.add_tools(HoverTool(tooltips=None, callback=callback, renderers=[cr]))

        strOutputFilename = strPathDir + strSep +  "layout.html"

        output_file(strOutputFilename, title="CustomJS for hover tool")
        save(p)

        self.Webview.connect("script_dialog", self.on_webview_script_dialog_from_hover)


        with open(strOutputFilename, 'r') as fileHTML:
            strHTML = fileHTML.read()
        self.Webview.load_html(strHTML,None)

    def onClockTimer(self):
        strDatetimenow = str(datetime.datetime.now())
        Builder.get_object("lbGraficoNome").set_label(strDatetimenow)
        return True

    def startClockTimer(self):
        GLib.timeout_add(1000, self.onClockTimer)

Builder = Gtk.Builder()
Builder.add_from_file(strGladeFullFilename)
Builder.connect_signals(Manipulador())
Window: Gtk.Window = Builder.get_object("main_window")
Window.show_all()
Builder.get_object("stackLeft").hide()
Builder.get_object("stackRight").hide()
Builder.get_object("stackBar").hide()
Gtk.main()
