# Projeto Estudos

## Piplist

pip install selenium
pip install jupyter ipython django-extensions
pip install numpy
pip install pandas
pip install bokeh
pip install sklearn
pip install nltk
pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0 
pip3 install pycairo
pip3 install PyGObject
pip3 install pyperclip
pip install matplotlib


Vou instalar o jupyter-notebook e também o django para fazer um banco de dados em django local.
E também o GTK.

Vou colocar os comandos de pip no piplist para refazer depois.

## Criar app Django

> Coloquei user e senha "admin"

 python manage.py makemigrations
 python manage.py migrate
 python3 manage.py createsuperuser

## Agora preciso 

> Criar um app e um banco de dados vazio e ver se consigo acessar do GTK e do Jupyter

## App

      ~/prog/ved/bdved$ python manage.py startapp appVed

Vamos ao Pycharm agora para fazer umas configurações.

## Funcionou

# Agora vou fazer funcionar um exemplo pelo GTK e outro pelo Jupyter-Notebook

> GTK funcionou!

## Jupyter-notebook

Para fazer funcionar pelo jupyter tem que dar um comando diferente:

> python manage.py shell_plus --notebook

E lá dentro tem que trocar o Kernel para 'Django Shell Plus'

E usar os primeiros comandos abaixo:

     import os
     os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
     from appVed.models import Unidade
     a = Unidade.objects.all()
     a


## Objetivos




Primeiro será criar uma interface gráfica com uma área onde quero plotar um gráfico.

E ir fazendo esses tutoriais de Matplotlib

https://matplotlib.org/stable/tutorials/index.html

Beleza!!

Vou transformar os dados em dataframe e de dataframe transformo em json e salvo como texto no django.

Vamos ver o maior tamanho possivel do django text ou se tem algo melhor.

Tem o JSONfield lá

https://docs.djangoproject.com/en/4.0/ref/models/fields/#jsonfield


Então pronto

Agora preciso de uma fonte de dados para analisar

pegar
transformar em dataframe
transformar em json
salvar

e poder fazer analises em cima
a primeira que quero é fazer o fft discreto para poder analisar a frequencia

O banco de dados pode ser de Funcoes
Cada registro tem um json e diversas outras propriedades


## Banco de Dados

Tabela de Gráficos que posso plotar e de qual API

Tabela de Eventos com dthrInicio e dthrFinal que é a época média em que algo aconteceu



Mudar a filosofia. Você não compra. Você troca.


Tabela de Datas em que troquei alguma coisa:
- Par Moeda/Moeda
Qual e quanto eu dei e qual e quanto eu recebi e qual e quanto eu dei de taxa para a exchange


- Quanto paguei pelas duas
- Data

(Posso olhar melhor depois)


Reais tem em várias exchanges e em cada um deles, 1 real tem a sua cotação em diversos outros ativos.

E de certa forma, o real de uma exchange vale mais ou menos que o mesmo real de outra exchange.

O mesmo para casas de cambio.

O que é um gráfico? É o valor que 1 BTC vale em reais em determinado momento.

OU

Se plotar ao contrário, podemos colocar o BITCOIN no eixo y e saber quanto 1 real vale em BITCOINs em relação ao tempo.

Se eu tenho um gráfico que diz quanto um bitcoin vale em reais. Eu posso fazer a conta inversa.


1 bitcoin ------------- 150.000 reais
    x      ------------- 1 real
    
x = 1 biutcoin x 1 reais / 150.000 reais

x = 1/ 150 000

Como 1 bitcoin são 10⁸ satoshis 

1 bitcoin = 100.000.000 = 10^8 Satoshis

.: x = 100.000.000 / 150.000



## Coisas

Com a queda do bitcoin, quais ativos estão na contramão? Só subindo?

Maker MKR

Fei USD (Atrelado ao dolar)
DAI
TrueUSD
Binance USD
USD Coin

Tem uma api dea CoinkMarketCap

https://coinmarketcap.com/api/

Pra fazer brincadeiras



#### Pra estudar quando tiver saco

https://academy.binance.com/pt/articles/a-complete-guide-to-cryptocurrency-trading-for-beginners?utm_campaign=googleadsxacademy&utm_source=googleadwords_int&utm_medium=cpc&ref=HDYAHEES&gclid=Cj0KCQjwpv2TBhDoARIsALBnVnnKcaVedURg3ukT5LfyoDRiKbL-dYqTjNPEVg2k3LisAeDNt6oVDCQaAoO3EALw_wcB


## Legal isso

https://arvr.google.com/cardboard/

https://arvr.google.com/cardboard/get-cardboard/

https://produto.mercadolivre.com.br/MLB-878878419-oculos-3d-realidade-virtual-google-cardboard-pronta-entrega-_JM?matt_tool=47921964&matt_word=&matt_source=google&matt_campaign_id=14303413631&matt_ad_group_id=125984290517&matt_match_type=&matt_network=g&matt_device=c&matt_creative=539354956473&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=509872170&matt_product_id=MLB878878419&matt_product_partition_id=1636606180488&matt_target_id=aud-395642386021:pla-1636606180488&gclid=Cj0KCQjwpv2TBhDoARIsALBnVnmkrb2o07l3ia1NPUJs6PfGmBandSbq66dAYOxm1aOqU8wB-Fwg7Q8aAlB5EALw_wcB


Achei por 1600 reais
https://pt.aliexpress.com/item/1005003440209196.html?UTABTest=aliabtest266652_356678&_randl_currency=BRL&_randl_shipto=BR&src=google&src=google&albch=shopping&acnt=768-202-3196&slnk=&plac=&mtctp=&albbt=Google_7_shopping&isSmbAutoCall=false&needSmbHouyi=false&albcp=15224889960&albag=132494569431&trgt=295990060787&crea=pt1005003440209196&netw=u&device=c&albpg=295990060787&albpd=pt1005003440209196&gclid=Cj0KCQjwpv2TBhDoARIsALBnVnmWLUpc3R7aX5bB5wT0VQWrCbAg7cIQKsG2HafLRwv1a3FHOTi36HMaAtXoEALw_wcB&gclsrc=aw.ds&aff_fcid=1bf3fbec219b4d56a87d4f0bdb1d42a5-1652573263613-05777-UneMJZVf&aff_fsk=UneMJZVf&aff_platform=aaf&sk=UneMJZVf&aff_trace_key=1bf3fbec219b4d56a87d4f0bdb1d42a5-1652573263613-05777-UneMJZVf&terminal_id=93c87484bdec4585bf9b0060c7c4c14d&OLP=1082100908_f_group1&o_s_id=1082100908&afSmartRedirect=y

https://pt.aliexpress.com/item/1005003440209196.html?UTABTest=aliabtest266652_356678&_randl_currency=BRL&_randl_shipto=BR&src=google&src=google&albch=shopping&acnt=768-202-3196&slnk=&plac=&mtctp=&albbt=Google_7_shopping&isSmbAutoCall=false&needSmbHouyi=false&albcp=15224889960&albag=132494569431&trgt=295990060787&crea=pt1005003440209196&netw=u&device=c&albpg=295990060787&albpd=pt1005003440209196&gclid=Cj0KCQjwpv2TBhDoARIsALBnVnmWLUpc3R7aX5bB5wT0VQWrCbAg7cIQKsG2HafLRwv1a3FHOTi36HMaAtXoEALw_wcB&gclsrc=aw.ds&aff_fcid=1bf3fbec219b4d56a87d4f0bdb1d42a5-1652573263613-05777-UneMJZVf&aff_fsk=UneMJZVf&aff_platform=aaf&sk=UneMJZVf&aff_trace_key=1bf3fbec219b4d56a87d4f0bdb1d42a5-1652573263613-05777-UneMJZVf&terminal_id=93c87484bdec4585bf9b0060c7c4c14d&OLP=1082100908_f_group1&o_s_id=1082100908&afSmartRedirect=y

Mais 300  reais de frete
2000 conto

Vou comprar !!

## Voltando pro banco

Vou cadastrar o PAR dizendo quem é o eixo Y

Tem que fazer umn app de carteira mesmo pra vc cadastrar quanto tem de determinada moeda e em que exchange

Então preciso cadastrar as Exchanges ou as Carteiras



Então blz

Resolvi parar de querer dominar o mundo com o banco de dados

Vou começar resolvendo o problema pequeno e com o tempo vou arrumando o banco de dados

Por enquanto como a única corretora que animo de usar é a binance, vamos de binance mesmo


Vou querer cadastrar:

- O Mercado (nem precisa pois é uma lista fixa)

- Quanto comprei de determinada moeda e por quanto

Por isso preciso cadastrar os pares

Por exemplo, comprei tantos satoshis de BTC por tantos reais
Depois troquei tantos Satoshis de BTC por tantos outros Satoshis de Waves

Não vou programar agora agora nao

Ainda estou filosofando

Vamos aos poucos


# 16/05

Vc precisa fazer uma carteira que mostre quanto você comprou de determinada moeda e por quanto.

Isso é uma transação.

O problema é que nem todas as transações são de real para cripto. Algumas são de cripto para cripto.

Então a transação deveria ter:

fkMoedaDebito = Moeda que vc gastou
Valor

fkMoedaCredito = Moeda que vc comprou
Valor

E ai temos um "problema": Enquanto em cripto a menor unidade é 1 Satoshi, no Fiat, a menor unidade é o centavo.

Como parear Satoshis e Centavos de forma a guardar a quantidade no mesmo campo?

1 real = 100 centavos de real (cent avos)
1 dolar = 100 centavos de dolar

1 bitcoin = 100.000.000 Satoshis

Se vc considerar que 1 centavo é uma fração do Real que corresponde a 0,01 reais

Então 1 Satoshi é uma fração do Bitcoin e corresponde a 0,00000001 BitCoins

Quanto seria 1 Satoshi de real? 0,00000001 Reais

Então 1 centavo deveria corresponder a 10^8 / 100

1 centavo = 1.000.000 de Satoshis

1 real = 100.000.000 Satoshis

1 real = 1 Bitcoin

Claro que não, mas no banco é assim que eu poderia guardar

Ou posso fazer uma adaptações

Pq lá eu vou guardar em Satoshis

Vai ser sempre algo como: 100.000.000 Satoshis

100.000.000 Satoshis --------  

O campo, quando representar cripto será um número de 8 dígitos (Satoshi)
E quando representar fiat, estará representando a quantia em centavos daquele Fiat


1000 reais = 100.000 centavos

1 real = 100 centavos

1 centavo = 1 centavo

100 = 1 real

Ou seja não está bom pois a conversão de cabeça não é automática

Entao vamos la

10 real = 10.000 milireais
1 real = 1000 milireais

1 centavo = 10 milireais

= 10 000 mili mili = micro

Então pronto

O campo vai guardar a menor fração do dinheiro aque ele representa


Quanto for Satoshi, vai ser satoshi
Quando for centavo, vaui ser centavo



1 a 100.000.000 Satoshis de um lado
1 centavo a 1.000.000 reais

Mas se vc precisar de frações de centavo, vc não consegue


0,001 centavo vc nao pode

## Vamos tirar a dúvida com a Binance

Lá na Binance ele guarda Real com 8 casas decimais


0,00894800   =  R$0,00894800 reais

Ele guarda usando 8 casas decimais para real

Ou seja 1 real pra ele são

1,00000000 reais


Ele diz lá que eu tenho

0,00894800 BRL

E que isso equivale a 

0,00894800 Reais

Ou seja

0,894800 centavos

10 digitos depois da casa decimal


Ou seja o maior valor é de 

100000000000 

E o menor é de:

0,00000001 "Reais"





0,894800 centavos 

Se eu quisesse guardar isso sem usar casas decimais

Entao a minha unidade deveria ser

894800 x 10^-6 centavos


894800 micro centavos

1 bitcoin equivale a 1,00000000 bitcoins

ou 

1 bitcoin = 100000000 x 10-8  bitcoins
1 bitcoin = 100000000 x coco  bitcoins

Minha unidade vai ser "coco"

100000000 cocos de bitcoins

1 bitcoin = 100.000.000 satoshis  = 100000000 x coco  bitcoins

100.000.000 satoshis  = 100000000 x coco  bitcoins

1 satoshis = 1 coco de bitcoin

Mudando o nome Coco para Sats

então 1 satoshi = 1 sat de bitcoin

E aí eu batizo 1 sat como uma multiplicador que significa 10^-8

Então no meu banco de dados vou guardar em "Sats da Moeda"

Quem tem 1 real

Tem na verdade

1 x 100.000.000 de Sats de Real


100000000 esse é o valor que vai para o BD para representar 1 real


Para representar meus 0,00894800 reais eu preciso de


894800 Sats de Real

Ou seja, se eu quiser saber o total em Reais tenho que dividir por 100.000.000

E vai voltar para:

 
0,00894800 reais

Então eu vou guardar no banco de dados em "Sats" da Moeda

1 Sat = 10^-8


1 bitcoin serão 1 x 100.000.000 Sats no banco de dados


151011.28 reais
 

# 17/05

Filosofando mais:

Embora preciso atachar a cada Sat de Bitcoin comprado o valor que paguei por ele e o instante de tempo em que fiz a troca.

Um Satochi é um Satochi. Ie, em tese, um Satoshi na Binance e um Satoshi no MB vale o mesmo.

Suponha que eu comprei 1000 reais em Bitcoin no MB = X. E desejo vende-los na Binance. Mas lá na Binance eu tenho já X bitcoins 
comprados em outro lugar.

Eu não preciso pegar esses X da MB e mandar para a Binance para vende-los lá. Lógico que não.

Eu posso vender esses X na Binance e anotar a venda e portanto o lucro (ou prejuizo) relativo a essa transação.

E aí os Bitcoins do MB relativo a essa compra do passado passam a ter um outro significado.

Na prática, eu não preciso saber onde fiz a troca.

Se está no meu poder seja onde for, então tudo que preciso saber é quanto comprei, quando e por qual valor.


Ex.:

Comprei 100 Satoshis a 500 reais em 10/10/2020 =

Comprei 50 Satoshis a 500 reais em 10/10/2020 +
Comprei 50 Satoshis a 500 reais em 10/10/2020


Vendi 50 Satoshis a 1000 reais em 10/09/2021 =
Vendi 10 Satoshis a 1000 reais em 10/09/2021 +
Vendi 40 Satoshis a 1000 reais em 10/09/2021


Eu queria uma forma matemática de ter uma medida que imbutisse o valor e o preço de uma vez.

===

Por exemplo:

Comprei 500 Satoshis por 100 reais em 10/10/2020

Cada Satochi custou 500 Satoshi / 100 reais = 5 Satochis / Real  
Tá mas nessa informação eu perco a quantidade 


Posso dizer que tenho 500 Satoshis que valem 100 reais pra mim


Ou 500 a 5 Sat/Real
Ou 500 Satoshi a 0,2 Real/Satoshi

E aí voltar ao preço que paguei é só multiplicar


O que fiz, na verdade é declarar o quanto eu comprei e qual a cotação invertida.

Ou 500 Satoshi a 0,2 Real/Satoshi

= 


0,00000500 bitcoins a 150.000 Real/Bitcoin

Se vc multiplica vc descobre o preço que paguei.

Beleza.
Mas aí a dimensão do tempo se perde.


Tenho 500 Satoshi a 0,2 Real/Satoshi em 10/10/2020
Vendi 500 Satoshi a 0,5 Real/Satochi em 10/10/2021

Quantos Satoshi me sobraram?
Qual foi meu lucro ou prejuizo?
Qual foi o rendimento? ou Depreciacao?

1) Comprei 500 Satoshi por 100 Reais
2) Vendi 500 Satoshi por   250 reais

Me sobraram 500 - 500 = 0 Satoshi
Mas lucrei 250 - 200 = 50 reais


Eu queria chegar nesses 50 reais de forma rápida sem fazer muita conta.

## Depois de alguma filosofia não cheguei a conclusão alguma

O que preciso guardar mesmo é o Par de Troca

## E já estou guardando

Agora vou fazer a contabilização da carteira.

A carteira é uma tabela, mas os dados para popular ela não são diretos do banco
É um algoritmo que le os pares de troca e calcula a carteira.



## Vou fazer o seguinte

Para poder separar o que depende de internet do que não depende
Vou criar mais colunas na tabela e esconde-las

Nessas colunas vou colocar o valor em sat dos ativos
E aí depois, para fazer processamento, eu pego o valor do ativo diretamente

Vou fazer essas modificações no tbCarteira primeiro

## Vou modificar para

Ele vai ler todos os ativos e calcular como transforma daquele ativo para real
uma unica vez no inicio do programa e criar um dict que vc informa o ativo e ele
informa o multiplicador para converter para real

Alguns são direto. É só chamar direto o preço do par XXXBRL
Outro vou ter que passar pelo BTC (ou outra moeda)
Só não sei se consigo fazer isso automatico, ou terei de cadastrar como faz.

Ex.: WAVES não tem em real

WAVESBTC e depois BTCBRL


Vou salvar esse dict como um arquivo JSON

E ai ao entrar, se eu nao quiser fazer chamadas à API, eu carrego esse JSON


E depois, de forma offline (sem precisar de novas chamadas), eu processo 
as tabelas para fazer as contas de previsão


PROPOSTA II (Pra resolver logo tudo)


No banco de dados ao cadastrar o ativo eu já informa uma string separado por virgula
como eu chego no valor em real

Ex.:

WAVES

WAVEBNB, BNBBTC, BTCBRL

Aqui são 3 chamadas multiplicando os multiplicadores até chegar num multiplicador que saia 
de um e chegue no outro


BTC vai ser: BTCBRL (uma chamada só direto)

Assim, eu já resolvo os casos de moedas que nao tem valor direto em real

Vai ser melhor assim mesmo

## Ficou bom

Não fiz JSON nem nada por enquanto

#/# Próximas Brincadeira: 


#### Clicar 2 vezes numa Transação e mostrar Gráfico

Clicar 2 vezes numa Transação e ele plota um gráfico daquele mercado

E ele coloca linhas para indicar as posições que tenho naquele mercado


#### Criar Tabela para conter Eventos Históricos com Data de Inicio e Data de Final

De modo a, ao plotar um gráfico, poder gerar um retângulo para indicar aquele evento.


## Plotagem

Eu resolvi trazer o código do criptoPlot para dentro do est.py porque eu não
vou plotar tudo. E a plotagem vai ser integrado ao aplicativo para poder
escolher o que plotar e etc.


https://docs.bokeh.org/en/latest/docs/user_guide/interaction/widgets.html

# 30/09/2023 

Após um bom tempo sem mexer eu voltei pq fiquei interessado no fft e olha que coisa eu já tinha pensado nisso mas não
implementei.

Vou tentar implementar.


# 01/10/23

Uma coisa que eu posso começar a fazer nesses projetos é ir criando uns vídeos numa pastinha
explicando como as coisas funcionam.

O objetivo seria conseguir retomar o projeto mais rapidamente depois pois 
as vezes isso acontece de vc parar por um tempo e depois querer voltar e 
ficar meio perdido.

