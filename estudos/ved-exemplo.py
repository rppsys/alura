# coding=utf-8
# https://lazka.github.io/pgi-docs/Gtk-3.0/classes.html

import os
from os import listdir
from os.path import isfile, join
import sys

import django
import gi
import datetime
import time
import pytz
import os.path
import argparse
import re
import math
import pandas as pd
import configparser

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# Bibliotecas Django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bdved.settings')
django.setup()
from django.utils import timezone
from django.utils.timezone import make_aware
from appVed.models import Unidade, Tentativa, Evento

# Bibliotecas Gtk
gi.require_version('Gtk','3.0')
from gi.repository import Gtk,Gdk,Pango,GLib

print("")

clNoColor='\033[0m'
clBlack='\033[0;30m'
clRed='\033[0;31m'
clGreen='\033[0;32m'
clOrange='\033[0;33m'
clBlue='\033[0;34m'
clPurple='\033[0;35m'
clCyan='\033[0;36m'
clLight_Gray='\033[0;37m'
clDark_Gray='\033[1;30m'
clLight_Red='\033[1;31m'
clLight_Green='\033[1;32m'
clYellow='\033[1;33m'
clLight_Blue='\033[1;34m'
clLight_Purple='\033[1;35m'
clLight_Cyan='\033[1;36m'
clWhite='\033[1;37m'

dictColors = {
'clBlack':clBlack,
'clRed':clRed,
'clGreen':clGreen,
'clOrange':clOrange,
'clBlue':clBlue,
'clPurple':clPurple,
'clCyan':clCyan,
'clLightGray':clLight_Gray,
'clDarkGray':clDark_Gray,
'clDarkRed':clLight_Red,
'clLightGreen':clLight_Green,
'clYellow':clYellow,
'clLightBlue':clLight_Blue,
'clMediumPurple':clLight_Purple,
'clDarkCyan':clLight_Cyan,
'clWhite':clWhite
}

listPrint = []
lpF = '%Y-%m-%d %H:%M:%S %f'

def pS():
    print('')
    printClear()
    strRet = datetime.datetime.now().strftime(lpF) + ' > INICIO'
    listPrint.append(strRet)
    return clLight_Blue + '---------- INICIO ----------' + clNoColor

def pE(objT = None):
    strRet = datetime.datetime.now().strftime(lpF) + ' > FINAL'
    listPrint.append(strRet)
    if objT != None:
        objT.textoResposta = retPrintStr()
        objT.save()
    printClear()
    return clLight_Blue + '---------- FINAL ----------' + clNoColor

def retPrintStr():
    return '\r\n'.join([s for s in listPrint])

def printClear():
    listPrint.clear()

def printCor(clColor,arg):
    print(clColor + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clColor + arg + clNoColor

def printL(arg):
    print(clOrange + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clOrange + arg + clNoColor

def printR(arg):
    print(clRed + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clRed + arg + clNoColor

def printA(arg):
    print(clLight_Blue + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clLight_Blue + arg + clNoColor

def printC(arg):
    print(clLight_Cyan + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clLight_Cyan + arg + clNoColor

def printY(arg):
    print(clYellow + arg + clNoColor)
    listPrint.append(datetime.datetime.now().strftime(lpF) + ' > ' + arg)
    return clYellow + arg + clNoColor

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

strSep = os.path.sep
strPathDir = "/home/ronie/prog/ved/bdved"
strGladeFullFilename = strPathDir  + strSep +  "ved.glade"
strTokenFullFilename = strPathDir + strSep +  "token.json"
strCredentialsFullFilename = strPathDir + strSep +  "credentials.json"
strAutokeyFullFilename = strPathDir + strSep +  "autokeydata.ini"
strRelatorioCSVFullFilename = strPathDir + strSep +  "relatorioCSV.csv"
strRelatorioSEIFullFilename = strPathDir + strSep +  "relatorioSEI.txt"

strPageSourceEDMFullFilename = strPathDir + strSep +  "psEDM.txt"

strSrcTodosFullFilename = strPathDir + strSep +  "srcTodos.txt"
strSrcProximosFullFilename = strPathDir + strSep +  "srcProximos.txt"


cfgAuto = configparser.ConfigParser()
# Não vou ler pois sempre vou criar um novo somente para leitura sobrescrevendo o anterior
# cfgAuto.read(strAutokeyFullFilename)
cfgAuto['data'] = {}
cfgAuto['data']['hoje'] = datetime.datetime.today().strftime("%Y%m%d%H%M%S")
with open(strAutokeyFullFilename, 'w') as configfile:
    cfgAuto.write(configfile)

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--developer", required=False,help="Executar como desenvolvedor")
args =  vars(ap.parse_args())
if args['developer'] != None:
    print("")
    printR('##### Modo Desenvolvedor #####')
    print("")
    booDev = True
else:
    print("")
    printY('##### Modo Usuário #####')
    print("")
    booDev = False

class Manipulador:
    def __init__(self):
        self.iniciarGeral()
        self.iniciarSaida()
        self.iniciarTabela("tvTreeTodos")
        self.iniciarTabela("tvTreeHoje")
        self.atualizarTabelas()
        # Relatórios
        self.iniciarRelatorio("tvTreeRelatorio")

    def nextST(self):
        if len(self.listSTP) != 0:
            if self.activeSTP < len(self.listSTP) - 1:
                self.activeSTP += 1
            else:
                self.activeSTP = 0
            Builder.get_object("stackTabelas").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
            Builder.get_object("stackTabelas").set_visible_child(self.listSTP[self.activeSTP])

    def priorST(self):
        if len(self.listSTP) != 0:
            if self.activeSTP > 0:
                self.activeSTP -= 1
            else:
                self.activeSTP = len(self.listSTP) - 1
            Builder.get_object("stackTabelas").set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
            Builder.get_object("stackTabelas").set_visible_child(self.listSTP[self.activeSTP])

    def iniciarGeral(self):
        self.listSTP = []
        self.activeSTP = 0

    def iniciarSaida(self):
        self.tvSaida = Builder.get_object("tvSaida")
        self.tbSaida = Builder.get_object("tbSaida")
        for tC in dictColors:
            self.tbSaida.tag = self.tbSaida.create_tag(tC, weight=Pango.Weight.BOLD, foreground=tC.replace('cl',''))

    def iniciarTabela(self,strTvTree):
        # Tabela Principal
        lsList = Gtk.ListStore(str, str, str, str, str, bool, str, bool, bool, bool, str)  #, str)

        # Atacha
        tvTree = Builder.get_object(strTvTree)
        tvTree.props.model = lsList

        # Gera Tabela
        dictCols = {
        'ID':'N',
        'Tipo':'T',
        'Titulo':'T',
        'Início':'T',
        'DV':'N',
        'DE?':'B',
        'YV':'N',
        'YE?':'B',
        'Link?':'B',
        'VM':'B',
        }

        colIndex = 0
        for colTitulo in dictCols:
            if dictCols[colTitulo] == 'T':
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'N':
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'B':
                renderer = Gtk.CellRendererToggle()
                column = Gtk.TreeViewColumn(colTitulo, renderer, active = colIndex)
            else:
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            column.props.alignment = 0.5
            column.props.max_width = 400

            if (dictCols[colTitulo] == 'T') or (dictCols[colTitulo] == 'N'):
                column.add_attribute(renderer, "foreground", len(dictCols.keys()))
                # column.add_attribute(renderer, "background", len(dictCols.keys())+1)

            if (colTitulo == 'VM'):
                renderer.connect("toggled", self.on_cell_toggled_VM,strTvTree)

            if (colTitulo == 'DE?'):
                renderer.connect("toggled", self.on_cell_toggled_DE,strTvTree)

            if (colTitulo == 'YE?'):
                renderer.connect("toggled", self.on_cell_toggled_YE,strTvTree)

            tvTree.append_column(column)
            colIndex += 1

        # Esconde coluna ID
        tvTree.get_column(0).set_visible(False)

    def iniciarRelatorio(self,strTvTree):
        # Tabela Principal

        lsList = Gtk.ListStore(str, str, str, str, str)

        # Atacha
        tvTree = Builder.get_object(strTvTree)
        tvTree.props.model = lsList

        # Gera Tabela
        dictCols = {
        'ID':'N',
        'Data':'T',
        'Tipo - Titulo':'T',
        'Autor':'T',
        'Observações':'T',
        }

        colIndex = 0
        for colTitulo in dictCols:
            if dictCols[colTitulo] == 'T':
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'N':
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            elif dictCols[colTitulo] == 'B':
                renderer = Gtk.CellRendererToggle()
                column = Gtk.TreeViewColumn(colTitulo, renderer, active = colIndex)
            else:
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(colTitulo, renderer, text = colIndex)
            column.props.alignment = 0.5
            column.props.max_width = 400

            tvTree.append_column(column)
            colIndex += 1

        # Esconde coluna ID
        tvTree.get_column(0).set_visible(False)
        tvTree.props.enable_grid_lines = 3
        tvTree.dictCols = dictCols

    def on_cell_toggled_VM(self, widget, path, strTvTree):
        # Preciso pegar o ID da linha path
        tvTree = Builder.get_object(strTvTree)
        lsList = tvTree.props.model
        treeiter = lsList.get_iter(path)
        numID = lsList.get_value(treeiter, 0)
        objE = Evento.objects.get(pk=numID)
        objE.booVerificacaoManual = not objE.booVerificacaoManual
        objE.save()

        # O Ideal seria chamar atualizaTabelas de novo
        # mas para aprender vou checar a caixa do GTK diretamente
        lsList[path][9] = not lsList[path][9]
        self.atualizarTabelas()

    def on_cell_toggled_DE(self, widget, path, strTvTree):
        # Preciso pegar o ID da linha path
        tvTree = Builder.get_object(strTvTree)
        lsList = tvTree.props.model
        treeiter = lsList.get_iter(path)
        numID = lsList.get_value(treeiter, 0)
        objE = Evento.objects.get(pk=numID)
        objE.booDemocraciaEncontrado = not objE.booDemocraciaEncontrado
        objE.save()

        # O Ideal seria chamar atualizaTabelas de novo
        # mas para aprender vou checar a caixa do GTK diretamente
        lsList[path][5] = not lsList[path][5]
        self.atualizarTabelas()

    def on_cell_toggled_YE(self, widget, path, strTvTree):
        # Preciso pegar o ID da linha path
        tvTree = Builder.get_object(strTvTree)
        lsList = tvTree.props.model
        treeiter = lsList.get_iter(path)
        numID = lsList.get_value(treeiter, 0)
        objE = Evento.objects.get(pk=numID)
        objE.booYoutubeEncontrado = not objE.booYoutubeEncontrado
        objE.save()

        # O Ideal seria chamar atualizaTabelas de novo
        # mas para aprender vou checar a caixa do GTK diretamente
        lsList[path][7] = not lsList[path][7]
        self.atualizarTabelas()

    def atualizarTabelas(self):
        dtHoje = timezone.now()
        delta1Dia = datetime.timedelta(1)
        delta3Dias = datetime.timedelta(3)
        dtAgora = datetime.datetime.today()

        try:
            dtHojeInic = timezone.make_aware(datetime.datetime(dtAgora.year, dtAgora.month, dtAgora.day, 0, 0, 0),
                                             timezone=pytz.timezone('Brazil/West'))
        except (pytz.NonExistentTimeError, pytz.AmbiguousTimeError):
            tz = pytz.timezone('Brazil/West')
            dtHojeInic = tz.localize(datetime.datetime(dtAgora.year, dtAgora.month, dtAgora.day, 0, 0, 0), is_dst=False)

        try:
            dtHojeFinal = timezone.make_aware(datetime.datetime(dtAgora.year, dtAgora.month, dtAgora.day, 23, 59, 59),
                                              timezone=pytz.timezone('Brazil/West'))
        except (pytz.NonExistentTimeError, pytz.AmbiguousTimeError):
            tz = pytz.timezone('Brazil/West')
            dtHojeFinal = tz.localize(datetime.datetime(dtAgora.year, dtAgora.month, dtAgora.day, 23, 59, 59),
                                      is_dst=False)

        numWeek = dtHoje.weekday()
        if (numWeek == 4):
            dtAmanhaFinal = dtHojeFinal + delta3Dias
        else:
            dtAmanhaFinal = dtHojeFinal + delta1Dia


        self.objEventos_Todos = Evento.objects.filter(booVerificacaoManual=False, booCancelado=False).order_by('-dthrInicio')
        self.objEventos_Hoje = Evento.objects.filter(dthrInicio__range=(dtHojeInic, dtAmanhaFinal),booCancelado=False)
        self.objEventos_Futuro = Evento.objects.filter(dthrInicio__gte=dtHojeInic)
        self.objEventos_booDE_False = Evento.objects.filter(dthrInicio__gte=dtHojeInic,booDemocraciaEncontrado=False)

        Builder.get_object("lbTodosStatus").set_label(str(len(self.objEventos_Todos)) + ' eventos')
        Builder.get_object("lbHojeStatus").set_label(str(len(self.objEventos_Hoje)) + ' eventos')

        # Todos
        tvTreeTodos = Builder.get_object("tvTreeTodos")
        lsList = tvTreeTodos.props.model
        lsList.clear()
        for objE in self.objEventos_Todos:
            if objE.dthrInicio.astimezone().strftime('%d/%m/%Y') == dtHoje.strftime('%d/%m/%Y'):
                strClBackgroundColor = 'yellow'
            else:
                strClBackgroundColor = 'white'
            lsList.append([str(objE.id),
                          objE.strTipo,
                          objE.strTitulo,
                          objE.dthrInicio.astimezone().strftime('%d/%m/%Y %H:%M'),
                          str(objE.numDemocraciaVerificado),
                          objE.booDemocraciaEncontrado,
                          str(objE.numYoutubeVerificado),
                          objE.booYoutubeEncontrado,
                          objE.strYoutube != '',
                          objE.booVerificacaoManual,
                          strClBackgroundColor])
                          #'black'])

        # Hoje
        tvTreeHoje = Builder.get_object("tvTreeHoje")
        lsList = tvTreeHoje.props.model
        lsList.clear()
        for objE in self.objEventos_Hoje:
            if objE.dthrInicio.astimezone().strftime('%d/%m/%Y') == dtHoje.strftime('%d/%m/%Y'):
                strClBackgroundColor = 'yellow'
            else:
                strClBackgroundColor = 'white'
            lsList.append([str(objE.id),
                          objE.strTipo,
                          objE.strTitulo,
                          objE.dthrInicio.astimezone().strftime('%d/%m/%Y %H:%M'),
                          str(objE.numDemocraciaVerificado),
                          objE.booDemocraciaEncontrado,
                          str(objE.numYoutubeVerificado),
                          objE.booYoutubeEncontrado,
                          objE.strYoutube != '',
                          objE.booVerificacaoManual,
                          strClBackgroundColor])
                          #'black'])

        # Já gera INI para self.objEventos_booDE_False
        numEvento = 0
        for objE in self.objEventos_booDE_False:
            numEvento += 1
            strS = 'E{:03d}'.format(numEvento)
            cfgAuto[strS] = {}
            cfgAuto[strS]['01-cod'] = ''
            cfgAuto[strS]['02-titulo'] = 'CLDF'
            cfgAuto[strS]['03-sigla'] = 'CLDF'
            cfgAuto[strS]['04-nome'] = objE.strAutor
            cfgAuto[strS]['05-tipo'] = objE.strTipo
            cfgAuto[strS]['06-objeto'] = objE.strTitulo
            cfgAuto[strS]['07-tema'] = objE.strTitulo
            cfgAuto[strS]['08-local'] = objE.strLocal
            cfgAuto[strS]['09-data'] = objE.dthrInicio.astimezone().strftime('%d/%m/%Y')
            cfgAuto[strS]['10-hora'] = objE.dthrInicio.astimezone().strftime('%H:%M:%S')
            cfgAuto[strS]['11-maxuser'] = '1'
            cfgAuto[strS]['12-youtube'] = objE.strYoutube

        cfgAuto['CFG'] = {}
        cfgAuto['CFG']['numEventos'] = str(numEvento)
        cfgAuto['CFG']['window_title_inserir'] = 'Adicionar sala | Site de administração do Django - Mozilla Firefox'
        cfgAuto['CFG']['window_title_sei'] = 'SEI/CLDF'
        cfgAuto['CFG']['window_class'] = 'Navigator.firefox'

        with open(strAutokeyFullFilename, 'w') as configfile:
            cfgAuto.write(configfile)

    def atualizarRelatorio(self):
        dtHoje = timezone.now()
        if dtHoje.month == 1:
            numMesAnterior = 12
            numAnoAnterior = dtHoje.year - 1
        else:
            numMesAnterior = dtHoje.month - 1
            numAnoAnterior = dtHoje.year

        try:
            dtMesInic = timezone.make_aware(datetime.datetime(numAnoAnterior, numMesAnterior, 1, 0, 0, 0),
                                             timezone=pytz.timezone('Brazil/West'))
        except (pytz.NonExistentTimeError, pytz.AmbiguousTimeError):
            tz = pytz.timezone('Brazil/West')
            dtMesInic = tz.localize(datetime.datetime(numAnoAnterior, numMesAnterior, 1, 0, 0, 0), is_dst=False)

        try:
            dtMesFinal = timezone.make_aware(datetime.datetime(dtHoje.year, dtHoje.month, 1, 23, 59, 59),
                                              timezone=pytz.timezone('Brazil/West'))
        except (pytz.NonExistentTimeError, pytz.AmbiguousTimeError):
            tz = pytz.timezone('Brazil/West')
            dtMesFinal = tz.localize(datetime.datetime(dtHoje.year, dtHoje.month, 1, 23, 59, 59),
                                      is_dst=False)

        dtMesFinal = dtMesFinal - datetime.timedelta(days=1)

        # Para testar vamos pedir do mes atual
        # dtMesInic = dtMesInic + datetime.timedelta(30)
        # dtMesFinal = dtMesFinal + datetime.timedelta(35)

        # Filtro
        self.objEventosRelatorio = Evento.objects.filter(dthrInicio__range=(dtMesInic, dtMesFinal),booDemocraciaEncontrado=True).order_by('dthrInicio')
        Builder.get_object("page3LbTitulo").set_label('Relatório de ' + dtMesInic.astimezone().strftime('%d/%m/%Y %H:%M') + ' a ' + dtMesFinal.astimezone().strftime('%d/%m/%Y %H:%M'))
        Builder.get_object("lbRelatorioStatus").set_label(str(len(self.objEventosRelatorio)) + ' eventos')

        # Relatorio
        tvTreeRelatorio = Builder.get_object("tvTreeRelatorio")
        lsList = tvTreeRelatorio.props.model
        lsList.clear()

        self.strRelatorioCSV = ''
        self.strRelatorioSEI = ''

        i = 0
        # Forma bonita em python de transformas as keys do dict em list e depois remover o primeiro elemento (ID)
        # listR = [*tvTreeRelatorio.dictCols]
        # del listR[0]

        listR = ['Evento']
        for objE in self.objEventosRelatorio:
            strObs = ''
            if objE.booCancelado:
                strObs = 'Evento cancelado'
                if objE.strObs != '':
                    strObs = strObs + ': ' + objE.strObs

            # Apenda no liststore para fins de visualização da tabela
            lsList.append([str(objE.id),
                          objE.dthrInicio.astimezone().strftime('%d/%m/%Y %H:%M'),
                          objE.strTipo + ' - ' + objE.strTitulo,
                          objE.strAutor,
                          strObs])

            # Monta o relatórios CSV
            if self.strRelatorioCSV == '':
                self.strRelatorioCSV = lsList[i][1] + ';' + lsList[i][2] + ';' + lsList[i][3] + ';' + lsList[i][4] + ';' + strObs + ';'
            else:
                self.strRelatorioCSV = self.strRelatorioCSV + '\r\n' + lsList[i][1] + ';' + lsList[i][2] + ';' + lsList[i][3] + ';' + lsList[i][4] + ';' + strObs + ';'

            # Monta o relatórios SEI
            if self.strRelatorioSEI == '':
                self.strRelatorioSEI = objE.strTipo + ' - ' + objE.strTitulo + ';'
            else:
                self.strRelatorioSEI = self.strRelatorioSEI + '\r\n' + objE.strTipo + ' - ' + objE.strTitulo + ';'
            i = i + 1

            # Monta a lista de valores para o ini
            listR.append(objE.dthrInicio.astimezone().strftime('%-d de %B às %Hh%M'))
            listR.append(objE.strTipo + ' - ' + objE.strTitulo)

        # Salva Relatórios
        with open(strRelatorioCSVFullFilename, 'w') as fileRelatorio:
            fileRelatorio.write(self.strRelatorioCSV)

        with open(strRelatorioSEIFullFilename, 'w') as fileRelatorio:
            fileRelatorio.write(self.strRelatorioSEI)

        # Gera INI
        cfgAuto['SEI'] = {}
        numD = 0
        for strR in listR:
            numD += 1
            strD = 'D{:05d}'.format(numD)
            cfgAuto['SEI'][strD] = strR
        cfgAuto['CFG']['numDados'] = str(numD)

        # Salva INI
        with open(strAutokeyFullFilename, 'w') as configfile:
            cfgAuto.write(configfile)

    def o(self,strStr):
        if strStr != '':
            reStrStr = 'a' + re.escape(strStr)
            rC = ''
            for tC in dictColors:
                reColor = re.escape(dictColors[tC])
                if reStrStr.find(reColor) > 0:
                    rC = tC
                    break
            if rC != '':
                # Tem a cor rC que é a dictColors[rC] > Remover
                strStr = strStr.strip(dictColors[rC])
                strStr = strStr.strip(clNoColor)
                #Inserir com tags
                self.tbSaida.insert_with_tags_by_name(self.tbSaida.get_end_iter(),strStr + '\r\n\0',rC)
            else:
                self.tbSaida.insert(self.tbSaida.get_end_iter(), strStr + '\r\n\0')

    def on_miSobre_activate(self,button):
        self.nextST()

    # Realiza Scroll Down no ScrolledWindow do txSaida sempre que algo for inserido lá
    def on_tvSaida_size_allocate(self,a,b):
        adj = Builder.get_object("swSaida").get_vadjustment()
        adj.set_value(adj.get_upper() - adj.get_page_size())

    def on_miVerAgendaVesp_activate(self,button):
        self.o(pS())
        self.o(printC('Verificar Agenda Véspera'))
        objT = self.pegaCsv()
        if (objT.booSucesso):
            self.carregaCsv(objT)
        self.o(printC('Verificação Concluída!'))
        self.o(pE(objT))
        self.atualizarTabelas()

    def on_miVerificarEDemocracia_activate(self,button):
        self.o(pS())
        self.o(printC('Verificar e-Democracia'))

        # Atualiza Tabelas
        self.atualizarTabelas()

        # Cria Tentativa
        objTentativa = self.criaNovaTentativa()
        objTentativa.numClasse = 2
        objTentativa.save()

        if len(self.objEventos_booDE_False) != 0:
            # Configurações do WebDriver
            options = Options()
            options.set_preference("http.response.timeout", 30)
            options.set_preference("dom.max_script_run_time", 30)
            browser = webdriver.Firefox(options=options)
            browser.set_page_load_timeout(30)
            browser.implicitly_wait(30)

            # Acessa e-Democracia
            self.o(printY('Acessando Portal e-Democracia'))
            strGet = "https://edemocracia.cl.df.leg.br/audiencias/"
            try:
                browser.get(strGet)
            except:
                pass

            assert "Audiencias Interativas" in browser.title
            time.sleep(2)
            src = browser.page_source

            with open(strPageSourceEDMFullFilename, 'w') as filePS:
                filePS.write(src)

            self.o(printL('Dados Lidos'))

            time.sleep(1)
            browser.close()
            time.sleep(1)
            browser.quit()
            time.sleep(1)

            for objE in self.objEventos_booDE_False:
                textoProcura = objE.strTitulo
                self.o(printA('Procurando "{}"'.format(textoProcura)))
                m = self.meu_re_search(textoProcura,src)

                if (m):
                    objE.booDemocraciaEncontrado = True
                    self.o(printL('"{}" encontrado no e-Democracia!'.format(textoProcura)))
                else:
                    objE.booDemocraciaEncontrado = False
                    self.o(printR('"{}" não encontrado no e-Democracia!'.format(textoProcura)))
                objE.numDemocraciaVerificado = objE.numDemocraciaVerificado + 1
                objE.save()

            self.atualizarTabelas()
            objTentativa.booSucesso = True
            objTentativa.save()
        else:
            self.o(printR('Hoje não há eventos para serem verificados no e-Democracia'))
            objTentativa.booSucesso = False
        # Finalmente
        objTentativa.save()
        self.o(printC('Verificação Concluída!'))
        self.o(pE(objTentativa))

    def on_miVerificarYoutube_activate(self,button):
        self.o(pS())
        self.o(printC('Verificar Youtube'))

        # Atualiza Tabelas
        self.atualizarTabelas()

        # Cria Tentativa
        objTentativa = self.criaNovaTentativa()
        objTentativa.numClasse = 3
        objTentativa.save()

        if len(self.objEventos_Futuro) != 0:
            # Configurações do WebDriver
            options = Options()
            options.set_preference("http.response.timeout", 30)
            options.set_preference("dom.max_script_run_time", 30)
            browser = webdriver.Firefox(options=options)
            browser.set_page_load_timeout(30)
            browser.implicitly_wait(30)

            # Acessa Youtube
            self.o(printY('Acessando YouTube'))

            # Aba para página "Próximos Eventos"
            strGet = "https://www.youtube.com/c/TVC%C3%A2maraDistrital/videos?view=2&live_view=502"
            try:
                browser.get(strGet)
            except:
                pass
            assert "TV Câmara Distrital - YouTube" in browser.title

            time.sleep(2)
            window_proximos = browser.current_window_handle
            srcProximos = browser.page_source
            with open(strSrcProximosFullFilename, 'w') as filePS:
                filePS.write(srcProximos)
            self.o(printL('Próximos Vídeos carregado!'))

            # Nova Aba para página "Todos os Vídeos"
            browser.switch_to.new_window('tab')
            #strGet = "https://www.youtube.com/c/TVC%C3%A2maraDistrital/videos?view=57"
            strGet = "https://www.youtube.com/c/TVC%C3%A2maraDistrital/featured"

            try:
                browser.get(strGet)
            except:
                pass
            assert "TV Câmara Distrital - YouTube" in browser.title

            time.sleep(2)
            window_todos = browser.current_window_handle
            srcTodos = browser.page_source
            with open(strSrcTodosFullFilename, 'w') as filePS:
                filePS.write(srcTodos)
            self.o(printL('Todos vídeos carregado!'))

            time.sleep(1)
            browser.close()
            time.sleep(1)
            browser.switch_to.window(window_proximos)
            time.sleep(1)
            browser.close()
            time.sleep(1)
            browser.quit()
            time.sleep(1)

            self.o(printL('Verificando...'))
            for objE in self.objEventos_Futuro:
                booEncontrou, strLink = self.procuraYoutube(objE,srcProximos,srcTodos)
                # Atualiza
                if (booEncontrou):
                    objE.booYoutubeEncontrado = True
                    if (strLink != ''):
                        self.o(printL('"{}" encontrado no Youtube! Link Encontrado: {}'.format(objE.strTitulo,strLink)))
                        objE.strYoutube = strLink
                    else:
                        self.o(printL('"{}" encontrado no Youtube! Link Não Encontrado!'.format(objE.strTitulo)))
                else:
                    objE.booYoutubeEncontrado = False
                    self.o(printR('"{}" não encontrado no Youtube'.format(objE.strTitulo)))
                objE.numYoutubeVerificado = objE.numYoutubeVerificado + 1
                objE.save()

            self.atualizarTabelas()
            objTentativa.booSucesso = True
        else:
            self.o(printR('Hoje não há eventos para serem verificados no Youtube'))
            objTentativa.booSucesso = False
        # Finalmente
        objTentativa.save()
        self.o(printC('Verificação Concluída!'))
        self.o(pE(objTentativa))

    def on_miOpcao_CancelarEvento_activate(self,button):
        # Isso tudo é para pegar o ID do Evento Selecionado
        tvTreeTodos = Builder.get_object("tvTreeTodos")
        treeSelection = tvTreeTodos.get_selection()
        selModel, selPath = treeSelection.get_selected_rows()
        treeiter = selModel.get_iter(selPath)
        numID = selModel.get_value(treeiter, 0)
        objE = Evento.objects.get(pk=numID)
        if self.inputBox('Confirmação','Deseja mesmo cancelar o evento "' + objE.strTitulo  + '"?',booSomenteConfirma = True):
            booRet, strMotivo = self.inputBox('Cancelar evento','Informe o motivo:')
            if booRet:
                # Cancela
                objE.booCancelado = True
                objE.strObs = strMotivo
                objE.save()
                self.atualizarTabelas()
                self.msgConfirmacao('Informação','Evento cancelado!','dialog-emblem-default')

    def on_miOpcao_VerificarData_activate(self,button):
        dtData = self.dlgCalendar()
        if dtData != None:
            self.o(pS())
            self.o(printC('Verificar Agenda Alvo'))
            objT = self.pegaCsv(dtAlvoUser=dtData)
            if (objT.booSucesso):
                self.carregaCsv(objT)
            self.o(printC('Verificação Concluída!'))
            self.o(pE(objT))
            self.atualizarTabelas()

    def procuraYoutube(self,objE,srcProximos,srcTodos):
        self.o(printA('Procurando "{}"'.format(objE.strTitulo)))
        booEncontrou, strLink = self.procuraYoutubeSource(objE.strTipo + ' - ' + objE.strTitulo,srcProximos)
        if not booEncontrou:
            booEncontrou, strLink = self.procuraYoutubeSource(objE.strTipo + ' - ' + objE.strTitulo,srcTodos)
        return booEncontrou, strLink

    def procuraYoutubeSource(self,strProcura,strSource):
        strY = 'href="/watch?v='
        strP = '">' + strProcura

        f = math.ceil(len(strP)*2/3)
        strP = strP[0:f-1]

        strY = re.escape(strY)
        strP = re.escape(strP)

        y = re.finditer(strY,strSource)
        p = re.finditer(strP,strSource)

        ly = []
        for m in y:
            ly.append(m)

        lp = []
        for m in p:
            lp.append(m)

        strLink = ''
        booEncontrou = False
        if len(ly) != 0:
            if len(lp) != 0:
                menor = len(strSource)
                for my in ly:
                    for mp in lp:
                        # print('{}[{},{}]-{}[{},{}] = {}'.format(my.group(0),my.start(),my.end(),mp.group(0),mp.start(),mp.end(),abs(my.start()-mp.start())))
                        if abs(my.start()-mp.start()) < menor:
                            menor = abs(my.start()-mp.start())
                            if my.start() > mp.start():
                                pE1 = mp.start()
                                pE2 = my.end()
                                pI1 = mp.end()
                                pI2 = my.start()
                            else:
                                pE1 = my.start()
                                pE2 = mp.end()
                                pI1 = my.end()
                                pI2 = mp.start()
                if menor != len(strSource):
                    if len(strSource[pI1:pI2]) == 11:
                        booEncontrou = True
                        # strSource[pE1:pE2] contém o resultado exterior (não uso para nada)
                        strLink = strSource[pI1:pI2] # Contém resultado interior (que é o link nesse caso)
        return booEncontrou, strLink

    def procuraYoutube_OLD(self,objE,srcProximos,srcTodos):
        textoProcura = objE.strTitulo
        textoInicio = 'title="' + objE.strTipo + ' - ' + objE.strTitulo
        textoFinal = '>' + objE.strTipo + ' - ' + objE.strTitulo
        self.o(printA('Procurando "{}"'.format(textoProcura)))

        booEncontrou = False
        strTextoLink = ''
        strLink = ''
        # print('Procurando nos proximos')
        m = self.meu_re_search(textoProcura,srcProximos)
        if (m):
            mI = self.meu_re_search(textoInicio,srcProximos)
            mF = self.meu_re_search(textoFinal,srcProximos)
            if (mI):
                if (mF):
                    spanI = mI.span()
                    spanF = mF.span()
                    posI = spanI[1]
                    posF = spanF[0]
                    strTextoLink = srcProximos[posI:posF]
                    # print('Texto Link Proximos = {}'.format(strTextoLink))
                    booEncontrou = True
        else:
            # print('Procurando em todos')
            m = self.meu_re_search(textoProcura, srcTodos)
            if (m):
                mI = self.meu_re_search(textoInicio,srcTodos)
                mF = self.meu_re_search(textoFinal,srcTodos)
                if (mI):
                    if (mF):
                        spanI = mI.span()
                        spanF = mF.span()
                        posI = spanI[1]
                        posF = spanF[0]
                        strTextoLink = srcTodos[posI:posF]
                        # print('Texto Link Todos = {}'.format(strTextoLink))
                        booEncontrou = True
        if (booEncontrou):
            strToFind = 'href="/watch?v='
            posLI = strTextoLink.find(strToFind)
            # print(posLI)
            if (posLI > 0):
                posLI = posLI + len(strToFind)
                posLF = strTextoLink.find('"',posLI)
                # print(posLF)
                if (posLF > 0):
                    strLink = strTextoLink[posLI:posLF]
        return booEncontrou,strLink

    def on_miExibirPrincipal_activate(self,button):
        Builder.get_object("stackTabelas").set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
        Builder.get_object("stackTabelas").set_visible_child(Builder.get_object("grupoPrimeiro"))

    def on_miExibirRelatorio_activate(self,button):
        self.atualizarRelatorio()
        Builder.get_object("stackTabelas").set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
        Builder.get_object("stackTabelas").set_visible_child(Builder.get_object("grupoSegundo"))

    def sair(self):
        print('Saindo...')
        Gtk.main_quit()

    def on_main_window_destroy(self, Window):
        self.sair()

    def on_btnSair_clicked(self,button):
        self.sair()

    def msgConfirmacao(self, param, param1, param2):
        mensagem: Gtk.MessageDialog = Builder.get_object("msgConfirmacao")
        mensagem.props.text = param
        mensagem.props.secondary_text = param1
        mensagem.props.icon_name = param2
        mensagem.show_all()
        mensagem.run()
        mensagem.hide()

    def inputBox(self, strTitulo, strSubtitulo,booSomenteConfirma = False):
        inputbox: Gtk.Dialog = Builder.get_object("inputBox")
        Builder.get_object("inputBox_lbTitulo").set_label(strTitulo)
        Builder.get_object("inputBox_lbSubtitulo").set_label(strSubtitulo)
        if booSomenteConfirma:
            # Configura
            Builder.get_object("inputBox_Entry").props.visible = False
            # Retorna
            retorno = inputbox.run()
            inputbox.hide()
            if retorno == -5:
                return True
            else:
                return False
        else:
            # Configura
            Builder.get_object("inputBox_Entry").props.visible = True
            # Retorna
            retorno = inputbox.run()
            inputbox.hide()
            retText = ''
            if retorno == -5:
                retText = Builder.get_object("inputBox_Entry").get_text()
                return True, retText
            else:
                return False, retText

    # Nem precisa disso mas vou deixar como aprendizado
    def on_inputBox_response(self, widget, response):
        pass
        #print(response)
        #if response == -5:
        #    print("OK")
        #else:
        #    print("Cancelar")


    def dlgCalendar(self):
        dtHoje = datetime.datetime.today()
        tdUmDia = datetime.timedelta(days=1)
        tdTresDias = datetime.timedelta(days=3)
        numWeek = dtHoje.weekday()

        if (numWeek == 4):
            dtPadrao = dtHoje + tdTresDias
        else:
            dtPadrao = dtHoje + tdUmDia

        dlgCalendar: Gtk.Dialog = Builder.get_object("dlgCalendar")
        cdCalendar: Gtk.Calendar = Builder.get_object("dlgCalendar_Calendar")
        cdCalendar.select_month(int(dtPadrao.strftime("%-m"))-1, int(dtPadrao.strftime("%Y")))
        cdCalendar.select_day(int(dtPadrao.strftime("%-d")))
        retorno = dlgCalendar.run()
        dlgCalendar.hide()
        if retorno == -5:
            y,m,d = cdCalendar.get_date()
            m = m + 1 # O mes sempre vem com offset em 0
            dtData = datetime.date(y,m,d)
            return dtData
        else:
            return None

    def pegaCsv(self,dtAlvoUser = None):
        objTentativa = self.criaNovaTentativa()
        objTentativa.numClasse = 1
        objTentativa.save()

        tdUmDia = datetime.timedelta(days=1)
        tdTresDias = datetime.timedelta(days=3)

        self.dtHoje = datetime.date.today()
        # Para testar
        # dtHoje = datetime.date(2022,3,21)

        numWeek = self.dtHoje.weekday()

        if (numWeek == 4):
            self.dtAlvo = self.dtHoje + tdTresDias
        else:
            self.dtAlvo = self.dtHoje + tdUmDia

        if dtAlvoUser != None:
            if dtAlvoUser > self.dtAlvo:
                self.dtAlvo = dtAlvoUser

        self.o(printY("Buscando eventos entre {} e {}".format(self.dtHoje.strftime('%d/%m/%Y'),self.dtAlvo.strftime('%d/%m/%Y'))))

        dtAlvoDe = datetime.datetime(self.dtHoje.year, self.dtHoje.month, self.dtHoje.day, hour=0, minute=0, second=0, microsecond=0)
        dtAlvoTo = datetime.datetime(self.dtAlvo.year, self.dtAlvo.month, self.dtAlvo.day, hour=23, minute=59, second=59, microsecond=0)

        miliAlvoDe = int(dtAlvoDe.timestamp() * 1000)
        miliAlvoTo = int(dtAlvoTo.timestamp() * 1000)

        strGet = 'https://www.cl.df.gov.br/web/guest/agenda?'\
                    'p_p_id=br_com_seatecnologia_in_CalendarListPortlet'\
                    '&p_p_lifecycle=2&p_p_state=normal'\
                    '&p_p_mode=view'\
                    '&p_p_resource_id=calendar%2Fbookings%2Fexport_csv'\
                    '&p_p_cacheability=cacheLevelPage'\
                    '&_br_com_seatecnologia_in_CalendarListPortlet_isSearch=false'\
                    '&_br_com_seatecnologia_in_CalendarListPortlet_startTime={}'\
                    '&_br_com_seatecnologia_in_CalendarListPortlet_endTime={}'.format(miliAlvoDe,miliAlvoTo)

        directory = "/home/ronie/prog/ved/data"

        options = Options()
        options.set_preference("browser.download.folderList", 2)
        options.set_preference("browser.download.manager.showWhenStarting", False)
        options.set_preference("browser.download.dir", directory)
        options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-gzip")
        options.set_preference("http.response.timeout", 5)
        options.set_preference("dom.max_script_run_time", 5)

        browser = webdriver.Firefox(options=options)
        browser.set_page_load_timeout(5)
        browser.implicitly_wait(5)

        try:
            browser.get(strGet)
        except:
            pass

        browser.close()
        browser.quit()

        dataPath = '/home/ronie/prog/ved/data/'
        onlyfiles = [f for f in listdir(dataPath) if isfile(join(dataPath, f))]
        for strFile in onlyfiles:
            if strFile.find('Guest') > -1:
                self.o(printL("Arquivo CSV baixado com sucesso!"))
                objTentativa.booSucesso = True
                objTentativa.save()
                break
        if not (objTentativa.booSucesso):
            self.o(printR("Arquivo CSV não foi baixado!"))
        return objTentativa

    def criaNovaTentativa(self):
        objTentativa = Tentativa()
        objTentativa.save()
        return objTentativa

    def criaEventoSe(self,pstrAgenda, pstrTitulo, pstrLocal, pstrTipo, pstrAutor):
        qryFiltro = Evento.objects.filter(strAgenda = pstrAgenda,
                                          strTitulo = pstrTitulo,
                                          strLocal = pstrLocal,
                                          strTipo = pstrTipo,
                                          strAutor = pstrAutor)
        if qryFiltro.count() == 0:
            objNovo = Evento(strAgenda = pstrAgenda,
                             strTitulo = pstrTitulo,
                             strLocal = pstrLocal,
                             strTipo = pstrTipo,
                             strAutor = pstrAutor)
            objNovo.save()
            return objNovo, True
        else:
            return qryFiltro[0], False

    def carregaCsv(self,objTentativa):
        dataPath = '/home/ronie/prog/ved/data/'
        onlyfiles = [f for f in listdir(dataPath) if isfile(join(dataPath, f))]

        # Encontra arquivo que começa com Guest
        i = 0
        for strFile in onlyfiles:
            if strFile.find('Guest') > -1:
                break
            else:
                i = i + 1

        strCSVFull = dataPath + onlyfiles[i]
        strFileFull = dataPath + 'agenda{:05d}-{}.csv'.format(len(onlyfiles),self.dtAlvo.strftime('%Y-%m-%d'))
        os.rename(strCSVFull, strFileFull)

        df = pd.read_csv(strFileFull,sep=';',header=1)
        df = df.rename(str.strip, axis='columns')
        df = df.rename(str.lower, axis='columns')
        df = df.fillna('')
        filtro = df.loc[df['tipo'].isin(['Sessão Solene','Sessão Solene Remota','Audiência Pública','Audiência Pública Remota'])]
        shape = filtro.shape
        numLinhas = shape[0]
        if (numLinhas > 0):
            self.o(printL("{} eventos entre {} e {}".format(numLinhas,self.dtHoje.strftime('%d/%m/%Y'),self.dtAlvo.strftime('%d/%m/%Y'))))
            for i in range(0,numLinhas):
                objEvento, booRet = self.criaEventoSe(filtro.iloc[i]['agenda'], filtro.iloc[i]['título'], filtro.iloc[i]['local'], filtro.iloc[i]['tipo'], filtro.iloc[i]['autor'])
                if (booRet):
                    objEvento.dthrInicio = make_aware(datetime.datetime.strptime(filtro.iloc[i]['data início'],'%d/%m/%Y %H:%M'))
                    objEvento.dthrFinal = make_aware(datetime.datetime.strptime(filtro.iloc[i]['data fim'],'%d/%m/%Y %H:%M'))
                    objEvento.strNumerodocumento = filtro.iloc[i]['numerodocumento']
                    objEvento.fkTentativa = objTentativa
                    objEvento.save()
                    if objEvento.strTitulo != '':
                        self.o(printY('Evento "' + objEvento.strTitulo + '" criado!'))
                    else:
                        self.o(printL('Evento do tipo "' + objEvento.strTipo + '" criado com título vazio!'))
                else:
                    self.o(printR('Evento "' + objEvento.strTitulo + '" já existe!'))
        else:
            self.o(printR("Não há eventos entre {} e {}".format(self.dtHoje.strftime('%d/%m/%Y'),self.dtAlvo.strftime('%d/%m/%Y'))))

    def meu_re_search(self,strPattern,strString,booAntes = False):
        if booAntes:
            return re.search(re.escape(strPattern), strString)
        else:
            strEscape = re.escape(strPattern)
            strSearch = strEscape.replace("\ ","\ *")
            return re.search(strSearch,strString)

Builder = Gtk.Builder()
Builder.add_from_file(strGladeFullFilename)
Builder.connect_signals(Manipulador())
Window: Gtk.Window = Builder.get_object("main_window")
Window.show_all()
Gtk.main()
