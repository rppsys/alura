https://www.youtube.com/watch?v=Yg5zkd9nm6w

💻 Code - Django: https://github.com/SteinOveHelset/dja...
💻 Code - Vue: https://github.com/SteinOveHelset/dja...

🔗 Live demo: https://djackets.codewithstein.com/
🔗 The files and commands I use on the server can be found in the repository here: https://github.com/SteinOveHelset/dja...

⭐️ Course Contents ⭐️
⌨️ (0:00:00) Intro
⌨️ (0:00:55) Demo
⌨️ (0:02:41) Install and setup (Django)
⌨️ (0:11:39) Install and setup (Vue)
⌨️ (0:15:49) Include Font Awesome
⌨️ (0:16:28) Set up the base template
⌨️ (0:22:55) Create django app and models for products
⌨️ (0:33:19) Create serializer and views for the products
⌨️ (0:39:50) Create simple front page (latest products)
⌨️ (0:45:40) View a product 
⌨️ (0:55:57) Sette opp Vuex / State
⌨️ (0:57:27) Make it possible to add to cart
⌨️ (1:05:31) Implement a loading bar
⌨️ (1:08:59) Set document title (All pages)
⌨️ (1:09:58) Make it possible to view a category
⌨️ (1:21:07) Add search functionality
⌨️ (1:28:07) View cart
⌨️ (1:34:34) Change contents of cart
⌨️ (1:38:14) Make it possible to sign up
⌨️ (1:43:34) Make it possible to log in
⌨️ (1:48:38) Create a simple my account page
⌨️ (1:53:24) Proceed to checkout (Authentication)
⌨️ (1:58:59) Create a simple success page
⌨️ (2:14:29) Show my orders on the my account page
⌨️ (2:18:41) Deploy Django 
⌨️ (2:40:39) Deploy Vue (Generate files locally, Send files to server, Set up nginx virtual host)https://www.youtube.com/watch?v=Yg5zkd9nm6w

💻 Code - Django: https://github.com/SteinOveHelset/dja...

> https://github.com/SteinOveHelset/djackets_django

💻 Code - Vue: https://github.com/SteinOveHelset/dja...

> https://github.com/SteinOveHelset/djackets_vue

https://twitter.com/codewithstein


🔗 Live demo: https://djackets.codewithstein.com/
🔗 The files and commands I use on the server can be found in the repository here: https://github.com/SteinOveHelset/dja...

⭐️ Course Contents ⭐️
⌨️ (0:00:00) Intro
⌨️ (0:00:55) Demo
⌨️ (0:02:41) Install and setup (Django)
⌨️ (0:11:39) Install and setup (Vue)
⌨️ (0:15:49) Include Font Awesome
⌨️ (0:16:28) Set up the base template
⌨️ (0:22:55) Create django app and models for products
⌨️ (0:33:19) Create serializer and views for the products
⌨️ (0:39:50) Create simple front page (latest products)
⌨️ (0:45:40) View a product 
⌨️ (0:55:57) Sette opp Vuex / State
⌨️ (0:57:27) Make it possible to add to cart
⌨️ (1:05:31) Implement a loading bar
⌨️ (1:08:59) Set document title (All pages)
⌨️ (1:09:58) Make it possible to view a category
⌨️ (1:21:07) Add search functionality
⌨️ (1:28:07) View cart
⌨️ (1:34:34) Change contents of cart
⌨️ (1:38:14) Make it possible to sign up
⌨️ (1:43:34) Make it possible to log in
⌨️ (1:48:38) Create a simple my account page
⌨️ (1:53:24) Proceed to checkout (Authentication)
⌨️ (1:58:59) Create a simple success page
⌨️ (2:14:29) Show my orders on the my account page
⌨️ (2:18:41) Deploy Django 
⌨️ (2:40:39) Deploy Vue (Generate files locally, Send files to server, Set up nginx virtual host)

----------------------------



In this video, I will show you how to build a web app from scratch with Django / Django Rest Framework for the backend and Vue for the frontend (standalone).

The web app will have a lot of functionality like authentication, dashboard, possibility to generate pdf invoices and send them, register payments, send reminders, etc.

00:00:00 Introduction
00:00:54 Setup and basics
00:24:30 Authentication
00:46:30 Clients
01:18:30 Teams
01:38:30 Invoices
02:13:30 Misc
02:42:30 Refactoring
03:00:30 PDF
03:29:30 Credit notes
04:00:30 Deployment

---

Code With Stein Premium

If you want to learn even more from me, check out my website https://codewithstein.com where you can track progress, talk to me and similar :-D

Support me

Donate: https://www.paypal.com/paypalme/codew...
Patreon: https://patreon.com/codewithstein
Merch: https://shop.spreadshirt.com/code-wit...

Important links

Discord: https://discord.gg/GJCvwc8EGs
Github: https://github.com/SteinOveHelset/inv... and https://github.com/SteinOveHelset/inv...
Twitter: https://twitter.com/codewithstein
Website: https://codewithstein.com


