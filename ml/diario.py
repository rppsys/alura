#!/usr/bin/env python
# coding=utf-8
import os
import sys
import gi
import subprocess
import time

# https://www.nltk.org/
from nltk.tokenize import wordpunct_tokenize
from nltk.tokenize import WordPunctTokenizer
from nltk.tokenize import WhitespaceTokenizer
from nltk.tokenize import SpaceTokenizer


# https://www.datacamp.com/community/tutorials/argument-parsing-in-python
import argparse

from datetime import datetime

gi.require_version('Gtk','3.0')
from gi.repository import Gtk,Gdk,Pango,GLib

#python3 diario.py -d dev

# Os Strings de Pasta Dir nunca terão a última /
strSep = os.path.sep
strPathDir = "/home/ronie/prog/diario"

strGladeFullFilename = strPathDir  + strSep +  "diario.glade"
strDiarioFullFilename = strPathDir + strSep +  "diario.md"
strPastaRootDir = ""
strPastaRootDirFull = ""

print('strPathDir: ' + strPathDir)
print('strGladeFullFilename: ' + strGladeFullFilename)
print('strDiarioFullFilename: ' + strGladeFullFilename)

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--developer", required=False,help="Executar como desenvolvedor")
args =  vars(ap.parse_args())
if args['developer'] != None:
    print('Modo Desenvolvedor')
    booDev = True
else:
    print('Modo Usuário')
    booDev = False


class Manipulador:
    def __init__(self):
        # ----------------
        # Inicializações Gerais
        # ----------------
        self.Stack: Gtk.Stack = Builder.get_object("stackMain")

        # ----------------
        # TextView e TextBuffer
        # ----------------
        self.tvDiarioNotas: Gtk.TextView = Builder.get_object("tvDiarioNotas")
        self.tbDiarioNotas: Gtk.TextBuffer = Builder.get_object("tbDiarioNotas") #Não vou acessar diretamente
        self.lbStatus: Gtk.Label = Builder.get_object("lbStatusEsquerda")
        self.lbStatusDireita: Gtk.Label = Builder.get_object("lbStatusDireita")

        self.lbStatus.set_text("Iniciado")


        # Tags
        self.tvDiarioNotas.textbuffer = self.tvDiarioNotas.get_buffer()


        # Lista de Propriedades:
        # https://python-gtk-3-tutorial.readthedocs.io/en/latest/textview.html
        # https://docs.gtk.org/Pango/index.html
        # https://docs.gtk.org/Pango/enum.Weight.html
        # https://valadoc.org/pango/Pango.Underline.html
        # https://docs.gtk.org/Pango/pango_markup.html


        # Essas cores estão funcionando:
        # https://www.w3schools.com/colors/colors_names.asp


        self.tvDiarioNotas.tag_bold = self.tvDiarioNotas.textbuffer.create_tag("bold", weight=Pango.Weight.BOLD, foreground="yellow")
        self.tvDiarioNotas.tag_italic = self.tvDiarioNotas.textbuffer.create_tag("italic", style=Pango.Style.ITALIC)
        self.tvDiarioNotas.tag_underline = self.tvDiarioNotas.textbuffer.create_tag("underline", underline=Pango.Underline.SINGLE)
        self.tvDiarioNotas.tag_strike = self.tvDiarioNotas.textbuffer.create_tag("strike", strikethrough=True)

        self.tvDiarioNotas.tag_found = self.tvDiarioNotas.textbuffer.create_tag("found", background="yellow")

        # q1 = #, q2 = ## e assim por diante
        self.tvDiarioNotas.tag_quote = self.tvDiarioNotas.textbuffer.create_tag("quote", font="Ubuntu 14", background="DimGrey", foreground="GhostWhite")
        self.tvDiarioNotas.tag_q1 = self.tvDiarioNotas.textbuffer.create_tag("q1", font="Ubuntu 20", weight=Pango.Weight.BOLD, foreground="DodgerBlue")
        self.tvDiarioNotas.tag_q2 = self.tvDiarioNotas.textbuffer.create_tag("q2", font="Ubuntu 18", weight=Pango.Weight.BOLD, style=Pango.Style.ITALIC, foreground="DarkCyan")
        self.tvDiarioNotas.tag_q3 = self.tvDiarioNotas.textbuffer.create_tag("q3", font="Ubuntu 16", foreground="LightSkyBlue")
        self.tvDiarioNotas.tag_q4 = self.tvDiarioNotas.textbuffer.create_tag("q4", font="Ubuntu 14", style=Pango.Style.ITALIC, foreground="MediumTurquoise")

        # ----------------
        # Configurações da MainWindow
        # ----------------
        self.MainWindow: Gtk.Window = Builder.get_object("main_window")
        # https://lazka.github.io/pgi-docs/Gtk-3.0/classes/Widget.html#Gtk.Widget
        self.MainWindow.connect("key-press-event",self.on_key_press_event)
        self.MainWindow.connect("key-release-event",self.on_key_release_event)

        # ----------------
        # Configurações de Alguns Widgets
        # ----------------
        self.tbDiarioNotas.connect("modified-changed",self.on_buffer_modified_changed)
        self.tbDiarioNotas.connect("changed",self.on_buffer_changed)

        # Faz Pull
        if not booDev:
            self.gitPull()

        print('Diário Inicializado!')
                
        # Verifica se Pasta Root Existe
        self.verificaPastaRootDir()
        self.verificaArquivos()

        # Busca Texto Passado
        self.abrir()
        print('Diário Atualizado!')

    # https://riptutorial.com/gtk3/example/16426/simple-binding-to-a-widget-s-key-press-event
    def on_key_press_event(self, widget, event):
        # print("Key press on widget: ", widget.get_name())
        # print("          Modifiers: ", event.state)
        # print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))

        # check the event modifiers (can also use SHIFTMASK, etc)
        ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)
        alt = (event.state & Gdk.ModifierType.MOD1_MASK)
        shift = (event.state & Gdk.ModifierType.SHIFT_MASK)

        # Para simplificar código
        #https://lazka.github.io/pgi-docs/Gtk-3.0/classes/TextView.html
        buf = self.tvDiarioNotas.textbuffer

        # see if we recognise a keypress
        if ctrl:
            if alt:
                #ctrl + alt + h
                if event.keyval == Gdk.KEY_h:
                    self.on_btnInserirN1_clicked(self)
                #ctrl + alt + j
                if event.keyval == Gdk.KEY_j:
                    self.on_btnInserirN2_clicked(self)
                #ctrl + alt + k
                if event.keyval == Gdk.KEY_k:
                    self.on_btnInserirN3_clicked(self)
                #ctrl + alt + j
                if event.keyval == Gdk.KEY_l:
                    self.on_btnInserirN4_clicked(self)
            else:
                #ctrl + s
                if event.keyval == Gdk.KEY_s:
                    self.on_btnSalvar_clicked(self)
        else:
            if alt:
                pass
            else: # Não tem Ctrl nem Alt
                if ((event.keyval == Gdk.KEY_Up) or (event.keyval == Gdk.KEY_Down) or (event.keyval == Gdk.KEY_Left) or (event.keyval == Gdk.KEY_Right)):
                    pass

    def on_key_release_event(self, widget, event):
        #print("Key release on widget: ", widget.get_name())
        #print("          Modifiers: ", event.state)
        #print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))
        # check the event modifiers (can also use SHIFTMASK, etc)
        ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)
        alt = (event.state & Gdk.ModifierType.MOD1_MASK)
        shift = (event.state & Gdk.ModifierType.SHIFT_MASK)


    def on_buffer_modified_changed(self,text_buffer):
        self.lbStatusDireita.set_text('Modificado')

    def on_buffer_changed(self,text_buffer):
        self.calculaParametros()

    # https://janakiev.com/blog/python-shell-commands/
    def teste(self):
        print('----- TESTE ----- ')
        stream = os.popen('git -C ' + strPathDir  + ' status')
        o = stream.read()
        print(o)
        print('----- TESTE ----- ')

    def getPath(self):
        stream = os.popen('readlink -e .')
        o = stream.read()
        print(o)

    def gitPush(self):
        today = datetime.today()
        strCommit = today.strftime("Auto %Y%m%d %-H:%M:%S")
        print("----- PUSH ----- ")
        stream = os.popen('git -C ' + strPathDir  + ' add --all')
        o = stream.read()
        print(o)
        stream = os.popen('git -C ' + strPathDir  + ' commit -m "' + strCommit + '"')
        o = stream.read()
        print(o)
        stream = os.popen('git -C ' + strPathDir  + ' push')
        o = stream.read()
        print(o)
        print("----- PUSH ----- ")

    def gitPull(self):
        print("----- PULL ----- ")
        stream = os.popen('git -C ' + strPathDir  + ' pull')
        o = stream.read()
        print(o)
        print("----- PULL ----- ")


    def on_btnSalvar_clicked(self,button):
        self.salvar()
        self.msgConfirmacao('Informação','Salvo!','dialog-emblem-default')

    # Salva tanto o texto sem formatação quanto o pango
    def salvar(self):
        # https://python-gtk-3-tutorial.readthedocs.io/en/latest/textview.html
        string = self.tbDiarioNotas.get_text(self.tbDiarioNotas.get_start_iter(),self.tbDiarioNotas.get_end_iter(),True)
        fileDiario = open(self.strArquivoDiarioFull, "w")
        fileDiario.write(string)
        fileDiario.close()
        print('Diário Salvo!')

        #https://stackoverflow.com/questions/51851321/pygtk-serialization
        buf = self.tvDiarioNotas.get_buffer()
        start, end = buf.get_bounds()
        tag_format = buf.register_serialize_tagset()
        content = buf.serialize(buf, tag_format, start, end)
        GLib.file_set_contents(self.strArquivoPangoFull,content)

        # Estado
        self.tbDiarioNotas.set_modified(False)
        self.lbStatusDireita.set_text('Salvo!')
        print('Pango Salvo!')

    # Abre o arquivo pango
    def abrir(self):
        # https://webreflection.github.io/gjs-documentation/GLib-2.0/index.html
        buf = self.tvDiarioNotas.get_buffer()
        start = buf.get_start_iter()
        tag_format = buf.register_deserialize_tagset()
        data = GLib.file_get_contents(self.strArquivoPangoFull)
        content = data.contents
        buf.deserialize(buf,tag_format,start,content)
        # self.lbStatusDireita.set_text('Carregado')
        print('Pango Aberto!')

    # Não estou mais usando - Isso abre o texto sem formatação que eu gravo
    def abrirMD(self):
        fileDiario = open(self.strArquivoDiarioFull, "r")
        string = fileDiario.read()
        self.tbDiarioNotas.set_text(string)
        fileDiario.close()

    def on_btnInserirN1_clicked(self,button):
        self.tbDiarioNotas.insert_at_cursor('# ')

    def on_btnInserirN2_clicked(self,button):
        self.tbDiarioNotas.insert_at_cursor('## ')

    def on_btnInserirN3_clicked(self,button):
        self.tbDiarioNotas.insert_at_cursor('### ')

    def on_btnInserirN4_clicked(self,button):
        self.tbDiarioNotas.insert_at_cursor('#### ')

    def on_main_window_destroy(self, Window):
        # Salva
        self.salvar()

        #Faz Push se não estiver no modo Desenvolvedor
        if not booDev:
            self.gitPush()
            print('Saindo em 3')
            time.sleep(1)
            print('2')
            time.sleep(1)
            print('1')
        Gtk.main_quit()

    def msgConfirmacao(self, param, param1, param2):
        mensagem: Gtk.MessageDialog = Builder.get_object("msgConfirmacao")
        mensagem.props.text = param
        mensagem.props.secondary_text = param1
        mensagem.props.icon_name = param2
        mensagem.show_all()
        mensagem.run()
        mensagem.hide()

    # Verifica se a pasta do Ano Atual Existe, se não existir, cria
    def verificaPastaRootDir(self):
        today = datetime.today()
        self.strPastaRootDir = today.strftime("pasta%Y")
        self.strPastaRootDirFull = strPathDir + strSep + self.strPastaRootDir
        print('strPastaRootDirFull: ' + self.strPastaRootDirFull)
        if not os.path.isdir(self.strPastaRootDirFull):
            print("PastaRootDir não existe. Será criada.")
            # https://www.geeksforgeeks.org/create-a-directory-in-python/
            os.mkdir(self.strPastaRootDirFull)
            print("PastaRootDir: " + self.strPastaRootDirFull + ' criada!')
        else:
            print("PastaRootDir: " + self.strPastaRootDirFull + ' já existe!')

    # Verifica se arquivo de indice existe na PastaRoot, se não existir, cria o arquivo
    def verificaArquivos(self):
        today = datetime.today()
        strHoje = today.strftime("%A, %-d de %B de %Y").capitalize()
        strFormato = today.strftime("%Y%m%d")

        self.strArquivoDiarioFilename = "data" + strFormato + ".md"
        self.strArquivoPangoFilename = "pango" + strFormato + ".pango"

        self.strArquivoDiarioFull = self.strPastaRootDirFull + strSep + self.strArquivoDiarioFilename
        self.strArquivoPangoFull = self.strPastaRootDirFull + strSep + self.strArquivoPangoFilename

        if not os.path.isfile(self.strArquivoDiarioFull):
            print("Diário: " + self.strArquivoDiarioFilename + " não existe! Será criado!")
            fileDiario = open(self.strArquivoDiarioFull, "w")
            fileDiario.write("# " + strHoje)
            fileDiario.close()
        else:
            print("Diário: " + self.strArquivoDiarioFilename + " já existe!")

        if not os.path.isfile(self.strArquivoPangoFull):
            print("Pango: " + self.strArquivoPangoFilename + " não existe! Será criado!")
            buf = self.tvDiarioNotas.get_buffer()
            buf.insert(buf.get_end_iter(),"# " + strHoje)
            start, end = buf.get_bounds()
            tag_format = buf.register_serialize_tagset()
            content = buf.serialize(buf, tag_format, start, end)
            GLib.file_set_contents(self.strArquivoPangoFull,content)
            buf.set_text("",0)
        else:
            print("Pango: " + self.strArquivoPangoFilename + " já existe!")


    def calculaParametros(self):
        # -----------------------
        # Inicializações
        # -----------------------
        # Buffer Alvo
        buf = self.tvDiarioNotas.textbuffer
        # print("")
        iterStart = buf.get_start_iter()
        iterEnd = buf.get_end_iter()

        # Total de Linhas e Posicao do Cursor
        numLinhaTotal = buf.get_line_count()
        numPosCursor = buf.props.cursor_position
        # print("Total de Linhas = {}".format(numLinhaTotal))
        # print("Pos Cursor = {}".format(numPosCursor))


        # Linha Atual
        m = buf.get_insert()
        i = buf.get_iter_at_mark(m)
        numLinhaAtual = i.get_line()
        # print("Linha Atual = {}".format(numLinhaAtual))


        # Texto da Linha Atual
        #https://lazka.github.io/pgi-docs/Gtk-3.0/classes/TextIter.html#Gtk.TextIter
        iterLineStart = buf.get_iter_at_line(numLinhaAtual)
        if ((numLinhaAtual + 1) != numLinhaTotal):
            iterLineEnd = buf.get_iter_at_line(numLinhaAtual+1)
            iterLineEnd.backward_char()
        else:
            iterLineEnd = buf.get_end_iter()
        strLinhaTexto = buf.get_text(iterLineStart,iterLineEnd,False)
        # print("Texto da Linha = '{}'".format(strLinhaTexto))

        # -----------------------
        # Operações com o texto da Linha
        # -----------------------
        # https://www.nltk.org/api/nltk.tokenize.html
        listSep = WordPunctTokenizer().tokenize(strLinhaTexto)
        # print(listSep)

        listInd = list(WordPunctTokenizer().span_tokenize(strLinhaTexto))
        # print(listInd)

        # Remove todas as tags sempre e depois eu refaço
        buf.remove_all_tags(iterLineStart, iterLineEnd)


        # *Italic characters*
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_italic,'*',strLinhaTexto,numLinhaAtual)

        # _Italic characters_
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_italic,'_',strLinhaTexto,numLinhaAtual)

        # **bold characters**
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_bold,'**',strLinhaTexto,numLinhaAtual)

        # __bold characters__
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_bold,'__',strLinhaTexto,numLinhaAtual)

        # ~~strikethrough text~~
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_strike,'~~',strLinhaTexto,numLinhaAtual)


        if '>' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_quote, iterLineStart, iterLineEnd)
        elif '####' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q4, iterLineStart, iterLineEnd)
        elif '###' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q3, iterLineStart, iterLineEnd)
        elif '##' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q2, iterLineStart, iterLineEnd)
        elif '#' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q1, iterLineStart, iterLineEnd)



    def aplicaTagEmOperadores(self,tag,strOperador,strLinhaTexto,numLinhaAtual):
        buf = self.tvDiarioNotas.textbuffer
        listSep = WordPunctTokenizer().tokenize(strLinhaTexto)
        listInd = list(WordPunctTokenizer().span_tokenize(strLinhaTexto))
        # https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-in-a-list
        listOperador = [i for i, j in enumerate(listSep) if j == strOperador]
        #listOperador contem os indices de quando ** aparece
        # Preciso transformar em pares de indices
        llPares = []
        index = 0
        for i in listOperador:
            index = index + 1
            if (index % 2 == 0):
                llPares.append([listOperador[index-2],listOperador[index-1]])
        #Agora vou de par em par para fazer o apply tag
        # listPar[0] contem o indice da esquerda
        # listPar[1] contem o indice da direita
        for listPar in llPares:
            # Esses abaixo incluem os operadores
            numCharStart = listInd[listPar[0]][0]
            numCharEnd = listInd[listPar[1]][1]
            # Esses abaixo não incluem os operadores
            #numCharStart = listInd[listPar[0]][1]
            #numCharEnd = listInd[listPar[1]][0]
            # Agora preciso descobrir como pegar iter no char
            # print('Vai de {} até {}'.format(numCharStart,numCharEnd))
            iterPosStart = buf.get_iter_at_line(numLinhaAtual)
            # print("1")
            # print(iterPosStart.get_char())
            iterPosStart.forward_chars(numCharStart)
            # print("2")
            # print(iterPosStart.get_char())
            iterPosEnd = buf.get_iter_at_line(numLinhaAtual)
            # print("3")
            # print(iterPosEnd.get_char())
            iterPosEnd.forward_chars(numCharEnd)
            # print("4")
            # print(iterPosEnd.get_char())
            # Finalmente aplica a tag
            buf.apply_tag(tag, iterPosStart, iterPosEnd)


Builder = Gtk.Builder()
Builder.add_from_file(strGladeFullFilename)
Builder.connect_signals(Manipulador())
Window: Gtk.Window = Builder.get_object("main_window")
Window.show_all()
Gtk.main()
