#!/usr/bin/env bash
# Filename: ml.sh
cd ~/prog/vML/bin &&
source activate &&
cd ~/prog/alura/ml

if [[ $1 = 'pull' ]]
then
  git pull
fi

if [[ $1 = 'push' ]]
then
  git push
fi
