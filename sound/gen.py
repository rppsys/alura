from gensound import WAV, Gain, Triangle, Sine
from gensound.effects import Vibrato, Stretch

d1 = WAV('Bass-Drum-1.wav')
d2 = WAV('Bass-Drum-2.wav')
d3 = WAV('Bass-Drum-3.wav')
d4 = WAV('Boom-Kick.wav')

d1 = d1[0e3:0.25e3]
d2 = d2[0e3:0.25e3]
d3 = d3[0e3:0.25e3]
d4 = d4[0e3:0.25e3]

def dP(d,p):
    return d | float(p)

def dC(d,c):
    return d[0e3:float(c)]

def dCP(d,c,p):
    return d[0e3:float(c)] | float(p)


def dur(d):
    audio = d.realise(44100)
    return  (audio.length-1)/audio.sample_rate


