#!/usr/bin/env python
# coding=utf-8
import os
import sys
import gi
import subprocess
import time
import math

# https://www.nltk.org/
from nltk.tokenize import wordpunct_tokenize
from nltk.tokenize import WordPunctTokenizer
from nltk.tokenize import WhitespaceTokenizer
from nltk.tokenize import SpaceTokenizer


# https://www.datacamp.com/community/tutorials/argument-parsing-in-python
import argparse

import datetime

gi.require_version('Gtk','3.0')
from gi.repository import Gtk,Gdk,Pango,GLib

gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2

#python3 diario.py -d dev

print("")

clNoColor='\033[0m'
clBlack='\033[0;30m'
clRed='\033[0;31m'
clGreen='\033[0;32m'
clOrange='\033[0;33m'
clBlue='\033[0;34m'
clPurple='\033[0;35m'
clCyan='\033[0;36m'
clLight_Gray='\033[0;37m'
clDark_Gray='\033[1;30m'
clLight_Red='\033[1;31m'
clLight_Green='\033[1;32m'
clYellow='\033[1;33m'
clLight_Blue='\033[1;34m'
clLight_Purple='\033[1;35m'
clLight_Cyan='\033[1;36m'
clWhite='\033[1;37m'

def printCor(clColor,arg):
    print(clColor + arg + clNoColor)

def printL(arg):
    print(clOrange + arg + clNoColor)

def printR(arg):
    print(clRed + arg + clNoColor)

def printA(arg):
    print(clLight_Blue + arg + clNoColor)

def printC(arg):
    print(clLight_Cyan + arg + clNoColor)

def printY(arg):
    print(clYellow + arg + clNoColor)

# Os Strings de Pasta Dir nunca terão a última /
strSep = os.path.sep
strPathDir = "/home/ronie/prog/alura/sound"

strGladeFullFilename = strPathDir  + strSep +  "draw.glade"

strPastaRootDir = ""
strPastaRootDirFull = ""

printL('strPathDir: ' + strPathDir)
printL('strGladeFullFilename: ' + strGladeFullFilename)


ap = argparse.ArgumentParser()
ap.add_argument("-d", "--developer", required=False,help="Executar como desenvolvedor")
args =  vars(ap.parse_args())
if args['developer'] != None:
    print("")
    printY('##### Modo Desenvolvedor #####')
    print("")
    booDev = True
else:
    print("")
    printY('##### Modo Usuário #####')
    print("")
    booDev = False


class Manipulador:
    def __init__(self):
        # ----------------
        # Inicializações Gerais
        # ----------------

        # ----------------
        # Configurações da MainWindow
        # ----------------
        self.MainWindow: Gtk.Window = Builder.get_object("main_window")
        self.MainWindow.connect("key-press-event",self.on_key_press_event)
        self.MainWindow.connect("key-release-event",self.on_key_release_event)

        # ----------------
        # Configurações de Alguns Widgets
        # ----------------
        self.desenha()

    def desenha(self):
        draw = Builder.get_object("draw")


    def on_draw_draw(self):
        print('Desenhou')

    def on_main_window_destroy(self, Window):
        Gtk.main_quit()

    def on_key_press_event(self, widget, event):
        # print("Key press on widget: ", widget.get_name())
        # print("          Modifiers: ", event.state)
        # print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))

        # check the event modifiers (can also use SHIFTMASK, etc)
        ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)
        alt = (event.state & Gdk.ModifierType.MOD1_MASK)
        shift = (event.state & Gdk.ModifierType.SHIFT_MASK)

        # Para simplificar código
        #https://lazka.github.io/pgi-docs/Gtk-3.0/classes/TextView.html

        # see if we recognise a keypress
        if ctrl:
            if alt:
                #ctrl + alt + h
                if event.keyval == Gdk.KEY_h:
                    pass
                #ctrl + alt + j
                if event.keyval == Gdk.KEY_j:
                    pass
                #ctrl + alt + k
                if event.keyval == Gdk.KEY_k:
                    pass
                #ctrl + alt + j
                if event.keyval == Gdk.KEY_l:
                    pass
            else:
                #ctrl + s
                if event.keyval == Gdk.KEY_s:
                    pass
        else:
            if alt:
                pass
            else: # Não tem Ctrl nem Alt
                if ((event.keyval == Gdk.KEY_Up) or (event.keyval == Gdk.KEY_Down) or (event.keyval == Gdk.KEY_Left) or (event.keyval == Gdk.KEY_Right)):
                    pass

    def on_key_release_event(self, widget, event):
        #print("Key release on widget: ", widget.get_name())
        #print("          Modifiers: ", event.state)
        #print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))
        # check the event modifiers (can also use SHIFTMASK, etc)
        ctrl = (event.state & Gdk.ModifierType.CONTROL_MASK)
        alt = (event.state & Gdk.ModifierType.MOD1_MASK)
        shift = (event.state & Gdk.ModifierType.SHIFT_MASK)

Builder = Gtk.Builder()
Builder.add_from_file(strGladeFullFilename)
Builder.connect_signals(Manipulador())
Window: Gtk.Window = Builder.get_object("main_window")
Window.show_all()
Gtk.main()
