from gensound import WAV, test_wav
from gensound import Sine
from gensound import Gain

#wav = WAV(test_wav) # load sample WAV, included with gensound


#wav.play(sample_rate=32000)
#wav[1].play() # wav[0] is L channel, wav[1] is R
#wav = wav[5e3:6e3] # since 5e3 is float, gensound knows we are not talking about channels
#wav = wav**5

#wav[0,4e3:] += Sine(frequency=440, duration=2e3)*Gain(-9)

#wav.play()

#s = Sine('D5 C# A F# B G# E# C# F#', duration=0.5e3)
#s.play()



drum = WAV("Bass-Drum-1.wav") # load sample WAV, included with gensound
gui = WAV("Alesis-Fusion-Nylon-String-Guitar-C4.wav") # load sample WAV, included with gensound

drum = drum[0.0:500.0]
gui = gui[0.0:200.0]


novo = drum[100.0:] + gui


novo = novo**3

novo.realise(44100).plot()


novo.play()

