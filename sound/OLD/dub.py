from pydub import AudioSegment
from pydub.playback import play

#song2 = AudioSegment.from_wav("Bass-Drum-1.wav")
#song2.export("output.wav", format="wav")

ten_seconds = 10 * 1000

song = AudioSegment.from_mp3("sample.mp3")
s2 = 5 * 1000
f2 = song[:s2]

b = f2[:2000]
e = f2[-3000:]

# boost volume by 6dB
b = b + 6
e = e - 3
e = e*2

b = b.fade_in(2000).fade_out(3000)

with_style = b.append(e, crossfade=500)


play(with_style)

