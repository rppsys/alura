from gensound import WAV, test_wav
from gensound import Sine, Triangle
from gensound import Gain

drum = WAV("Bass-Drum-1.wav")
d1 = drum[0.0:200.0]
d2 = drum[0.0:400.0]

md = d1

x = 100.0

for i in range(1,6):
    x = x | md**i
    x = x | 1000.0
    print(i)

x.play()

# https://www.noiiz.com/sounds/instruments
