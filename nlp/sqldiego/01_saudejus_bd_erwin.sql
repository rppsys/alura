CREATE TABLE absenteismo
( 
	id_absenteismo       integer  NOT NULL ,
	dth_absenteismo      datetime  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	id_cid               integer  NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	sin_interno          char(1)  NOT NULL ,
	id_atestado_externo_homologacao integer  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_documento_externo integer  NULL ,
	arquivo              varchar(200)  NULL ,
	id_grau_parentesco   integer  NULL ,
	justificativa        varchar(500)  NULL 
)
go

ALTER TABLE absenteismo
	ADD CONSTRAINT XPKabsenteismo PRIMARY KEY (id_absenteismo ASC)
go

CREATE TABLE absenteismo_motivo
( 
	id_absenteismo_motivo integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sigla                varchar(10)  NOT NULL ,
	sin_informa_paciente char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	sigla_integracao     varchar(20)  NULL 
)
go



ALTER TABLE absenteismo_motivo
	ADD CONSTRAINT XPKabsenteismo_motivo PRIMARY KEY (id_absenteismo_motivo ASC)
go



CREATE TABLE agenda
( 
	id_agenda            integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	descricao            varchar(200)  NULL ,
--	dta_inicio           datetime  NOT NULL ,
--	dta_fim              datetime  NOT NULL ,
	sin_paciente_solicita char(1)  NOT NULL ,
	id_calendario_feriado integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE agenda
	ADD CONSTRAINT XPKagenda PRIMARY KEY (id_agenda ASC)
go



CREATE TABLE agenda_anotacao
( 
	id_agenda_anotacao   integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	data                 datetime  NOT NULL ,
	anotacao             varchar(3000)  NULL 
)
go



ALTER TABLE agenda_anotacao
	ADD CONSTRAINT XPKagenda_anotacao PRIMARY KEY (id_agenda_anotacao ASC)
go



CREATE TABLE agenda_bloco_adicional
( 
	id_agenda_bloco_adicional integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	dta_bloco            datetime  NOT NULL ,
	hora_inicio          integer  NOT NULL ,
	hora_fim             integer  NOT NULL ,
	duracao              integer  NOT NULL 
)
go



ALTER TABLE agenda_bloco_adicional
	ADD CONSTRAINT XPKagenda_bloco_adicional PRIMARY KEY (id_agenda_bloco_adicional ASC)
go



CREATE TABLE agenda_bloqueio
( 
	id_agenda_bloqueio   integer  NOT NULL ,
	id_agenda_horario    integer  NOT NULL ,
	justificativa        varchar(100)  NOT NULL ,
	id_tipo_bloqueio     integer  NOT NULL 
)
go



ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT XPKagenda_bloqueio PRIMARY KEY (id_agenda_bloqueio ASC)
go



CREATE TABLE agenda_configuracao
( 
	id_agenda_configuracao integer  NOT NULL ,
	id_historico_agenda  integer  NOT NULL ,
	dia_semana           integer  NULL ,
	hora_inicio          integer  NULL ,
	hora_fim             integer  NULL ,
	duracao              integer  NULL ,
	sin_paciente_solicita char(1)  NOT NULL 
)
go



ALTER TABLE agenda_configuracao
	ADD CONSTRAINT XPKagenda_configuracao PRIMARY KEY (id_agenda_configuracao ASC)
go



CREATE TABLE agenda_horario
( 
	id_agenda_horario    integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_paciente          integer  NULL ,
	id_tipo_consulta     integer  NULL ,
	data                 datetime  NULL ,
	hora_inicio          integer  NULL ,
	hora_fim             integer  NULL ,
	sta_horario          char(1)  NULL ,
	observacao           varchar(200)  NULL ,
	id_profissional_agenda integer  NULL ,
	id_profissional_marcacao integer  NULL ,
	sta_cancelado        varchar(1)  NULL ,
	sin_presencial       char(1)  NOT NULL 
)
go



ALTER TABLE agenda_horario
	ADD CONSTRAINT XPKagenda_horario PRIMARY KEY (id_agenda_horario ASC)
go



CREATE TABLE atestado_externo
( 
	id_atestado_externo  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	nome_medico          varchar(100)  NULL ,
	crm_medico           varchar(20)  NULL ,
	especialidade_medica varchar(50)  NULL ,
	nome_clinica         varchar(100)  NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	dta_atestado         datetime  NOT NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	sin_necessita_revisao char(1)  NOT NULL ,
	chave_integracao_rh  varchar(20)  NULL ,
	id_cid               integer  NULL ,
	id_profissional_saude_registro integer  NULL ,
	dth_registro         datetime  NOT NULL ,
	arquivo              varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	observacao           varchar(1000)  NULL ,
	justificativa        varchar(500)  NULL ,
	id_grau_parentesco   integer  NULL ,
	id_orgao             integer  NOT NULL ,
	id_especialidade     integer  NOT NULL 
)
go



ALTER TABLE atestado_externo
	ADD CONSTRAINT XPKatestado_externo PRIMARY KEY (id_atestado_externo ASC)
go



CREATE TABLE atestado_externo_arquivo
( 
	id_atestado_externo_arquivo integer  NOT NULL ,
	id_atestado_externo  integer  NOT NULL ,
	arquivo              varchar(200)  NOT NULL ,
	nome                 varchar(100)  NOT NULL 
)
go



ALTER TABLE atestado_externo_arquivo
	ADD CONSTRAINT XPKatestado_externo_arquivo PRIMARY KEY (id_atestado_externo_arquivo ASC)
go



CREATE TABLE atestado_externo_homologacao
( 
	id_atestado_externo_homologacao integer  NOT NULL ,
	id_atestado_externo  integer  NOT NULL ,
	sta_homologacao      char(1)  NULL ,
	sta_tipo_pericia     char(1)  NULL ,
	dth_homologacao      datetime  NULL ,
	resultado            text  NULL 
)
go



ALTER TABLE atestado_externo_homologacao
	ADD CONSTRAINT XPKatestado_externo_homologacao PRIMARY KEY (id_atestado_externo_homologacao ASC)
go



CREATE TABLE atestado_modelo
( 
	id_atestado_modelo   integer  NOT NULL ,
	titulo               varchar(100)  NOT NULL ,
	texto_modelo         varchar(8000)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE atestado_modelo
	ADD CONSTRAINT XPKatestado_modelo PRIMARY KEY (id_atestado_modelo ASC)
go



CREATE TABLE beneficio
( 
	id_beneficio         integer  NOT NULL ,
	id_beneficio_concessao integer  NULL ,
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	id_tipo_beneficio    integer  NOT NULL ,
	dta_pedido           datetime  NOT NULL ,
	processo             varchar(200)  NULL ,
	sta_motivo           char(1)  NOT NULL ,
	descricao            text  NULL ,
	dta_pericia          datetime  NULL ,
	resultado            text  NULL ,
	sta_resultado        char(1)  NULL ,
	id_cid               integer  NULL ,
	id_documento_externo integer  NULL ,
	anotacao             text  NULL ,
	arquivo_resultado    varchar(200)  NULL ,
	id_profissional_saude_registro integer  NULL ,
	id_orgao             integer  NOT NULL 
)
go



ALTER TABLE beneficio
	ADD CONSTRAINT XPKbeneficio PRIMARY KEY (id_beneficio ASC)
go



CREATE TABLE calendario_feriado
( 
	id_calendario_feriado integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	lista_dia_fixo       varchar(5000)  NULL ,
	lista_dia_movel      varchar(5000)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE calendario_feriado
	ADD CONSTRAINT XPKferiado PRIMARY KEY (id_calendario_feriado ASC)
go



CREATE TABLE cid
( 
	id_cid               integer  NOT NULL ,
	id_cid_principal     integer  NULL ,
	codigo               varchar(20)  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	apelido              varchar(100)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE cid
	ADD CONSTRAINT XPKcid PRIMARY KEY (id_cid ASC)
go



CREATE TABLE consulta
( 
	id_consulta          integer  NOT NULL ,
	id_tipo_consulta     integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	dth_consulta         datetime  NOT NULL ,
	evolucao             text  NULL ,
	id_agenda_horario    integer  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL 
)
go



ALTER TABLE consulta
	ADD CONSTRAINT XPKconsulta PRIMARY KEY (id_consulta ASC)
go



CREATE TABLE documento_externo
( 
	id_documento_externo integer  NOT NULL ,
	numero_processo      varchar(100)  NULL ,
	url_processo         text  NULL ,
	tipo_documento       varchar(100)  NULL ,
	numero_documento     varchar(100)  NULL ,
	url_documento        text  NULL ,
	conteudo_documento   text  NULL ,
	dth_envio            datetime  NULL ,
	retorno_sistema_externo text  NULL ,
	origem               varchar(20)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE documento_externo
	ADD CONSTRAINT XPKdocumento_externo PRIMARY KEY (id_documento_externo ASC)
go



CREATE TABLE especialidade
( 
	id_especialidade     integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_procedimento     char(1)  NOT NULL ,
	sin_cid              char(1)  NOT NULL ,
	sin_odontologia      char(1)  NOT NULL ,
	sin_historico_familiar char(1)  NOT NULL ,
	sin_medicamento_continuo char(1)  NOT NULL ,
	sin_historico_familiar_psicossocial char(1)  NOT NULL ,
	sin_encaminhamento_psicossocial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE especialidade
	ADD CONSTRAINT XPKespecialidade PRIMARY KEY (id_especialidade ASC)
go



CREATE TABLE evento
( 
	id_evento            integer  NOT NULL ,
	descricao            varchar(200)  NOT NULL ,
	qtd_vaga             integer  NOT NULL ,
	mensagem_usuario_recusa varchar(8000)  NULL ,
	mensagem_usuario_marcacao varchar(8000)  NULL ,
	mensagem_usuario_cancelamento varchar(8000)  NULL ,
	local                varchar(1000)  NULL ,
	ano_referencia       integer  NULL ,
	dth_inscricao_inicio datetime  NULL ,
	dth_inscricao_fim    datetime  NULL ,
	dta_evento           datetime  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	id_tipo_evento       integer  NOT NULL 
)
go



ALTER TABLE evento
	ADD CONSTRAINT XPKagenda_evento PRIMARY KEY (id_evento ASC)
go



CREATE TABLE evento_publico_alvo
( 
	id_evento_publico_alvo integer  NOT NULL ,
	id_evento            integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	mensagem             varchar(1000)  NULL ,
	sin_recusa           char(1)  NOT NULL ,
	dta_recusa           datetime  NULL ,
	sin_inscricao        char(1)  NOT NULL ,
	dth_inscricao        datetime  NULL ,
	sin_comparecimento   varchar(20)  NOT NULL 
)
go



ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT XPKevento_publico_alvo PRIMARY KEY (id_evento_publico_alvo ASC)
go



CREATE TABLE exame
( 
	id_exame             bigint  NOT NULL ,
	id_tipo_exame        integer  NOT NULL ,
	titulo               varchar(100)  NULL ,
	descricao            varchar(1000)  NULL ,
	arquivo              varchar(200)  NULL ,
	id_paciente          integer  NOT NULL ,
	id_laboratorio       integer  NULL ,
	dth_exame            datetime  NOT NULL ,
	dth_inclusao         datetime  NULL ,
	id_profissional_saude integer  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL 
)
go



ALTER TABLE exame
	ADD CONSTRAINT XPKexame PRIMARY KEY (id_exame ASC)
go



CREATE TABLE exame_resultado
( 
	id_exame_resultado   integer  NOT NULL ,
	txt_resultado        varchar(100)  NULL ,
	id_exame             bigint  NOT NULL ,
	id_tipo_exame_resultado integer  NOT NULL 
)
go



ALTER TABLE exame_resultado
	ADD CONSTRAINT XPKexame_resultado PRIMARY KEY (id_exame_resultado ASC)
go



CREATE TABLE grau_parentesco
( 
	id_grau_parentesco   integer  NOT NULL ,
	nome                 varchar(40)  NOT NULL ,
	chave_integracao     varchar(20)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE grau_parentesco
	ADD CONSTRAINT XPKgrau_parentesco PRIMARY KEY (id_grau_parentesco ASC)
go



CREATE TABLE historico_agenda
( 
	id_historico_agenda  integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	descricao            varchar(200)  NULL ,
	texto_agenda         varchar(2000)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE historico_agenda
	ADD CONSTRAINT XPKhistorico_agenda_configuracao PRIMARY KEY (id_historico_agenda ASC)
go



CREATE TABLE infra_log
( 
	id_infra_log         integer  NOT NULL ,
	dth_log              datetime  NULL ,
	texto_log            varchar(max)  NOT NULL ,
	ip                   varchar(15)  NULL ,
	sta_tipo             char(1)  NOT NULL 
)
go



ALTER TABLE infra_log
	ADD CONSTRAINT XPKinfra_log PRIMARY KEY (id_infra_log ASC)
go



CREATE TABLE infra_log_acesso
( 
	id_infra_log_acesso  integer  NOT NULL ,
	id_orgao_usuario     int  NOT NULL ,
	sigla_usuario        varchar(80)  NOT NULL ,
	id_orgao_usuario_emulador int  NULL ,
	sigla_usuario_emulador varchar(80)  NULL ,
	acao                 varchar(100)  NULL ,
	url                  varchar(5000)  NOT NULL ,
	ip                   varchar(20)  NOT NULL ,
	dth_acesso           datetime  NOT NULL ,
	dados                varchar(max) NULL ,
	pid                  bigint  NULL 
)
go



ALTER TABLE infra_log_acesso
	ADD CONSTRAINT XPKinfra_log_acesso PRIMARY KEY (id_infra_log_acesso ASC)
go



CREATE TABLE infra_parametro
( 
	nome                 varchar(500)  NOT NULL ,
	valor                varchar(5000)  NULL 
)
go



ALTER TABLE infra_parametro
	ADD CONSTRAINT XPKinfra_parametro PRIMARY KEY (nome ASC)
go



CREATE TABLE infra_sequencia
( 
	nome_tabela          varchar(35)  NOT NULL ,
	qtd_incremento       bigint  NOT NULL ,
	num_atual            bigint  NOT NULL ,
	num_maximo           bigint  NOT NULL 
)
go



ALTER TABLE infra_sequencia
	ADD CONSTRAINT XPKinfra_sequencia PRIMARY KEY (nome_tabela ASC)
go



CREATE TABLE item_questionario
( 
	id_item_questionario int  NOT NULL ,
	sta_tipo             char(1)  NOT NULL ,
	id_questionario      int  NOT NULL ,
	id_questao           int  NULL ,
	id_tipo_questao      int  NULL ,
	sequencia            int  NOT NULL ,
	sin_numerado         char(1)  NOT NULL ,
	sin_visivel          char(1)  NOT NULL ,
	sin_obrigatorio      char(1)  NOT NULL 
)
go



ALTER TABLE item_questionario
	ADD CONSTRAINT XPKitem_questionario PRIMARY KEY (id_item_questionario ASC)
go



CREATE TABLE laboratorio
( 
	id_laboratorio       integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE laboratorio
	ADD CONSTRAINT XPKlaboratorio PRIMARY KEY (id_laboratorio ASC)
go



CREATE TABLE log_paciente_integracao
( 
	id_log_paciente_integracao integer  NOT NULL ,
	conteudo             varchar(5000)  NOT NULL ,
	dth_atualizacao      datetime  NOT NULL ,
	sin_sucesso          char(1)  NOT NULL ,
	mensagem_erro        text  NULL ,
	id_paciente          integer  NULL ,
	sigla_sistema        varchar(20)  NOT NULL ,
	identificacao_servico varchar(30)  NULL ,
	sta_sentido_integracao char(1)  NOT NULL 
)
go



ALTER TABLE log_paciente_integracao
	ADD CONSTRAINT XPKlog_paciente_integracao PRIMARY KEY (id_log_paciente_integracao ASC)
go



CREATE TABLE log_questionario
( 
	id_log_questionario  int  NOT NULL ,
	dth_log              datetime  NOT NULL ,
	texto_log            text  NOT NULL ,
	ip                   varchar(15)  NOT NULL 
)
go



ALTER TABLE log_questionario
	ADD CONSTRAINT XPKlog_questionario PRIMARY KEY (id_log_questionario ASC)
go



CREATE TABLE medicamento
( 
	id_medicamento       integer  NOT NULL ,
	id_medicamento_nome  integer  NOT NULL ,
	dose                 varchar(50)  NULL ,
	quantidade           varchar(50)  NULL ,
	forma                varchar(50)  NULL ,
	posologia            varchar(100)  NULL ,
	quantidade_receita   varchar(50)  NULL ,
	via_uso              varchar(50)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE medicamento
	ADD CONSTRAINT XPKmedicamento PRIMARY KEY (id_medicamento ASC)
go



CREATE TABLE medicamento_nome
( 
	id_medicamento_nome  integer  NOT NULL ,
	nome_comercial       varchar(100)  NOT NULL ,
	nome_quimico         varchar(200)  NULL ,
	sin_receita_especial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE medicamento_nome
	ADD CONSTRAINT XPKmedicamento_nome PRIMARY KEY (id_medicamento_nome ASC)
go



CREATE TABLE ocupacao
( 
	id_ocupacao          integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE ocupacao
	ADD CONSTRAINT XPKocupacao PRIMARY KEY (id_ocupacao ASC)
go



CREATE TABLE orgao
( 
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	rodape_atestado      varchar(1000)  NULL ,
	rodape_receita       varchar(1000)  NULL ,
	caminho_logo         varchar(300)  NULL ,
	sigla                varchar(10)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE orgao
	ADD CONSTRAINT XPKorgao PRIMARY KEY (id_orgao ASC)
go



CREATE TABLE paciente
( 
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NULL ,
	id_unidade_vinculada integer  NOT NULL ,
	id_paciente_categoria integer  NOT NULL ,
	nome                 varchar(100)  NULL ,
	nome_social          varchar(100)  NULL ,
	telefone             varchar(200)  NULL ,
	email                varchar(100)  NULL ,
	dta_nascimento       datetime  NULL ,
	sexo                 char(1)  NULL ,
	nome_pai             varchar(100)  NULL ,
	nome_mae             varchar(100)  NULL ,
	grau_dependencia     varchar(80)  NULL ,
	cpf                  varchar(14)  NULL ,
	contato              varchar(1000)  NULL ,
	observacao           varchar(1000)  NULL ,
	endereco             varchar(300)  NULL ,
	endereco_cidade      varchar(80)  NULL ,
	endereco_uf          varchar(2)  NULL ,
	endereco_cep         varchar(10)  NULL ,
	cargo                varchar(200)  NULL ,
	lotacao              varchar(200)  NULL ,
	dta_exercicio        datetime  NULL ,
	dta_saida            datetime  NULL ,
	login                varchar(50)  NULL ,
	caminho_foto         varchar(300)  NULL ,
	matricula            varchar(10)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	chave_integracao     varchar(20)  NULL ,
	alerta               varchar(1000)  NULL ,
	medicamento_continuo varchar(1000)  NULL ,
	historico_familiar   varchar(2000)  NULL ,
	odontologia          varchar(2000)  NULL ,
	contato_rh           varchar(1000)  NULL ,
	historico_familiar_psicossocial varchar(2000)  NULL ,
	encaminhamento_psicossocial varchar(2000)  NULL ,
	nota_geral           varchar(2000)  NULL ,
	tipo_sanguineo       varchar(5)  NULL ,
	convenio_medico      varchar(50)  NULL ,
	sta_area_atuacao     char(1)  NULL ,
	sin_vaga_pcd         char(1)  NOT NULL 
)
go



ALTER TABLE paciente
	ADD CONSTRAINT XPKpaciente PRIMARY KEY (id_paciente ASC)
go



CREATE TABLE paciente_categoria
( 
	id_paciente_categoria integer  NOT NULL ,
	nome                 varchar(50)  NULL ,
	sin_exige_cpf        char(1)  NOT NULL ,
	sin_exige_titular    char(1)  NOT NULL ,
	sin_ativo            char(1)  NULL ,
	menu_item_oculto     varchar(500)  NULL 
)
go



ALTER TABLE paciente_categoria
	ADD CONSTRAINT XPKpaciente_categoria PRIMARY KEY (id_paciente_categoria ASC)
go



CREATE TABLE pericia
( 
	id_pericia           int  NOT NULL ,
	dth_pericia          datetime  NULL ,
	id_paciente          integer  NOT NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_cid               integer  NULL ,
	sin_necessita_revisao char(1)  NOT NULL ,
	sta_tipo_pericia     char(1)  NOT NULL ,
	resultado            text  NULL ,
	sta_homologacao      char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_documento_externo integer  NULL ,
	id_profissional_saude_registro integer  NULL ,
	id_orgao             integer  NOT NULL 
)
go



ALTER TABLE pericia
	ADD CONSTRAINT XPKpericia PRIMARY KEY (id_pericia ASC)
go



CREATE TABLE permissao_agenda
( 
	id_permissao_agenda  integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go



ALTER TABLE permissao_agenda
	ADD CONSTRAINT XPKpermissao_agenda PRIMARY KEY (id_permissao_agenda ASC)
go



CREATE TABLE permissao_especialidade
( 
	id_permissao_especialidade integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go



ALTER TABLE permissao_especialidade
	ADD CONSTRAINT XPKpermissao_especialidade PRIMARY KEY (id_permissao_especialidade ASC)
go



CREATE TABLE permissao_tipo_evento
( 
	id_permissao_tipo_evento integer  NOT NULL ,
	id_tipo_evento       integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go



ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT XPKrel_tipo_evento_profissional_saude PRIMARY KEY (id_permissao_tipo_evento ASC)
go



CREATE TABLE procedimento
( 
	id_procedimento      integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	nome                 varchar(200)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE procedimento
	ADD CONSTRAINT XPKtipo_procedimento PRIMARY KEY (id_procedimento ASC)
go



CREATE TABLE profissional_saude
( 
	id_profissional_saude integer  NOT NULL ,
	registro_profissional varchar(100)  NULL ,
	nome_profissional    varchar(80)  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	login_orgao          varchar(50)  NULL ,
	id_agenda            integer  NULL ,
	texto_carimbo        varchar(1000)  NULL ,
	id_especialidade     integer  NOT NULL ,
	sin_consulta_outro_orgao char(1)  NOT NULL ,
	id_ocupacao          integer  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	sin_consulta_outra_agenda char(1)  NOT NULL 
)
go



ALTER TABLE profissional_saude
	ADD CONSTRAINT XPKprofissional_saude PRIMARY KEY (id_profissional_saude ASC)
go



CREATE TABLE profissional_saude_homologacao
( 
	id_profissional_saude_homologacao integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_beneficio         integer  NULL ,
	id_atestado_externo_homologacao integer  NULL ,
	id_pericia           int  NULL ,
	sin_assinatura       char(1)  NOT NULL 
)
go



ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT XPKrel_profissional_saude_beneficio_homologacao PRIMARY KEY (id_profissional_saude_homologacao ASC)
go



CREATE TABLE prontuario_assinatura
( 
	id_prontuario_assinatura integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	data_hora_criacao    datetime  NOT NULL ,
	sin_assinado         char(1)  NOT NULL ,
	data_hora_assinatura datetime  NULL ,
	arquivo              varchar(200)  NULL ,
	chave_assinatura     varchar(2000)  NULL ,
	conteudo_arquivo     text  NULL 
)
go



ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT XPKprontuario_assinatura PRIMARY KEY (id_prontuario_assinatura ASC)
go



CREATE TABLE prontuario_assinatura_item
( 
	id_prontuario_assinatura_item integer  NOT NULL ,
	id_prontuario_assinatura integer  NOT NULL ,
--	nome_tabela          varchar(40)  NOT NULL ,
	tabela          varchar(40)  NOT NULL ,
	id_registro_tabela   integer  NULL ,
	sta_operacao_registro char(1)  NOT NULL ,
	data_hora_gravacao   datetime  NOT NULL ,
	id_infra_auditoria   bigint  NULL ,
	conteudo_registro    text  NOT NULL 
)
go



ALTER TABLE prontuario_assinatura_item
	ADD CONSTRAINT XPKprontuario_assinatura_item PRIMARY KEY (id_prontuario_assinatura_item ASC)
go



CREATE TABLE questao
( 
	id_questao           int  NOT NULL ,
	texto                text  NOT NULL ,
	sin_html             char(1)  NULL 
)
go



ALTER TABLE questao
	ADD CONSTRAINT XPKquestao PRIMARY KEY (id_questao ASC)
go



CREATE TABLE questionario
( 
	id_questionario      int  NOT NULL ,
	titulo               varchar(200)  NOT NULL ,
	dth_abertura         datetime  NULL ,
	dth_fechamento       datetime  NULL ,
	sin_publico          char(1)  NOT NULL ,
	sin_exige_ticket     char(1)  NOT NULL ,
	sin_permite_alteracoes char(1)  NOT NULL ,
	sta_layout           char(1)  NOT NULL 
)
go



ALTER TABLE questionario
	ADD CONSTRAINT XPKquestionario PRIMARY KEY (id_questionario ASC)
go



CREATE TABLE receita
( 
	id_receita           integer  NOT NULL ,
	dth_receita          datetime  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_consulta          integer  NULL ,
	arquivo              varchar(8000)  NULL ,
	id_receita_modelo    integer  NULL ,
	conteudo             text  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	id_especialidade     integer  NOT NULL 
)
go



ALTER TABLE receita
	ADD CONSTRAINT XPKreceita PRIMARY KEY (id_receita ASC)
go



CREATE TABLE receita_modelo
( 
	id_receita_modelo    integer  NOT NULL ,
	titulo               varchar(100)  NOT NULL ,
	texto_modelo         varchar(8000)  NOT NULL ,
	sin_receita_especial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE receita_modelo
	ADD CONSTRAINT XPKreceita_modelo PRIMARY KEY (id_receita_modelo ASC)
go



CREATE TABLE registro_digitalizado
( 
	id_registro_digitalizado integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude_registro integer  NOT NULL ,
	dth_registro         datetime  NOT NULL ,
	titulo               varchar(200)  NULL ,
	dta_data_documento   datetime  NULL ,
	arquivo              varchar(200)  NULL ,
	id_tipo_registro_digitalizado integer  NOT NULL 
)
go



ALTER TABLE registro_digitalizado
	ADD CONSTRAINT XPKregistro_digitalizado PRIMARY KEY (id_registro_digitalizado ASC)
go



CREATE TABLE rel_agenda_tipo_consulta
( 
	id_rel_agenda_tipo_consulta integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_tipo_consulta     integer  NOT NULL ,
	sin_paciente_solicita char(1)  NOT NULL 
)
go



ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT XPKrel_agenda_tipo_consulta PRIMARY KEY (id_rel_agenda_tipo_consulta ASC)
go



CREATE TABLE rel_paciente_cid
( 
	id_rel_paciente_cid  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_cid               integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_consulta          integer  NULL ,
	id_absenteismo       integer  NULL ,
	id_beneficio         integer  NULL ,
	sin_permanente       char(1)  NOT NULL ,
	sin_controlado       char(1)  NOT NULL ,
	observacao           varchar(2000)  NULL ,
	dta_cid              datetime  NULL ,
	sin_periodico        char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	id_pericia           int  NULL 
)
go



ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT XPKrel_consulta_cid PRIMARY KEY (id_rel_paciente_cid ASC)
go



CREATE TABLE rel_paciente_especialidade
( 
	id_rel_paciente_especialidade integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	nota_especialidade   varchar(2000)  NULL 
)
go



ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT XPKrel_paciente_especialidade PRIMARY KEY (id_rel_paciente_especialidade ASC)
go



CREATE TABLE rel_paciente_procedimento
( 
	id_rel_paciente_procedimento integer  NOT NULL ,
	id_procedimento      integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	dth_procedimento     datetime  NOT NULL ,
	id_consulta          integer  NULL ,
	observacao           varchar(2000)  NULL 
)
go



ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT XPKrel_consulta_tipo_procedimento PRIMARY KEY (id_rel_paciente_procedimento ASC)
go



CREATE TABLE rel_paciente_profissional_saude
( 
	id_rel_paciente_profissional_saude integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	nota                 varchar(8000)  NULL 
)
go



ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT XPKrel_paciente_profissional_saude PRIMARY KEY (id_rel_paciente_profissional_saude ASC)
go



CREATE TABLE rel_receita_medicamento
( 
	id_rel_receita_medicamento integer  NOT NULL ,
	id_receita           integer  NOT NULL ,
	id_medicamento       integer  NOT NULL 
)
go



ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT XPKrel_receita_medicamento PRIMARY KEY (id_rel_receita_medicamento ASC)
go



CREATE TABLE resposta_questao
( 
	id_resposta_questao  int  NOT NULL ,
	id_resposta_questionario int  NOT NULL ,
	id_item_questionario int  NOT NULL ,
	valor_resposta       text  NULL ,
	valor_complemento    text  NULL 
)
go



ALTER TABLE resposta_questao
	ADD CONSTRAINT XPKresposta_questao PRIMARY KEY (id_resposta_questao ASC)
go



CREATE TABLE resposta_questao_historico
( 
	id_resposta_questao_historico int  NOT NULL ,
	id_resposta_questao  int  NOT NULL ,
	valor_resposta       text  NULL ,
	valor_complemento    text  NULL ,
	dth_resposta         datetime  NULL 
)
go



ALTER TABLE resposta_questao_historico
	ADD CONSTRAINT XPKresposta_questao_historico PRIMARY KEY (id_resposta_questao_historico ASC)
go



CREATE TABLE resposta_questionario
( 
	id_resposta_questionario int  NOT NULL ,
	id_questionario      int  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	dth_resposta         datetime  NOT NULL 
)
go



ALTER TABLE resposta_questionario
	ADD CONSTRAINT XPKresposta_questionario PRIMARY KEY (id_resposta_questionario ASC)
go



CREATE TABLE texto_generico
( 
	id_texto_generico    integer  NOT NULL ,
	nome                 varchar(200)  NOT NULL ,
	dth_geracao          datetime  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	conteudo             text  NOT NULL ,
	arquivo              varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_especialidade     integer  NOT NULL 
)
go



ALTER TABLE texto_generico
	ADD CONSTRAINT XPKtexto_gerado PRIMARY KEY (id_texto_generico ASC)
go



CREATE TABLE ticket_questionario
( 
	id_ticket_questionario int  NOT NULL ,
	id_questionario      int  NOT NULL ,
	ticket               varchar(100)  NOT NULL ,
	dth_uso              datetime  NOT NULL 
)
go



ALTER TABLE ticket_questionario
	ADD CONSTRAINT XPKticket_questionario PRIMARY KEY (id_ticket_questionario ASC)
go



CREATE TABLE tipo_beneficio
( 
	id_tipo_beneficio    integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	sin_informa_paciente char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE tipo_beneficio
	ADD CONSTRAINT XPKtipo_beneficio PRIMARY KEY (id_tipo_beneficio ASC)
go



CREATE TABLE tipo_bloqueio
( 
	id_tipo_bloqueio     integer  NOT NULL ,
	nome                 varchar(80)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE tipo_bloqueio
	ADD CONSTRAINT XPKtipo_bloqueio PRIMARY KEY (id_tipo_bloqueio ASC)
go



CREATE TABLE tipo_consulta
( 
	id_tipo_consulta     integer  NOT NULL ,
	nome                 varchar(40)  NOT NULL ,
	sta_assistencial_ocupacional char(1)  NOT NULL ,
	sin_periodico        char(1)  NULL ,
	sin_cid_consulta_obrigatorio char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE tipo_consulta
	ADD CONSTRAINT XPKtipo_consulta PRIMARY KEY (id_tipo_consulta ASC)
go



CREATE TABLE tipo_evento
( 
	id_tipo_evento       integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	descricao            varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	id_orgao             integer  NOT NULL 
)
go



ALTER TABLE tipo_evento
	ADD CONSTRAINT XPKtipo_evento PRIMARY KEY (id_tipo_evento ASC)
go



CREATE TABLE tipo_exame
( 
	id_tipo_exame        integer  NOT NULL ,
	nome                 varchar(60)  NOT NULL ,
	sin_arquivo          char(1)  NOT NULL ,
	sin_resultado        char(1)  NOT NULL ,
	sin_horario          char(1)  NOT NULL ,
	sin_valida_edicao    char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE tipo_exame
	ADD CONSTRAINT XPKtipo_exame PRIMARY KEY (id_tipo_exame ASC)
go



CREATE TABLE tipo_exame_resultado
( 
	id_tipo_exame_resultado integer  NOT NULL ,
	id_tipo_exame        integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	nome_tecnico         varchar(50)  NULL ,
	unidade              varchar(10)  NULL ,
	sta_valor_resultado  char(1)  NOT NULL ,
	sin_calculado        char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	ordem                integer  NOT NULL ,
	formula              varchar(80)  NULL ,
	codigo               varchar(20)  NULL 
)
go



ALTER TABLE tipo_exame_resultado
	ADD CONSTRAINT XPKtipo_exame_resultado PRIMARY KEY (id_tipo_exame_resultado ASC)
go



CREATE TABLE tipo_questao
( 
	id_tipo_questao      int  NOT NULL ,
	descricao            varchar(200)  NOT NULL ,
	sta_tipo_resposta    char(2)  NOT NULL ,
	opcoes_resposta      text  NULL ,
	sta_tipo_complemento char(2)  NULL ,
	opcoes_complemento   text  NULL 
)
go



ALTER TABLE tipo_questao
	ADD CONSTRAINT XPKtipo_questao PRIMARY KEY (id_tipo_questao ASC)
go



CREATE TABLE tipo_registro_digitalizado
( 
	id_tipo_registro_digitalizado integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	id_especialidade     integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT XPKtipo_registro_digitalizado PRIMARY KEY (id_tipo_registro_digitalizado ASC)
go



CREATE TABLE unidade_vinculada
( 
	id_unidade_vinculada integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	contato              varchar(1000)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	chave_integracao     varchar(20)  NULL 
)
go



ALTER TABLE unidade_vinculada
	ADD CONSTRAINT XPKunidade_vinculada PRIMARY KEY (id_unidade_vinculada ASC)
go



CREATE TABLE vacina
( 
	id_vacina            integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE vacina
	ADD CONSTRAINT XPKvacina PRIMARY KEY (id_vacina ASC)
go



CREATE TABLE vacina_carteira
( 
	id_vacina_carteira   integer  NOT NULL ,
	dta_aplicacao        datetime  NULL ,
	id_paciente          integer  NOT NULL ,
	id_vacina            integer  NOT NULL ,
	id_vacina_fabricante integer  NULL ,
	id_vacinacao_servico integer  NULL ,
	lote                 varchar(50)  NULL ,
	observacao           varchar(80)  NULL ,
	dose                 integer  NULL ,
	id_profissional_saude integer  NOT NULL 
)
go



ALTER TABLE vacina_carteira
	ADD CONSTRAINT XPKrel_paciente_vacina PRIMARY KEY (id_vacina_carteira ASC)
go



CREATE TABLE vacina_fabricante
( 
	id_vacina_fabricante integer  NOT NULL ,
	id_vacina            integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	qtd_doses            integer  NULL ,
	dias_intervalo_doses integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go



ALTER TABLE vacina_fabricante
	ADD CONSTRAINT XPKvacina_fabricante PRIMARY KEY (id_vacina_fabricante ASC)
go



CREATE TABLE vacinacao_servico
( 
	id_vacinacao_servico integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            varchar(20)  NOT NULL 
)
go



ALTER TABLE vacinacao_servico
	ADD CONSTRAINT XPKservico_vacinacao PRIMARY KEY (id_vacinacao_servico ASC)
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_absenteismo_motivo_absenteismo FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_cid_absenteismo FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_paciente_titular_absenteismo FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_atestado_externo_homologacao_absenteismo FOREIGN KEY (id_atestado_externo_homologacao) REFERENCES atestado_externo_homologacao(id_atestado_externo_homologacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_paciente_absenteismo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_profissional_saude_absenteismo FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_documento_externo_absenteismo FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE absenteismo
	ADD CONSTRAINT fk_grau_parentesco_absenteismo FOREIGN KEY (id_grau_parentesco) REFERENCES grau_parentesco(id_grau_parentesco)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda
	ADD CONSTRAINT fk_orgao_agenda FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda
	ADD CONSTRAINT fk_calendario_feriado_agenda FOREIGN KEY (id_calendario_feriado) REFERENCES calendario_feriado(id_calendario_feriado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_anotacao
	ADD CONSTRAINT fk_agenda_agenda_anotacao FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_bloco_adicional
	ADD CONSTRAINT fk_agenda_agenda_bloco_adicional FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT fk_agenda_horario_agenda_bloqueio FOREIGN KEY (id_agenda_horario) REFERENCES agenda_horario(id_agenda_horario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT fk_tipo_bloqueio_agenda_bloqueio FOREIGN KEY (id_tipo_bloqueio) REFERENCES tipo_bloqueio(id_tipo_bloqueio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_configuracao
	ADD CONSTRAINT fk_historico_agenda_agenda_configuracao FOREIGN KEY (id_historico_agenda) REFERENCES historico_agenda(id_historico_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_agenda_agenda_horario FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_paciente_agenda_horario FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_tipo_consulta_agenda_horario FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_profissional_agenda_agenda_horario FOREIGN KEY (id_profissional_agenda) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_profissional_atendimento_agenda_horario FOREIGN KEY (id_profissional_marcacao) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_cid_atestado_externo FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_paciente_titular_atestado_externo FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_paciente_atestado_externo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_absenteismo_motivo_atestado_externo FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_profissional_saude_atestado_externo FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_grau_parentesco_atestado_externo FOREIGN KEY (id_grau_parentesco) REFERENCES grau_parentesco(id_grau_parentesco)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_orgao_atestado_externo FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_especialidade_atestado_externo FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo_arquivo
	ADD CONSTRAINT fk_atestado_externo_atestado_externo_arquivo FOREIGN KEY (id_atestado_externo) REFERENCES atestado_externo(id_atestado_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE atestado_externo_homologacao
	ADD CONSTRAINT fk_atestado_externo_atestado_externo_homologacao FOREIGN KEY (id_atestado_externo) REFERENCES atestado_externo(id_atestado_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_paciente_beneficio FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_tipo_beneficio_beneficio FOREIGN KEY (id_tipo_beneficio) REFERENCES tipo_beneficio(id_tipo_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_paciente_titular_beneficio FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_beneficio_concessao_beneficio FOREIGN KEY (id_beneficio_concessao) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_cid_beneficio FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_documento_externo_beneficio FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_profissional_saude_beneficio FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE beneficio
	ADD CONSTRAINT fk_orgao_beneficio FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE cid
	ADD CONSTRAINT fk_cid_principal_cid FOREIGN KEY (id_cid_principal) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE consulta
	ADD CONSTRAINT fk_profissional_saude_consulta FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE consulta
	ADD CONSTRAINT fk_paciente_consulta FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE consulta
	ADD CONSTRAINT fk_agenda_horario_consulta FOREIGN KEY (id_agenda_horario) REFERENCES agenda_horario(id_agenda_horario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE consulta
	ADD CONSTRAINT fk_tipo_consulta_consulta FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE consulta
	ADD CONSTRAINT fk_especialidade_consulta FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE evento
	ADD CONSTRAINT fk_orgao_evento FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE evento
	ADD CONSTRAINT fk_tipo_evento_evento FOREIGN KEY (id_tipo_evento) REFERENCES tipo_evento(id_tipo_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT fk_evento_evento_publico_alvo FOREIGN KEY (id_evento) REFERENCES evento(id_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT fk_paciente_evento_publico_alvo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame
	ADD CONSTRAINT fk_laboratorio_exame FOREIGN KEY (id_laboratorio) REFERENCES laboratorio(id_laboratorio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame
	ADD CONSTRAINT fk_paciente_exame FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame
	ADD CONSTRAINT fk_tipo_exame_exame FOREIGN KEY (id_tipo_exame) REFERENCES tipo_exame(id_tipo_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame
	ADD CONSTRAINT fk_profissional_saude_exame FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame_resultado
	ADD CONSTRAINT fk_exame_exame_resultado FOREIGN KEY (id_exame) REFERENCES exame(id_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE exame_resultado
	ADD CONSTRAINT fk_tipo_exame_resultado_exame_resultado FOREIGN KEY (id_tipo_exame_resultado) REFERENCES tipo_exame_resultado(id_tipo_exame_resultado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE historico_agenda
	ADD CONSTRAINT fk_agenda_historico_agenda FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE item_questionario
	ADD CONSTRAINT fk_questionario_item_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE item_questionario
	ADD CONSTRAINT fk_questao_item_questionario FOREIGN KEY (id_questao) REFERENCES questao(id_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE item_questionario
	ADD CONSTRAINT fk_tipo_questao_item_questionario FOREIGN KEY (id_tipo_questao) REFERENCES tipo_questao(id_tipo_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE laboratorio
	ADD CONSTRAINT fk_orgao_laboratorio FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE log_paciente_integracao
	ADD CONSTRAINT fk_paciente_log_paciente_integracao FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE medicamento
	ADD CONSTRAINT fk_medicamento_nome_medicamento FOREIGN KEY (id_medicamento_nome) REFERENCES medicamento_nome(id_medicamento_nome)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE paciente
	ADD CONSTRAINT fk_paciente_titular_paciente FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE paciente
	ADD CONSTRAINT fk_paciente_categoria_paciente FOREIGN KEY (id_paciente_categoria) REFERENCES paciente_categoria(id_paciente_categoria)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE paciente
	ADD CONSTRAINT fk_unidade_vinculada_paciente FOREIGN KEY (id_unidade_vinculada) REFERENCES unidade_vinculada(id_unidade_vinculada)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_paciente_pericia FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_absenteismo_motivo_pericia FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_documento_externo_pericia FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_especialidade_pericia FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_profissional_saude_pericia FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_orgao_pericia FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE pericia
	ADD CONSTRAINT fk_cid_pericia FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_agenda
	ADD CONSTRAINT fk_agenda_permissao_agenda FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_agenda
	ADD CONSTRAINT fk_profissional_saude_permissao_agenda FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_especialidade
	ADD CONSTRAINT fk_profissional_saude_permissao_especialidade FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_especialidade
	ADD CONSTRAINT fk_especialidade_permissao_especialidade FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT fk_tipo_evento_permissao_tipo_evento FOREIGN KEY (id_tipo_evento) REFERENCES tipo_evento(id_tipo_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT fk_profissional_saude_permissao_tipo_evento FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE procedimento
	ADD CONSTRAINT fk_especialidade_tipo_procedimento FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_paciente_profissional_saude FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_orgao_profissional_saude FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_agenda_profissional_saude FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_especialidade_profissional_saude FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_ocupacao_profissional_saude FOREIGN KEY (id_ocupacao) REFERENCES ocupacao(id_ocupacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_profissional_saude_profissional_saude_homologacao FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_beneficio_profissional_saude_homologacao FOREIGN KEY (id_beneficio) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_pericia_profissional_saude_homologacao FOREIGN KEY (id_pericia) REFERENCES pericia(id_pericia)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_atestado_externo_homologacao_profissional_saude_homologacao FOREIGN KEY (id_atestado_externo_homologacao) REFERENCES atestado_externo_homologacao(id_atestado_externo_homologacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT fk_paciente_prontuario_assinatura FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT fk_profissional_saude_prontuario_assinatura FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE prontuario_assinatura_item
	ADD CONSTRAINT fk_prontuario_assinatura_prontuario_assinatura_item FOREIGN KEY (id_prontuario_assinatura) REFERENCES prontuario_assinatura(id_prontuario_assinatura)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE receita
	ADD CONSTRAINT fk_profissional_saude_receita FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE receita
	ADD CONSTRAINT fk_paciente_receita FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE receita
	ADD CONSTRAINT fk_consulta_receita FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE receita
	ADD CONSTRAINT fk_receita_modelo_receita FOREIGN KEY (id_receita_modelo) REFERENCES receita_modelo(id_receita_modelo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE receita
	ADD CONSTRAINT fk_especialidade_receita FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_profissional_saude_registro_digitalizado FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_paciente_registro_digitalizado FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_tipo_registro_digitalizado_registro_digitalizado FOREIGN KEY (id_tipo_registro_digitalizado) REFERENCES tipo_registro_digitalizado(id_tipo_registro_digitalizado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT fk_agenda_rel_agenda_tipo_consulta FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT fk_tipo_consulta_rel_agenda_tipo_consulta FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_consulta_rel_paciente_cid FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_cid_rel_paciente_cid FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_paciente_rel_paciente_cid FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_cid FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_beneficio_rel_paciente_cid FOREIGN KEY (id_beneficio) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_absenteismo_rel_paciente_cid FOREIGN KEY (id_absenteismo) REFERENCES absenteismo(id_absenteismo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_pericia_rel_paciente_cid FOREIGN KEY (id_pericia) REFERENCES pericia(id_pericia)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT fk_especialidade_rel_paciente_especialidade FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT fk_paciente_rel_paciente_especialidade FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_procedimento_rel_paciente_procedimento FOREIGN KEY (id_procedimento) REFERENCES procedimento(id_procedimento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_consulta_rel_paciente_procedimento FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_paciente_rel_paciente_procedimento FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_procedimento FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_profissional_saude FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT fk_paciente_rel_paciente_profissional_saude FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT fk_receita_rel_receita_medicamento FOREIGN KEY (id_receita) REFERENCES receita(id_receita)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT fk_medicamento_rel_receita_medicamento FOREIGN KEY (id_medicamento) REFERENCES medicamento(id_medicamento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE resposta_questao
	ADD CONSTRAINT fk_resposta_questionario_resposta_questao FOREIGN KEY (id_resposta_questionario) REFERENCES resposta_questionario(id_resposta_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE resposta_questao
	ADD CONSTRAINT fk_item_questionario_resposta_questao FOREIGN KEY (id_item_questionario) REFERENCES item_questionario(id_item_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE resposta_questao_historico
	ADD CONSTRAINT fk_resposta_questao_resposta_questao_historico FOREIGN KEY (id_resposta_questao) REFERENCES resposta_questao(id_resposta_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE resposta_questionario
	ADD CONSTRAINT fk_questionario_resposta_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE resposta_questionario
	ADD CONSTRAINT fk_paciente_resposta_questionario FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE texto_generico
	ADD CONSTRAINT fk_paciente_texto_generico FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE texto_generico
	ADD CONSTRAINT fk_profissional_saude_texto_generico FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE texto_generico
	ADD CONSTRAINT fk_especialidade_texto_generico FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ticket_questionario
	ADD CONSTRAINT fk_questionario_ticket_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE tipo_evento
	ADD CONSTRAINT fk_orgao_tipo_evento FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE tipo_exame_resultado
	ADD CONSTRAINT fk_tipo_exame_tipo_exame_resultado FOREIGN KEY (id_tipo_exame) REFERENCES tipo_exame(id_tipo_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT fk_especialidade_tipo_registro_digitalizado FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT fk_orgao_tipo_registro_digitalizado FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE unidade_vinculada
	ADD CONSTRAINT fk_orgao_unidade_vinculada FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_paciente_vacina_carteira FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacina_vacina_carteira FOREIGN KEY (id_vacina) REFERENCES vacina(id_vacina)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacina_fabricante_vacina_carteira FOREIGN KEY (id_vacina_fabricante) REFERENCES vacina_fabricante(id_vacina_fabricante)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacinacao_servico_vacina_carteira FOREIGN KEY (id_vacinacao_servico) REFERENCES vacinacao_servico(id_vacinacao_servico)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_profissional_saude_vacina_carteira FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE vacina_fabricante
	ADD CONSTRAINT fk_vacina_vacina_fabricante FOREIGN KEY (id_vacina) REFERENCES vacina(id_vacina)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go