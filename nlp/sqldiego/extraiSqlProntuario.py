# coding=utf-8

import re
import os
from os import listdir
from os.path import isfile, join, splitext

# Le arquivos
strSep = os.path.sep
strPathDir = "/home/ronie/prog/alura/nlp/sqldiego"
strSqlFilename = strPathDir  + strSep + "01_saudejus_bd_erwin.sql"

with open(strSqlFilename) as filePS:
    srcSQL = filePS.read()   

# Cria Estruturas de Dados    
dictCreate = {}
dictAlter = {}

strToFind = 'CREATE TABLE'
a = re.finditer('{}.*'.format(strToFind),srcSQL)
for m in a:
    strMatch = m.group(0)
    strTabela = srcSQL[m.start(0) + len(strToFind):m.end(0)].strip()
    dictCreate[strTabela] = [m]
    dictAlter[strTabela] = []

strToFind = 'ALTER TABLE'
a = re.finditer('{}.*'.format(strToFind),srcSQL)
for m in a:
    strMatch = m.group(0)
    strTabela = srcSQL[m.start(0) + len(strToFind):m.end(0)].strip()
    dictAlter[strTabela].append(m)


# Gera Arquivos
for strTabela in dictCreate:
    # Cria Arquivo Para a strTabela
    strFile = ''
    strOutputFullFilename = strPathDir + strSep + 'DATA' + strSep + '{}.sql'.format(strTabela)
    with open(strOutputFullFilename, 'w') as fileOutput:
        fileOutput.write(strFile)  
    
    # Adiciona ao arquivo o Create
    mCreate = dictCreate[strTabela][0]
    posStart = mCreate.start(0)
    posEnd = srcSQL.find('go\n',mCreate.end(0))
    strToFile = srcSQL[posStart:posEnd+2]
    strToFile = strToFile + '\n\n'
    
    strOutputFullFilename = strPathDir + strSep + 'DATA' + strSep + '{}.sql'.format(strTabela)
    with open(strOutputFullFilename, 'a') as fileOutput:
        fileOutput.write(strToFile)  
        
for strTabela in dictAlter:
    # Arquivo já existe
    # Adiciona ao arquivo os Alters
    for mAlter in dictAlter[strTabela]:
        posStart = mAlter.start(0)
        posEnd = srcSQL.find('go\n',mAlter.end(0))
        strToFile = srcSQL[posStart:posEnd+2]
        strToFile = strToFile + '\n\n'
    
        strOutputFullFilename = strPathDir + strSep + 'DATA' + strSep + '{}.sql'.format(strTabela)
        with open(strOutputFullFilename, 'a') as fileOutput:
            fileOutput.write(strToFile)  
   
