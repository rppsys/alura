CREATE TABLE paciente
( 
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NULL ,
	id_unidade_vinculada integer  NOT NULL ,
	id_paciente_categoria integer  NOT NULL ,
	nome                 varchar(100)  NULL ,
	nome_social          varchar(100)  NULL ,
	telefone             varchar(200)  NULL ,
	email                varchar(100)  NULL ,
	dta_nascimento       datetime  NULL ,
	sexo                 char(1)  NULL ,
	nome_pai             varchar(100)  NULL ,
	nome_mae             varchar(100)  NULL ,
	grau_dependencia     varchar(80)  NULL ,
	cpf                  varchar(14)  NULL ,
	contato              varchar(1000)  NULL ,
	observacao           varchar(1000)  NULL ,
	endereco             varchar(300)  NULL ,
	endereco_cidade      varchar(80)  NULL ,
	endereco_uf          varchar(2)  NULL ,
	endereco_cep         varchar(10)  NULL ,
	cargo                varchar(200)  NULL ,
	lotacao              varchar(200)  NULL ,
	dta_exercicio        datetime  NULL ,
	dta_saida            datetime  NULL ,
	login                varchar(50)  NULL ,
	caminho_foto         varchar(300)  NULL ,
	matricula            varchar(10)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	chave_integracao     varchar(20)  NULL ,
	alerta               varchar(1000)  NULL ,
	medicamento_continuo varchar(1000)  NULL ,
	historico_familiar   varchar(2000)  NULL ,
	odontologia          varchar(2000)  NULL ,
	contato_rh           varchar(1000)  NULL ,
	historico_familiar_psicossocial varchar(2000)  NULL ,
	encaminhamento_psicossocial varchar(2000)  NULL ,
	nota_geral           varchar(2000)  NULL ,
	tipo_sanguineo       varchar(5)  NULL ,
	convenio_medico      varchar(50)  NULL ,
	sta_area_atuacao     char(1)  NULL ,
	sin_vaga_pcd         char(1)  NOT NULL 
)
go

ALTER TABLE paciente
	ADD CONSTRAINT XPKpaciente PRIMARY KEY (id_paciente ASC)
go

ALTER TABLE paciente
	ADD CONSTRAINT fk_paciente_titular_paciente FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE paciente
	ADD CONSTRAINT fk_paciente_categoria_paciente FOREIGN KEY (id_paciente_categoria) REFERENCES paciente_categoria(id_paciente_categoria)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE paciente
	ADD CONSTRAINT fk_unidade_vinculada_paciente FOREIGN KEY (id_unidade_vinculada) REFERENCES unidade_vinculada(id_unidade_vinculada)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

