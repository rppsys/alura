CREATE TABLE atestado_externo_arquivo
( 
	id_atestado_externo_arquivo integer  NOT NULL ,
	id_atestado_externo  integer  NOT NULL ,
	arquivo              varchar(200)  NOT NULL ,
	nome                 varchar(100)  NOT NULL 
)
go

ALTER TABLE atestado_externo_arquivo
	ADD CONSTRAINT XPKatestado_externo_arquivo PRIMARY KEY (id_atestado_externo_arquivo ASC)
go

ALTER TABLE atestado_externo_arquivo
	ADD CONSTRAINT fk_atestado_externo_atestado_externo_arquivo FOREIGN KEY (id_atestado_externo) REFERENCES atestado_externo(id_atestado_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

