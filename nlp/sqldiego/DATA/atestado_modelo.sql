CREATE TABLE atestado_modelo
( 
	id_atestado_modelo   integer  NOT NULL ,
	titulo               varchar(100)  NOT NULL ,
	texto_modelo         varchar(8000)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE atestado_modelo
	ADD CONSTRAINT XPKatestado_modelo PRIMARY KEY (id_atestado_modelo ASC)
go

