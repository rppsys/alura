CREATE TABLE permissao_tipo_evento
( 
	id_permissao_tipo_evento integer  NOT NULL ,
	id_tipo_evento       integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go

ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT XPKrel_tipo_evento_profissional_saude PRIMARY KEY (id_permissao_tipo_evento ASC)
go

ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT fk_tipo_evento_permissao_tipo_evento FOREIGN KEY (id_tipo_evento) REFERENCES tipo_evento(id_tipo_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE permissao_tipo_evento
	ADD CONSTRAINT fk_profissional_saude_permissao_tipo_evento FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

