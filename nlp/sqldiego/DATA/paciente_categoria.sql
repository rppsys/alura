CREATE TABLE paciente_categoria
( 
	id_paciente_categoria integer  NOT NULL ,
	nome                 varchar(50)  NULL ,
	sin_exige_cpf        char(1)  NOT NULL ,
	sin_exige_titular    char(1)  NOT NULL ,
	sin_ativo            char(1)  NULL ,
	menu_item_oculto     varchar(500)  NULL 
)
go

ALTER TABLE paciente_categoria
	ADD CONSTRAINT XPKpaciente_categoria PRIMARY KEY (id_paciente_categoria ASC)
go

