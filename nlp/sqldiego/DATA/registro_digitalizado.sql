CREATE TABLE registro_digitalizado
( 
	id_registro_digitalizado integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude_registro integer  NOT NULL ,
	dth_registro         datetime  NOT NULL ,
	titulo               varchar(200)  NULL ,
	dta_data_documento   datetime  NULL ,
	arquivo              varchar(200)  NULL ,
	id_tipo_registro_digitalizado integer  NOT NULL 
)
go

ALTER TABLE registro_digitalizado
	ADD CONSTRAINT XPKregistro_digitalizado PRIMARY KEY (id_registro_digitalizado ASC)
go

ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_profissional_saude_registro_digitalizado FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_paciente_registro_digitalizado FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE registro_digitalizado
	ADD CONSTRAINT fk_tipo_registro_digitalizado_registro_digitalizado FOREIGN KEY (id_tipo_registro_digitalizado) REFERENCES tipo_registro_digitalizado(id_tipo_registro_digitalizado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

