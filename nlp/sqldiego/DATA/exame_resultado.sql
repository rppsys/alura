CREATE TABLE exame_resultado
( 
	id_exame_resultado   integer  NOT NULL ,
	txt_resultado        varchar(100)  NULL ,
	id_exame             bigint  NOT NULL ,
	id_tipo_exame_resultado integer  NOT NULL 
)
go

ALTER TABLE exame_resultado
	ADD CONSTRAINT XPKexame_resultado PRIMARY KEY (id_exame_resultado ASC)
go

ALTER TABLE exame_resultado
	ADD CONSTRAINT fk_exame_exame_resultado FOREIGN KEY (id_exame) REFERENCES exame(id_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE exame_resultado
	ADD CONSTRAINT fk_tipo_exame_resultado_exame_resultado FOREIGN KEY (id_tipo_exame_resultado) REFERENCES tipo_exame_resultado(id_tipo_exame_resultado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

