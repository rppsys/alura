CREATE TABLE medicamento
( 
	id_medicamento       integer  NOT NULL ,
	id_medicamento_nome  integer  NOT NULL ,
	dose                 varchar(50)  NULL ,
	quantidade           varchar(50)  NULL ,
	forma                varchar(50)  NULL ,
	posologia            varchar(100)  NULL ,
	quantidade_receita   varchar(50)  NULL ,
	via_uso              varchar(50)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE medicamento
	ADD CONSTRAINT XPKmedicamento PRIMARY KEY (id_medicamento ASC)
go

ALTER TABLE medicamento
	ADD CONSTRAINT fk_medicamento_nome_medicamento FOREIGN KEY (id_medicamento_nome) REFERENCES medicamento_nome(id_medicamento_nome)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

