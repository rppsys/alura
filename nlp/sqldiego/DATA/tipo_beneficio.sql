CREATE TABLE tipo_beneficio
( 
	id_tipo_beneficio    integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	sin_informa_paciente char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE tipo_beneficio
	ADD CONSTRAINT XPKtipo_beneficio PRIMARY KEY (id_tipo_beneficio ASC)
go

