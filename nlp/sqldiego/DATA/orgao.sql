CREATE TABLE orgao
( 
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	rodape_atestado      varchar(1000)  NULL ,
	rodape_receita       varchar(1000)  NULL ,
	caminho_logo         varchar(300)  NULL ,
	sigla                varchar(10)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE orgao
	ADD CONSTRAINT XPKorgao PRIMARY KEY (id_orgao ASC)
go

