CREATE TABLE consulta
( 
	id_consulta          integer  NOT NULL ,
	id_tipo_consulta     integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	dth_consulta         datetime  NOT NULL ,
	evolucao             text  NULL ,
	id_agenda_horario    integer  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL 
)
go

ALTER TABLE consulta
	ADD CONSTRAINT XPKconsulta PRIMARY KEY (id_consulta ASC)
go

ALTER TABLE consulta
	ADD CONSTRAINT fk_profissional_saude_consulta FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE consulta
	ADD CONSTRAINT fk_paciente_consulta FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE consulta
	ADD CONSTRAINT fk_agenda_horario_consulta FOREIGN KEY (id_agenda_horario) REFERENCES agenda_horario(id_agenda_horario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE consulta
	ADD CONSTRAINT fk_tipo_consulta_consulta FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE consulta
	ADD CONSTRAINT fk_especialidade_consulta FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

