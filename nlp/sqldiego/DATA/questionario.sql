CREATE TABLE questionario
( 
	id_questionario      int  NOT NULL ,
	titulo               varchar(200)  NOT NULL ,
	dth_abertura         datetime  NULL ,
	dth_fechamento       datetime  NULL ,
	sin_publico          char(1)  NOT NULL ,
	sin_exige_ticket     char(1)  NOT NULL ,
	sin_permite_alteracoes char(1)  NOT NULL ,
	sta_layout           char(1)  NOT NULL 
)
go

ALTER TABLE questionario
	ADD CONSTRAINT XPKquestionario PRIMARY KEY (id_questionario ASC)
go

