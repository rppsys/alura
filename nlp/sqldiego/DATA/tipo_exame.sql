CREATE TABLE tipo_exame
( 
	id_tipo_exame        integer  NOT NULL ,
	nome                 varchar(60)  NOT NULL ,
	sin_arquivo          char(1)  NOT NULL ,
	sin_resultado        char(1)  NOT NULL ,
	sin_horario          char(1)  NOT NULL ,
	sin_valida_edicao    char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE tipo_exame
	ADD CONSTRAINT XPKtipo_exame PRIMARY KEY (id_tipo_exame ASC)
go

