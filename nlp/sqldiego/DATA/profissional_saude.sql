CREATE TABLE profissional_saude
( 
	id_profissional_saude integer  NOT NULL ,
	registro_profissional varchar(100)  NULL ,
	nome_profissional    varchar(80)  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	login_orgao          varchar(50)  NULL ,
	id_agenda            integer  NULL ,
	texto_carimbo        varchar(1000)  NULL ,
	id_especialidade     integer  NOT NULL ,
	sin_consulta_outro_orgao char(1)  NOT NULL ,
	id_ocupacao          integer  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	sin_consulta_outra_agenda char(1)  NOT NULL 
)
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT XPKprofissional_saude PRIMARY KEY (id_profissional_saude ASC)
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_paciente_profissional_saude FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_orgao_profissional_saude FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_agenda_profissional_saude FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_especialidade_profissional_saude FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude
	ADD CONSTRAINT fk_ocupacao_profissional_saude FOREIGN KEY (id_ocupacao) REFERENCES ocupacao(id_ocupacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

