CREATE TABLE prontuario_assinatura_item
( 
	id_prontuario_assinatura_item integer  NOT NULL ,
	id_prontuario_assinatura integer  NOT NULL ,
--	nome_tabela          varchar(40)  NOT NULL ,
	tabela          varchar(40)  NOT NULL ,
	id_registro_tabela   integer  NULL ,
	sta_operacao_registro char(1)  NOT NULL ,
	data_hora_gravacao   datetime  NOT NULL ,
	id_infra_auditoria   bigint  NULL ,
	conteudo_registro    text  NOT NULL 
)
go

ALTER TABLE prontuario_assinatura_item
	ADD CONSTRAINT XPKprontuario_assinatura_item PRIMARY KEY (id_prontuario_assinatura_item ASC)
go

ALTER TABLE prontuario_assinatura_item
	ADD CONSTRAINT fk_prontuario_assinatura_prontuario_assinatura_item FOREIGN KEY (id_prontuario_assinatura) REFERENCES prontuario_assinatura(id_prontuario_assinatura)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

