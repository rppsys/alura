CREATE TABLE agenda
( 
	id_agenda            integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	descricao            varchar(200)  NULL ,
--	dta_inicio           datetime  NOT NULL ,
--	dta_fim              datetime  NOT NULL ,
	sin_paciente_solicita char(1)  NOT NULL ,
	id_calendario_feriado integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE agenda
	ADD CONSTRAINT XPKagenda PRIMARY KEY (id_agenda ASC)
go

ALTER TABLE agenda
	ADD CONSTRAINT fk_orgao_agenda FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda
	ADD CONSTRAINT fk_calendario_feriado_agenda FOREIGN KEY (id_calendario_feriado) REFERENCES calendario_feriado(id_calendario_feriado)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

