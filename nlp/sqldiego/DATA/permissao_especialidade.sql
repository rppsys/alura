CREATE TABLE permissao_especialidade
( 
	id_permissao_especialidade integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go

ALTER TABLE permissao_especialidade
	ADD CONSTRAINT XPKpermissao_especialidade PRIMARY KEY (id_permissao_especialidade ASC)
go

ALTER TABLE permissao_especialidade
	ADD CONSTRAINT fk_profissional_saude_permissao_especialidade FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE permissao_especialidade
	ADD CONSTRAINT fk_especialidade_permissao_especialidade FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

