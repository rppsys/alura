CREATE TABLE infra_sequencia
( 
	nome_tabela          varchar(35)  NOT NULL ,
	qtd_incremento       bigint  NOT NULL ,
	num_atual            bigint  NOT NULL ,
	num_maximo           bigint  NOT NULL 
)
go

ALTER TABLE infra_sequencia
	ADD CONSTRAINT XPKinfra_sequencia PRIMARY KEY (nome_tabela ASC)
go

