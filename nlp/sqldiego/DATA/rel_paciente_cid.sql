CREATE TABLE rel_paciente_cid
( 
	id_rel_paciente_cid  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_cid               integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_consulta          integer  NULL ,
	id_absenteismo       integer  NULL ,
	id_beneficio         integer  NULL ,
	sin_permanente       char(1)  NOT NULL ,
	sin_controlado       char(1)  NOT NULL ,
	observacao           varchar(2000)  NULL ,
	dta_cid              datetime  NULL ,
	sin_periodico        char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	id_pericia           int  NULL 
)
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT XPKrel_consulta_cid PRIMARY KEY (id_rel_paciente_cid ASC)
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_consulta_rel_paciente_cid FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_cid_rel_paciente_cid FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_paciente_rel_paciente_cid FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_cid FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_beneficio_rel_paciente_cid FOREIGN KEY (id_beneficio) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_absenteismo_rel_paciente_cid FOREIGN KEY (id_absenteismo) REFERENCES absenteismo(id_absenteismo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_cid
	ADD CONSTRAINT fk_pericia_rel_paciente_cid FOREIGN KEY (id_pericia) REFERENCES pericia(id_pericia)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

