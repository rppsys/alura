CREATE TABLE rel_paciente_profissional_saude
( 
	id_rel_paciente_profissional_saude integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	nota                 varchar(8000)  NULL 
)
go

ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT XPKrel_paciente_profissional_saude PRIMARY KEY (id_rel_paciente_profissional_saude ASC)
go

ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_profissional_saude FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_profissional_saude
	ADD CONSTRAINT fk_paciente_rel_paciente_profissional_saude FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

