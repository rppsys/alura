CREATE TABLE rel_paciente_procedimento
( 
	id_rel_paciente_procedimento integer  NOT NULL ,
	id_procedimento      integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	dth_procedimento     datetime  NOT NULL ,
	id_consulta          integer  NULL ,
	observacao           varchar(2000)  NULL 
)
go

ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT XPKrel_consulta_tipo_procedimento PRIMARY KEY (id_rel_paciente_procedimento ASC)
go

ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_procedimento_rel_paciente_procedimento FOREIGN KEY (id_procedimento) REFERENCES procedimento(id_procedimento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_consulta_rel_paciente_procedimento FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_paciente_rel_paciente_procedimento FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_procedimento
	ADD CONSTRAINT fk_profissional_saude_rel_paciente_procedimento FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

