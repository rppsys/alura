CREATE TABLE cid
( 
	id_cid               integer  NOT NULL ,
	id_cid_principal     integer  NULL ,
	codigo               varchar(20)  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	apelido              varchar(100)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE cid
	ADD CONSTRAINT XPKcid PRIMARY KEY (id_cid ASC)
go

ALTER TABLE cid
	ADD CONSTRAINT fk_cid_principal_cid FOREIGN KEY (id_cid_principal) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

