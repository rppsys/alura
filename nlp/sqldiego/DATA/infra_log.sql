CREATE TABLE infra_log
( 
	id_infra_log         integer  NOT NULL ,
	dth_log              datetime  NULL ,
	texto_log            varchar(max)  NOT NULL ,
	ip                   varchar(15)  NULL ,
	sta_tipo             char(1)  NOT NULL 
)
go

ALTER TABLE infra_log
	ADD CONSTRAINT XPKinfra_log PRIMARY KEY (id_infra_log ASC)
go

