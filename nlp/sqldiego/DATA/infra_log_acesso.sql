CREATE TABLE infra_log_acesso
( 
	id_infra_log_acesso  integer  NOT NULL ,
	id_orgao_usuario     int  NOT NULL ,
	sigla_usuario        varchar(80)  NOT NULL ,
	id_orgao_usuario_emulador int  NULL ,
	sigla_usuario_emulador varchar(80)  NULL ,
	acao                 varchar(100)  NULL ,
	url                  varchar(5000)  NOT NULL ,
	ip                   varchar(20)  NOT NULL ,
	dth_acesso           datetime  NOT NULL ,
	dados                varchar(max) NULL ,
	pid                  bigint  NULL 
)
go

ALTER TABLE infra_log_acesso
	ADD CONSTRAINT XPKinfra_log_acesso PRIMARY KEY (id_infra_log_acesso ASC)
go

