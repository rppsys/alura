CREATE TABLE historico_agenda
( 
	id_historico_agenda  integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	descricao            varchar(200)  NULL ,
	texto_agenda         varchar(2000)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE historico_agenda
	ADD CONSTRAINT XPKhistorico_agenda_configuracao PRIMARY KEY (id_historico_agenda ASC)
go

ALTER TABLE historico_agenda
	ADD CONSTRAINT fk_agenda_historico_agenda FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

