CREATE TABLE log_paciente_integracao
( 
	id_log_paciente_integracao integer  NOT NULL ,
	conteudo             varchar(5000)  NOT NULL ,
	dth_atualizacao      datetime  NOT NULL ,
	sin_sucesso          char(1)  NOT NULL ,
	mensagem_erro        text  NULL ,
	id_paciente          integer  NULL ,
	sigla_sistema        varchar(20)  NOT NULL ,
	identificacao_servico varchar(30)  NULL ,
	sta_sentido_integracao char(1)  NOT NULL 
)
go

ALTER TABLE log_paciente_integracao
	ADD CONSTRAINT XPKlog_paciente_integracao PRIMARY KEY (id_log_paciente_integracao ASC)
go

ALTER TABLE log_paciente_integracao
	ADD CONSTRAINT fk_paciente_log_paciente_integracao FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

