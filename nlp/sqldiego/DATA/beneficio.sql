CREATE TABLE beneficio
( 
	id_beneficio         integer  NOT NULL ,
	id_beneficio_concessao integer  NULL ,
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	id_tipo_beneficio    integer  NOT NULL ,
	dta_pedido           datetime  NOT NULL ,
	processo             varchar(200)  NULL ,
	sta_motivo           char(1)  NOT NULL ,
	descricao            text  NULL ,
	dta_pericia          datetime  NULL ,
	resultado            text  NULL ,
	sta_resultado        char(1)  NULL ,
	id_cid               integer  NULL ,
	id_documento_externo integer  NULL ,
	anotacao             text  NULL ,
	arquivo_resultado    varchar(200)  NULL ,
	id_profissional_saude_registro integer  NULL ,
	id_orgao             integer  NOT NULL 
)
go

ALTER TABLE beneficio
	ADD CONSTRAINT XPKbeneficio PRIMARY KEY (id_beneficio ASC)
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_paciente_beneficio FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_tipo_beneficio_beneficio FOREIGN KEY (id_tipo_beneficio) REFERENCES tipo_beneficio(id_tipo_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_paciente_titular_beneficio FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_beneficio_concessao_beneficio FOREIGN KEY (id_beneficio_concessao) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_cid_beneficio FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_documento_externo_beneficio FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_profissional_saude_beneficio FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE beneficio
	ADD CONSTRAINT fk_orgao_beneficio FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

