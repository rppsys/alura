CREATE TABLE prontuario_assinatura
( 
	id_prontuario_assinatura integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	data_hora_criacao    datetime  NOT NULL ,
	sin_assinado         char(1)  NOT NULL ,
	data_hora_assinatura datetime  NULL ,
	arquivo              varchar(200)  NULL ,
	chave_assinatura     varchar(2000)  NULL ,
	conteudo_arquivo     text  NULL 
)
go

ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT XPKprontuario_assinatura PRIMARY KEY (id_prontuario_assinatura ASC)
go

ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT fk_paciente_prontuario_assinatura FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE prontuario_assinatura
	ADD CONSTRAINT fk_profissional_saude_prontuario_assinatura FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

