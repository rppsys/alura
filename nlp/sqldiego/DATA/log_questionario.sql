CREATE TABLE log_questionario
( 
	id_log_questionario  int  NOT NULL ,
	dth_log              datetime  NOT NULL ,
	texto_log            text  NOT NULL ,
	ip                   varchar(15)  NOT NULL 
)
go

ALTER TABLE log_questionario
	ADD CONSTRAINT XPKlog_questionario PRIMARY KEY (id_log_questionario ASC)
go

