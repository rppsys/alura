CREATE TABLE pericia
( 
	id_pericia           int  NOT NULL ,
	dth_pericia          datetime  NULL ,
	id_paciente          integer  NOT NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_cid               integer  NULL ,
	sin_necessita_revisao char(1)  NOT NULL ,
	sta_tipo_pericia     char(1)  NOT NULL ,
	resultado            text  NULL ,
	sta_homologacao      char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_documento_externo integer  NULL ,
	id_profissional_saude_registro integer  NULL ,
	id_orgao             integer  NOT NULL 
)
go

ALTER TABLE pericia
	ADD CONSTRAINT XPKpericia PRIMARY KEY (id_pericia ASC)
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_paciente_pericia FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_absenteismo_motivo_pericia FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_documento_externo_pericia FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_especialidade_pericia FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_profissional_saude_pericia FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_orgao_pericia FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE pericia
	ADD CONSTRAINT fk_cid_pericia FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

