CREATE TABLE agenda_configuracao
( 
	id_agenda_configuracao integer  NOT NULL ,
	id_historico_agenda  integer  NOT NULL ,
	dia_semana           integer  NULL ,
	hora_inicio          integer  NULL ,
	hora_fim             integer  NULL ,
	duracao              integer  NULL ,
	sin_paciente_solicita char(1)  NOT NULL 
)
go

ALTER TABLE agenda_configuracao
	ADD CONSTRAINT XPKagenda_configuracao PRIMARY KEY (id_agenda_configuracao ASC)
go

ALTER TABLE agenda_configuracao
	ADD CONSTRAINT fk_historico_agenda_agenda_configuracao FOREIGN KEY (id_historico_agenda) REFERENCES historico_agenda(id_historico_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

