CREATE TABLE receita_modelo
( 
	id_receita_modelo    integer  NOT NULL ,
	titulo               varchar(100)  NOT NULL ,
	texto_modelo         varchar(8000)  NOT NULL ,
	sin_receita_especial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE receita_modelo
	ADD CONSTRAINT XPKreceita_modelo PRIMARY KEY (id_receita_modelo ASC)
go

