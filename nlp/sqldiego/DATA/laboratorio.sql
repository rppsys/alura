CREATE TABLE laboratorio
( 
	id_laboratorio       integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE laboratorio
	ADD CONSTRAINT XPKlaboratorio PRIMARY KEY (id_laboratorio ASC)
go

ALTER TABLE laboratorio
	ADD CONSTRAINT fk_orgao_laboratorio FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

