CREATE TABLE rel_receita_medicamento
( 
	id_rel_receita_medicamento integer  NOT NULL ,
	id_receita           integer  NOT NULL ,
	id_medicamento       integer  NOT NULL 
)
go

ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT XPKrel_receita_medicamento PRIMARY KEY (id_rel_receita_medicamento ASC)
go

ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT fk_receita_rel_receita_medicamento FOREIGN KEY (id_receita) REFERENCES receita(id_receita)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_receita_medicamento
	ADD CONSTRAINT fk_medicamento_rel_receita_medicamento FOREIGN KEY (id_medicamento) REFERENCES medicamento(id_medicamento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

