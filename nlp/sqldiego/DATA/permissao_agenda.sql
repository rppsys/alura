CREATE TABLE permissao_agenda
( 
	id_permissao_agenda  integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL 
)
go

ALTER TABLE permissao_agenda
	ADD CONSTRAINT XPKpermissao_agenda PRIMARY KEY (id_permissao_agenda ASC)
go

ALTER TABLE permissao_agenda
	ADD CONSTRAINT fk_agenda_permissao_agenda FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE permissao_agenda
	ADD CONSTRAINT fk_profissional_saude_permissao_agenda FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

