CREATE TABLE ocupacao
( 
	id_ocupacao          integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE ocupacao
	ADD CONSTRAINT XPKocupacao PRIMARY KEY (id_ocupacao ASC)
go

