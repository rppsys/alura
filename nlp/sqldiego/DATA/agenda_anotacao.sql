CREATE TABLE agenda_anotacao
( 
	id_agenda_anotacao   integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	data                 datetime  NOT NULL ,
	anotacao             varchar(3000)  NULL 
)
go

ALTER TABLE agenda_anotacao
	ADD CONSTRAINT XPKagenda_anotacao PRIMARY KEY (id_agenda_anotacao ASC)
go

ALTER TABLE agenda_anotacao
	ADD CONSTRAINT fk_agenda_agenda_anotacao FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

