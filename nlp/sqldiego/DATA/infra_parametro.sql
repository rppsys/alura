CREATE TABLE infra_parametro
( 
	nome                 varchar(500)  NOT NULL ,
	valor                varchar(5000)  NULL 
)
go

ALTER TABLE infra_parametro
	ADD CONSTRAINT XPKinfra_parametro PRIMARY KEY (nome ASC)
go

