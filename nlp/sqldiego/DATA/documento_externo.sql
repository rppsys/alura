CREATE TABLE documento_externo
( 
	id_documento_externo integer  NOT NULL ,
	numero_processo      varchar(100)  NULL ,
	url_processo         text  NULL ,
	tipo_documento       varchar(100)  NULL ,
	numero_documento     varchar(100)  NULL ,
	url_documento        text  NULL ,
	conteudo_documento   text  NULL ,
	dth_envio            datetime  NULL ,
	retorno_sistema_externo text  NULL ,
	origem               varchar(20)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE documento_externo
	ADD CONSTRAINT XPKdocumento_externo PRIMARY KEY (id_documento_externo ASC)
go

