CREATE TABLE questao
( 
	id_questao           int  NOT NULL ,
	texto                text  NOT NULL ,
	sin_html             char(1)  NULL 
)
go

ALTER TABLE questao
	ADD CONSTRAINT XPKquestao PRIMARY KEY (id_questao ASC)
go

