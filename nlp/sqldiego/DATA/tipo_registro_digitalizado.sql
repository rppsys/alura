CREATE TABLE tipo_registro_digitalizado
( 
	id_tipo_registro_digitalizado integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	id_especialidade     integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT XPKtipo_registro_digitalizado PRIMARY KEY (id_tipo_registro_digitalizado ASC)
go

ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT fk_especialidade_tipo_registro_digitalizado FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE tipo_registro_digitalizado
	ADD CONSTRAINT fk_orgao_tipo_registro_digitalizado FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

