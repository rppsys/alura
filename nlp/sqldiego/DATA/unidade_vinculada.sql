CREATE TABLE unidade_vinculada
( 
	id_unidade_vinculada integer  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	contato              varchar(1000)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	chave_integracao     varchar(20)  NULL 
)
go

ALTER TABLE unidade_vinculada
	ADD CONSTRAINT XPKunidade_vinculada PRIMARY KEY (id_unidade_vinculada ASC)
go

ALTER TABLE unidade_vinculada
	ADD CONSTRAINT fk_orgao_unidade_vinculada FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

