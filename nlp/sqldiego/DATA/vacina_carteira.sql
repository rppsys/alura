CREATE TABLE vacina_carteira
( 
	id_vacina_carteira   integer  NOT NULL ,
	dta_aplicacao        datetime  NULL ,
	id_paciente          integer  NOT NULL ,
	id_vacina            integer  NOT NULL ,
	id_vacina_fabricante integer  NULL ,
	id_vacinacao_servico integer  NULL ,
	lote                 varchar(50)  NULL ,
	observacao           varchar(80)  NULL ,
	dose                 integer  NULL ,
	id_profissional_saude integer  NOT NULL 
)
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT XPKrel_paciente_vacina PRIMARY KEY (id_vacina_carteira ASC)
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_paciente_vacina_carteira FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacina_vacina_carteira FOREIGN KEY (id_vacina) REFERENCES vacina(id_vacina)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacina_fabricante_vacina_carteira FOREIGN KEY (id_vacina_fabricante) REFERENCES vacina_fabricante(id_vacina_fabricante)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_vacinacao_servico_vacina_carteira FOREIGN KEY (id_vacinacao_servico) REFERENCES vacinacao_servico(id_vacinacao_servico)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE vacina_carteira
	ADD CONSTRAINT fk_profissional_saude_vacina_carteira FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

