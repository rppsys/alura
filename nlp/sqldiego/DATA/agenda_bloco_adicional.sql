CREATE TABLE agenda_bloco_adicional
( 
	id_agenda_bloco_adicional integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	dta_bloco            datetime  NOT NULL ,
	hora_inicio          integer  NOT NULL ,
	hora_fim             integer  NOT NULL ,
	duracao              integer  NOT NULL 
)
go

ALTER TABLE agenda_bloco_adicional
	ADD CONSTRAINT XPKagenda_bloco_adicional PRIMARY KEY (id_agenda_bloco_adicional ASC)
go

ALTER TABLE agenda_bloco_adicional
	ADD CONSTRAINT fk_agenda_agenda_bloco_adicional FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

