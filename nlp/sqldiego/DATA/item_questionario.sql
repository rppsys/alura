CREATE TABLE item_questionario
( 
	id_item_questionario int  NOT NULL ,
	sta_tipo             char(1)  NOT NULL ,
	id_questionario      int  NOT NULL ,
	id_questao           int  NULL ,
	id_tipo_questao      int  NULL ,
	sequencia            int  NOT NULL ,
	sin_numerado         char(1)  NOT NULL ,
	sin_visivel          char(1)  NOT NULL ,
	sin_obrigatorio      char(1)  NOT NULL 
)
go

ALTER TABLE item_questionario
	ADD CONSTRAINT XPKitem_questionario PRIMARY KEY (id_item_questionario ASC)
go

ALTER TABLE item_questionario
	ADD CONSTRAINT fk_questionario_item_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE item_questionario
	ADD CONSTRAINT fk_questao_item_questionario FOREIGN KEY (id_questao) REFERENCES questao(id_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE item_questionario
	ADD CONSTRAINT fk_tipo_questao_item_questionario FOREIGN KEY (id_tipo_questao) REFERENCES tipo_questao(id_tipo_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

