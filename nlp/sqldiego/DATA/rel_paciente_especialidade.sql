CREATE TABLE rel_paciente_especialidade
( 
	id_rel_paciente_especialidade integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	nota_especialidade   varchar(2000)  NULL 
)
go

ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT XPKrel_paciente_especialidade PRIMARY KEY (id_rel_paciente_especialidade ASC)
go

ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT fk_especialidade_rel_paciente_especialidade FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_paciente_especialidade
	ADD CONSTRAINT fk_paciente_rel_paciente_especialidade FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

