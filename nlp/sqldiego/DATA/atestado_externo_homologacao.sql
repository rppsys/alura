CREATE TABLE atestado_externo_homologacao
( 
	id_atestado_externo_homologacao integer  NOT NULL ,
	id_atestado_externo  integer  NOT NULL ,
	sta_homologacao      char(1)  NULL ,
	sta_tipo_pericia     char(1)  NULL ,
	dth_homologacao      datetime  NULL ,
	resultado            text  NULL 
)
go

ALTER TABLE atestado_externo_homologacao
	ADD CONSTRAINT XPKatestado_externo_homologacao PRIMARY KEY (id_atestado_externo_homologacao ASC)
go

ALTER TABLE atestado_externo_homologacao
	ADD CONSTRAINT fk_atestado_externo_atestado_externo_homologacao FOREIGN KEY (id_atestado_externo) REFERENCES atestado_externo(id_atestado_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

