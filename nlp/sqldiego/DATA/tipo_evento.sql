CREATE TABLE tipo_evento
( 
	id_tipo_evento       integer  NOT NULL ,
	nome                 varchar(100)  NOT NULL ,
	descricao            varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	id_orgao             integer  NOT NULL 
)
go

ALTER TABLE tipo_evento
	ADD CONSTRAINT XPKtipo_evento PRIMARY KEY (id_tipo_evento ASC)
go

ALTER TABLE tipo_evento
	ADD CONSTRAINT fk_orgao_tipo_evento FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

