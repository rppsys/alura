CREATE TABLE texto_generico
( 
	id_texto_generico    integer  NOT NULL ,
	nome                 varchar(200)  NOT NULL ,
	dth_geracao          datetime  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	conteudo             text  NOT NULL ,
	arquivo              varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_especialidade     integer  NOT NULL 
)
go

ALTER TABLE texto_generico
	ADD CONSTRAINT XPKtexto_gerado PRIMARY KEY (id_texto_generico ASC)
go

ALTER TABLE texto_generico
	ADD CONSTRAINT fk_paciente_texto_generico FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE texto_generico
	ADD CONSTRAINT fk_profissional_saude_texto_generico FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE texto_generico
	ADD CONSTRAINT fk_especialidade_texto_generico FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

