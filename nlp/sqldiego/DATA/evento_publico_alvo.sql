CREATE TABLE evento_publico_alvo
( 
	id_evento_publico_alvo integer  NOT NULL ,
	id_evento            integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	mensagem             varchar(1000)  NULL ,
	sin_recusa           char(1)  NOT NULL ,
	dta_recusa           datetime  NULL ,
	sin_inscricao        char(1)  NOT NULL ,
	dth_inscricao        datetime  NULL ,
	sin_comparecimento   varchar(20)  NOT NULL 
)
go

ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT XPKevento_publico_alvo PRIMARY KEY (id_evento_publico_alvo ASC)
go

ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT fk_evento_evento_publico_alvo FOREIGN KEY (id_evento) REFERENCES evento(id_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE evento_publico_alvo
	ADD CONSTRAINT fk_paciente_evento_publico_alvo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

