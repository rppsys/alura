CREATE TABLE resposta_questao
( 
	id_resposta_questao  int  NOT NULL ,
	id_resposta_questionario int  NOT NULL ,
	id_item_questionario int  NOT NULL ,
	valor_resposta       text  NULL ,
	valor_complemento    text  NULL 
)
go

ALTER TABLE resposta_questao
	ADD CONSTRAINT XPKresposta_questao PRIMARY KEY (id_resposta_questao ASC)
go

ALTER TABLE resposta_questao
	ADD CONSTRAINT fk_resposta_questionario_resposta_questao FOREIGN KEY (id_resposta_questionario) REFERENCES resposta_questionario(id_resposta_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE resposta_questao
	ADD CONSTRAINT fk_item_questionario_resposta_questao FOREIGN KEY (id_item_questionario) REFERENCES item_questionario(id_item_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

