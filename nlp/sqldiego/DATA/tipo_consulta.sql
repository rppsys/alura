CREATE TABLE tipo_consulta
( 
	id_tipo_consulta     integer  NOT NULL ,
	nome                 varchar(40)  NOT NULL ,
	sta_assistencial_ocupacional char(1)  NOT NULL ,
	sin_periodico        char(1)  NULL ,
	sin_cid_consulta_obrigatorio char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE tipo_consulta
	ADD CONSTRAINT XPKtipo_consulta PRIMARY KEY (id_tipo_consulta ASC)
go

