CREATE TABLE medicamento_nome
( 
	id_medicamento_nome  integer  NOT NULL ,
	nome_comercial       varchar(100)  NOT NULL ,
	nome_quimico         varchar(200)  NULL ,
	sin_receita_especial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE medicamento_nome
	ADD CONSTRAINT XPKmedicamento_nome PRIMARY KEY (id_medicamento_nome ASC)
go

