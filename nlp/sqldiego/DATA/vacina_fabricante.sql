CREATE TABLE vacina_fabricante
( 
	id_vacina_fabricante integer  NOT NULL ,
	id_vacina            integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	qtd_doses            integer  NULL ,
	dias_intervalo_doses integer  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE vacina_fabricante
	ADD CONSTRAINT XPKvacina_fabricante PRIMARY KEY (id_vacina_fabricante ASC)
go



