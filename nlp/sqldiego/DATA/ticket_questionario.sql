CREATE TABLE ticket_questionario
( 
	id_ticket_questionario int  NOT NULL ,
	id_questionario      int  NOT NULL ,
	ticket               varchar(100)  NOT NULL ,
	dth_uso              datetime  NOT NULL 
)
go

ALTER TABLE ticket_questionario
	ADD CONSTRAINT XPKticket_questionario PRIMARY KEY (id_ticket_questionario ASC)
go

ALTER TABLE ticket_questionario
	ADD CONSTRAINT fk_questionario_ticket_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

