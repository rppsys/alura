CREATE TABLE rel_agenda_tipo_consulta
( 
	id_rel_agenda_tipo_consulta integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_tipo_consulta     integer  NOT NULL ,
	sin_paciente_solicita char(1)  NOT NULL 
)
go

ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT XPKrel_agenda_tipo_consulta PRIMARY KEY (id_rel_agenda_tipo_consulta ASC)
go

ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT fk_agenda_rel_agenda_tipo_consulta FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE rel_agenda_tipo_consulta
	ADD CONSTRAINT fk_tipo_consulta_rel_agenda_tipo_consulta FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

