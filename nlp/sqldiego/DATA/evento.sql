CREATE TABLE evento
( 
	id_evento            integer  NOT NULL ,
	descricao            varchar(200)  NOT NULL ,
	qtd_vaga             integer  NOT NULL ,
	mensagem_usuario_recusa varchar(8000)  NULL ,
	mensagem_usuario_marcacao varchar(8000)  NULL ,
	mensagem_usuario_cancelamento varchar(8000)  NULL ,
	local                varchar(1000)  NULL ,
	ano_referencia       integer  NULL ,
	dth_inscricao_inicio datetime  NULL ,
	dth_inscricao_fim    datetime  NULL ,
	dta_evento           datetime  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	id_orgao             integer  NOT NULL ,
	id_tipo_evento       integer  NOT NULL 
)
go

ALTER TABLE evento
	ADD CONSTRAINT XPKagenda_evento PRIMARY KEY (id_evento ASC)
go

ALTER TABLE evento
	ADD CONSTRAINT fk_orgao_evento FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE evento
	ADD CONSTRAINT fk_tipo_evento_evento FOREIGN KEY (id_tipo_evento) REFERENCES tipo_evento(id_tipo_evento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

