CREATE TABLE especialidade
( 
	id_especialidade     integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_procedimento     char(1)  NOT NULL ,
	sin_cid              char(1)  NOT NULL ,
	sin_odontologia      char(1)  NOT NULL ,
	sin_historico_familiar char(1)  NOT NULL ,
	sin_medicamento_continuo char(1)  NOT NULL ,
	sin_historico_familiar_psicossocial char(1)  NOT NULL ,
	sin_encaminhamento_psicossocial char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE especialidade
	ADD CONSTRAINT XPKespecialidade PRIMARY KEY (id_especialidade ASC)
go

