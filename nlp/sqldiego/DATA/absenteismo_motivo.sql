CREATE TABLE absenteismo_motivo
( 
	id_absenteismo_motivo integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sigla                varchar(10)  NOT NULL ,
	sin_informa_paciente char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	sigla_integracao     varchar(20)  NULL 
)
go

ALTER TABLE absenteismo_motivo
	ADD CONSTRAINT XPKabsenteismo_motivo PRIMARY KEY (id_absenteismo_motivo ASC)
go

