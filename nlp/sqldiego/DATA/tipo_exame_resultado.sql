CREATE TABLE tipo_exame_resultado
( 
	id_tipo_exame_resultado integer  NOT NULL ,
	id_tipo_exame        integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	nome_tecnico         varchar(50)  NULL ,
	unidade              varchar(10)  NULL ,
	sta_valor_resultado  char(1)  NOT NULL ,
	sin_calculado        char(1)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	ordem                integer  NOT NULL ,
	formula              varchar(80)  NULL ,
	codigo               varchar(20)  NULL 
)
go

ALTER TABLE tipo_exame_resultado
	ADD CONSTRAINT XPKtipo_exame_resultado PRIMARY KEY (id_tipo_exame_resultado ASC)
go

ALTER TABLE tipo_exame_resultado
	ADD CONSTRAINT fk_tipo_exame_tipo_exame_resultado FOREIGN KEY (id_tipo_exame) REFERENCES tipo_exame(id_tipo_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

