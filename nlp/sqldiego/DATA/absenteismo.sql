CREATE TABLE absenteismo
( 
	id_absenteismo       integer  NOT NULL ,
	dth_absenteismo      datetime  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	id_cid               integer  NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	sin_interno          char(1)  NOT NULL ,
	id_atestado_externo_homologacao integer  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	dth_inativacao       datetime  NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	id_documento_externo integer  NULL ,
	arquivo              varchar(200)  NULL ,
	id_grau_parentesco   integer  NULL ,
	justificativa        varchar(500)  NULL 
)
go

ALTER TABLE absenteismo
	ADD CONSTRAINT XPKabsenteismo PRIMARY KEY (id_absenteismo ASC)
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_absenteismo_motivo_absenteismo FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_cid_absenteismo FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_paciente_titular_absenteismo FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_atestado_externo_homologacao_absenteismo FOREIGN KEY (id_atestado_externo_homologacao) REFERENCES atestado_externo_homologacao(id_atestado_externo_homologacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_paciente_absenteismo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_profissional_saude_absenteismo FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_documento_externo_absenteismo FOREIGN KEY (id_documento_externo) REFERENCES documento_externo(id_documento_externo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE absenteismo
	ADD CONSTRAINT fk_grau_parentesco_absenteismo FOREIGN KEY (id_grau_parentesco) REFERENCES grau_parentesco(id_grau_parentesco)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

