CREATE TABLE tipo_questao
( 
	id_tipo_questao      int  NOT NULL ,
	descricao            varchar(200)  NOT NULL ,
	sta_tipo_resposta    char(2)  NOT NULL ,
	opcoes_resposta      text  NULL ,
	sta_tipo_complemento char(2)  NULL ,
	opcoes_complemento   text  NULL 
)
go

ALTER TABLE tipo_questao
	ADD CONSTRAINT XPKtipo_questao PRIMARY KEY (id_tipo_questao ASC)
go

