CREATE TABLE agenda_bloqueio
( 
	id_agenda_bloqueio   integer  NOT NULL ,
	id_agenda_horario    integer  NOT NULL ,
	justificativa        varchar(100)  NOT NULL ,
	id_tipo_bloqueio     integer  NOT NULL 
)
go

ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT XPKagenda_bloqueio PRIMARY KEY (id_agenda_bloqueio ASC)
go

ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT fk_agenda_horario_agenda_bloqueio FOREIGN KEY (id_agenda_horario) REFERENCES agenda_horario(id_agenda_horario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda_bloqueio
	ADD CONSTRAINT fk_tipo_bloqueio_agenda_bloqueio FOREIGN KEY (id_tipo_bloqueio) REFERENCES tipo_bloqueio(id_tipo_bloqueio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

