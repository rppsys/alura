CREATE TABLE grau_parentesco
( 
	id_grau_parentesco   integer  NOT NULL ,
	nome                 varchar(40)  NOT NULL ,
	chave_integracao     varchar(20)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE grau_parentesco
	ADD CONSTRAINT XPKgrau_parentesco PRIMARY KEY (id_grau_parentesco ASC)
go

