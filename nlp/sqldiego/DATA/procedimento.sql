CREATE TABLE procedimento
( 
	id_procedimento      integer  NOT NULL ,
	id_especialidade     integer  NOT NULL ,
	nome                 varchar(200)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE procedimento
	ADD CONSTRAINT XPKtipo_procedimento PRIMARY KEY (id_procedimento ASC)
go

ALTER TABLE procedimento
	ADD CONSTRAINT fk_especialidade_tipo_procedimento FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

