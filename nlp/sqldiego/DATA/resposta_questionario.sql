CREATE TABLE resposta_questionario
( 
	id_resposta_questionario int  NOT NULL ,
	id_questionario      int  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	dth_resposta         datetime  NOT NULL 
)
go

ALTER TABLE resposta_questionario
	ADD CONSTRAINT XPKresposta_questionario PRIMARY KEY (id_resposta_questionario ASC)
go

ALTER TABLE resposta_questionario
	ADD CONSTRAINT fk_questionario_resposta_questionario FOREIGN KEY (id_questionario) REFERENCES questionario(id_questionario)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE resposta_questionario
	ADD CONSTRAINT fk_paciente_resposta_questionario FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

