CREATE TABLE tipo_bloqueio
( 
	id_tipo_bloqueio     integer  NOT NULL ,
	nome                 varchar(80)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE tipo_bloqueio
	ADD CONSTRAINT XPKtipo_bloqueio PRIMARY KEY (id_tipo_bloqueio ASC)
go

