CREATE TABLE vacinacao_servico
( 
	id_vacinacao_servico integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            varchar(20)  NOT NULL 
)
go

ALTER TABLE vacinacao_servico
	ADD CONSTRAINT XPKservico_vacinacao PRIMARY KEY (id_vacinacao_servico ASC)
go

