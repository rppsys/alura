CREATE TABLE vacina
( 
	id_vacina            integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE vacina
	ADD CONSTRAINT XPKvacina PRIMARY KEY (id_vacina ASC)
go

