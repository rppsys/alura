CREATE TABLE receita
( 
	id_receita           integer  NOT NULL ,
	dth_receita          datetime  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_consulta          integer  NULL ,
	arquivo              varchar(8000)  NULL ,
	id_receita_modelo    integer  NULL ,
	conteudo             text  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	id_especialidade     integer  NOT NULL 
)
go

ALTER TABLE receita
	ADD CONSTRAINT XPKreceita PRIMARY KEY (id_receita ASC)
go

ALTER TABLE receita
	ADD CONSTRAINT fk_profissional_saude_receita FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE receita
	ADD CONSTRAINT fk_paciente_receita FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE receita
	ADD CONSTRAINT fk_consulta_receita FOREIGN KEY (id_consulta) REFERENCES consulta(id_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE receita
	ADD CONSTRAINT fk_receita_modelo_receita FOREIGN KEY (id_receita_modelo) REFERENCES receita_modelo(id_receita_modelo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE receita
	ADD CONSTRAINT fk_especialidade_receita FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

