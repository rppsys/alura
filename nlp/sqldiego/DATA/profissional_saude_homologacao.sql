CREATE TABLE profissional_saude_homologacao
( 
	id_profissional_saude_homologacao integer  NOT NULL ,
	id_profissional_saude integer  NOT NULL ,
	id_beneficio         integer  NULL ,
	id_atestado_externo_homologacao integer  NULL ,
	id_pericia           int  NULL ,
	sin_assinatura       char(1)  NOT NULL 
)
go

ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT XPKrel_profissional_saude_beneficio_homologacao PRIMARY KEY (id_profissional_saude_homologacao ASC)
go

ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_profissional_saude_profissional_saude_homologacao FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_beneficio_profissional_saude_homologacao FOREIGN KEY (id_beneficio) REFERENCES beneficio(id_beneficio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_pericia_profissional_saude_homologacao FOREIGN KEY (id_pericia) REFERENCES pericia(id_pericia)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE profissional_saude_homologacao
	ADD CONSTRAINT fk_atestado_externo_homologacao_profissional_saude_homologacao FOREIGN KEY (id_atestado_externo_homologacao) REFERENCES atestado_externo_homologacao(id_atestado_externo_homologacao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

