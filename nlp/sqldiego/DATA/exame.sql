CREATE TABLE exame
( 
	id_exame             bigint  NOT NULL ,
	id_tipo_exame        integer  NOT NULL ,
	titulo               varchar(100)  NULL ,
	descricao            varchar(1000)  NULL ,
	arquivo              varchar(200)  NULL ,
	id_paciente          integer  NOT NULL ,
	id_laboratorio       integer  NULL ,
	dth_exame            datetime  NOT NULL ,
	dth_inclusao         datetime  NULL ,
	id_profissional_saude integer  NOT NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL 
)
go

ALTER TABLE exame
	ADD CONSTRAINT XPKexame PRIMARY KEY (id_exame ASC)
go

ALTER TABLE exame
	ADD CONSTRAINT fk_laboratorio_exame FOREIGN KEY (id_laboratorio) REFERENCES laboratorio(id_laboratorio)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE exame
	ADD CONSTRAINT fk_paciente_exame FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE exame
	ADD CONSTRAINT fk_tipo_exame_exame FOREIGN KEY (id_tipo_exame) REFERENCES tipo_exame(id_tipo_exame)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE exame
	ADD CONSTRAINT fk_profissional_saude_exame FOREIGN KEY (id_profissional_saude) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

