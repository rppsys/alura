CREATE TABLE calendario_feriado
( 
	id_calendario_feriado integer  NOT NULL ,
	nome                 varchar(50)  NOT NULL ,
	lista_dia_fixo       varchar(5000)  NULL ,
	lista_dia_movel      varchar(5000)  NULL ,
	sin_ativo            char(1)  NOT NULL 
)
go

ALTER TABLE calendario_feriado
	ADD CONSTRAINT XPKferiado PRIMARY KEY (id_calendario_feriado ASC)
go

