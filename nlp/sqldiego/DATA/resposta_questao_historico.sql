CREATE TABLE resposta_questao_historico
( 
	id_resposta_questao_historico int  NOT NULL ,
	id_resposta_questao  int  NOT NULL ,
	valor_resposta       text  NULL ,
	valor_complemento    text  NULL ,
	dth_resposta         datetime  NULL 
)
go

ALTER TABLE resposta_questao_historico
	ADD CONSTRAINT XPKresposta_questao_historico PRIMARY KEY (id_resposta_questao_historico ASC)
go

ALTER TABLE resposta_questao_historico
	ADD CONSTRAINT fk_resposta_questao_resposta_questao_historico FOREIGN KEY (id_resposta_questao) REFERENCES resposta_questao(id_resposta_questao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

