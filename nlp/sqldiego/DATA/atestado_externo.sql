CREATE TABLE atestado_externo
( 
	id_atestado_externo  integer  NOT NULL ,
	id_paciente          integer  NOT NULL ,
	id_paciente_titular  integer  NOT NULL ,
	nome_medico          varchar(100)  NULL ,
	crm_medico           varchar(20)  NULL ,
	especialidade_medica varchar(50)  NULL ,
	nome_clinica         varchar(100)  NULL ,
	dta_inicio           datetime  NOT NULL ,
	dta_fim              datetime  NOT NULL ,
	dta_atestado         datetime  NOT NULL ,
	id_absenteismo_motivo integer  NOT NULL ,
	sin_necessita_revisao char(1)  NOT NULL ,
	chave_integracao_rh  varchar(20)  NULL ,
	id_cid               integer  NULL ,
	id_profissional_saude_registro integer  NULL ,
	dth_registro         datetime  NOT NULL ,
	arquivo              varchar(200)  NULL ,
	sin_ativo            char(1)  NOT NULL ,
	motivo_inativacao    varchar(1000)  NULL ,
	dth_inativacao       datetime  NULL ,
	observacao           varchar(1000)  NULL ,
	justificativa        varchar(500)  NULL ,
	id_grau_parentesco   integer  NULL ,
	id_orgao             integer  NOT NULL ,
	id_especialidade     integer  NOT NULL 
)
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT XPKatestado_externo PRIMARY KEY (id_atestado_externo ASC)
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_cid_atestado_externo FOREIGN KEY (id_cid) REFERENCES cid(id_cid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_paciente_titular_atestado_externo FOREIGN KEY (id_paciente_titular) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_paciente_atestado_externo FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_absenteismo_motivo_atestado_externo FOREIGN KEY (id_absenteismo_motivo) REFERENCES absenteismo_motivo(id_absenteismo_motivo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_profissional_saude_atestado_externo FOREIGN KEY (id_profissional_saude_registro) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_grau_parentesco_atestado_externo FOREIGN KEY (id_grau_parentesco) REFERENCES grau_parentesco(id_grau_parentesco)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_orgao_atestado_externo FOREIGN KEY (id_orgao) REFERENCES orgao(id_orgao)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE atestado_externo
	ADD CONSTRAINT fk_especialidade_atestado_externo FOREIGN KEY (id_especialidade) REFERENCES especialidade(id_especialidade)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

