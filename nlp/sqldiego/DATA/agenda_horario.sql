CREATE TABLE agenda_horario
( 
	id_agenda_horario    integer  NOT NULL ,
	id_agenda            integer  NOT NULL ,
	id_paciente          integer  NULL ,
	id_tipo_consulta     integer  NULL ,
	data                 datetime  NULL ,
	hora_inicio          integer  NULL ,
	hora_fim             integer  NULL ,
	sta_horario          char(1)  NULL ,
	observacao           varchar(200)  NULL ,
	id_profissional_agenda integer  NULL ,
	id_profissional_marcacao integer  NULL ,
	sta_cancelado        varchar(1)  NULL ,
	sin_presencial       char(1)  NOT NULL 
)
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT XPKagenda_horario PRIMARY KEY (id_agenda_horario ASC)
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_agenda_agenda_horario FOREIGN KEY (id_agenda) REFERENCES agenda(id_agenda)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_paciente_agenda_horario FOREIGN KEY (id_paciente) REFERENCES paciente(id_paciente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_tipo_consulta_agenda_horario FOREIGN KEY (id_tipo_consulta) REFERENCES tipo_consulta(id_tipo_consulta)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_profissional_agenda_agenda_horario FOREIGN KEY (id_profissional_agenda) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE agenda_horario
	ADD CONSTRAINT fk_profissional_atendimento_agenda_horario FOREIGN KEY (id_profissional_marcacao) REFERENCES profissional_saude(id_profissional_saude)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

