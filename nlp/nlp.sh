#!/usr/bin/env bash
# Filename: nlp.sh
cd ~/prog/vNLP/bin &&
source activate &&
cd ~/prog/alura/nlp

if [[ $1 = 'pull' ]]
then
  cd ~/prog/alura &&
  git pull
fi

if [[ $1 = 'push' ]]
then
  cd ~/prog/alura &&
  git push
fi
