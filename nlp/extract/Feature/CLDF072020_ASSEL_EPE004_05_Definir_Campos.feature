# language: pt
# encoding: utf-8



Funcionalidade: 05 - Definir Campos

  A seguir, as seguintes definições de campos referentes à funcionalidade Solicitar Ordem de Serviço:

  Contexto:
    Dadas as seguintes definições de campos de Solicitação:
      | Nome do Campo             | Legenda do Campo          | Tipo                             | Origem da Informação |
      | Deputado/Órgão            | Deputado/Órgão            | Texto 100 caracteres             |                      |
      | Contato                   | Contato                   | Texto 100 caracteres             |                      |
      | Ramal                     | Ramal                     | Numeral                          |                      |
      | Tipo                      | Tipo                      | Combobox                         |                      |
      | Subtipo                   | Subtipo                   | Combobox                         |                      |
      | Especificação do Trabalho | Especificação do Trabalho | Texto 500 caracteres             |                      |
      | Público                   | Público                   | Radio                            |                      |
      | Restrito                  | Restrito                  | Radio                            |                      |
      | Hipótese Legal            | Hipótese Legal            | Combobox                         |                      |
      | Número                    | Número                    | Numeral                          |                      |
      | Ano                       | Ano                       | Numeral                          |                      |
      | Comissão                  | Comissão                  | Combobox                         |                      |
      | Ementa                    | Ementa                    | Texto - Desabilitado para edição | PLE                  |
