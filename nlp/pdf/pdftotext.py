# https://betterprogramming.pub/how-to-convert-pdfs-into-searchable-key-words-with-python-85aab86c544f

import PyPDF2 
import textract
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

filename = 'teste.pdf' 
pdfFileObj = open(filename,'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
num_pages = pdfReader.numPages

count = 0
text = ""
while count < num_pages:
	pageObj = pdfReader.getPage(count)
	count +=1
	text += pageObj.extractText()

if text != "":
   text = text
else:
   text = textract.process(fileurl, method='tesseract', language='pt-br')
   
print(text)

tokens = word_tokenize(text)
punctuations = ['(',')',';',':','[',']',',']
stop_words = stopwords.words('portuguese')
keywords = [word for word in tokens if not word in stop_words and not word in punctuations]

for a in keywords:
	print(a)

# Depois teria de ir alimentando um banco de dados com as frequencias de cada palavra

